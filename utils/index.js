export const formatUid = id => 'i' + id.replace(/[{}]/g, '')

export const getPlaceholder = (placeholder, name) => {
  try {
    return placeholder[name]
  } catch {
    return []
  }
}