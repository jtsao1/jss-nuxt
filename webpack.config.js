const path = require('path')

module.exports = {
  mode: 'production',
  entry: path.join(__dirname, 'integrated.js'),
  output: {
    filename: 'server.bundle.js',
    libraryTarget: 'commonjs2'
  },
  externals: 'fsevents', // fsevents is macspecific
  target: 'node'
}
