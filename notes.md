Did not need to use any dependancies from the vue-sitecore-jss npm package. Everything we need can be created within Nuxt/Vue

Format JSON before it is used in the front end (don't want nested value objects)

Placeholder component should contain all components that can be rendered by Sitecore
  Need to update Placeholder.vue

Need to create 3 special components for urls that can also be html
  SCImage
  SCLInk
  SCBackgroundImage

Some components will need to be coded a certain way to account for experience editor (e.g. tabs)
  Tabs - in experience editor we want to display the tabs opened

Can use the store to keep keep track of sitecore context, viewback, route data