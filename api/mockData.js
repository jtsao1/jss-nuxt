const styleguide = {
  "sitecore": {
    "context": {
      "pageEditing": false,
      "site": { "name": "JssDisconnectedLayoutService" },
      "pageState": "normal",
      "language": "en"
    },
    "route": {
      "databaseName": "available-in-connected-mode",
      "deviceId": "available-in-connected-mode",
      "itemId": "available-in-connected-mode",
      "itemLanguage": "en",
      "itemVersion": 1,
      "layoutId": "available-in-connected-mode",
      "templateId": "available-in-connected-mode",
      "templateName": "App Route",
      "name": "styleguide",
      "fields": { "pageTitle": { "value": "Styleguide | Sitecore JSS" } },
      "placeholders": {
        "jss-main": [
          {
            "uid": "{E02DDB9B-A062-5E50-924A-1940D7E053CE}",
            "componentName": "ContentBlock",
            "dataSource": "available-in-connected-mode",
            "params": {},
            "fields": {
              "heading": { "value": "JSS Styleguide" },
              "content": {
                "value": "<p>This is a live set of examples of how to use JSS. For more information on using JSS, please see <a href=\"https://jss.sitecore.net\" target=\"_blank\" rel=\"noopener noreferrer\">the documentation</a>.</p>\n<p>The content and layout of this page is defined in <code>/data/routes/styleguide/en.yml</code></p>\n"
              }
            }
          },
          {
            "uid": "{34A6553C-81DE-5CD3-989E-853F6CB6DF8C}",
            "componentName": "Styleguide-Layout",
            "dataSource": "available-in-connected-mode",
            "params": {},
            "fields": {},
            "placeholders": {
              "jss-styleguide-layout": [
                {
                  "uid": "{B7C779DA-2B75-586C-B40D-081FCB864256}",
                  "componentName": "Styleguide-Section",
                  "dataSource": "available-in-connected-mode",
                  "params": {},
                  "fields": { "heading": { "value": "Content Data" } },
                  "placeholders": {
                    "jss-styleguide-section": [
                      {
                        "uid": "{63B0C99E-DAC7-5670-9D66-C26A78000EAE}",
                        "componentName": "Styleguide-FieldUsage-Text",
                        "dataSource": "available-in-connected-mode",
                        "params": {},
                        "fields": {
                          "heading": { "value": "Single-Line Text" },
                          "sample": {
                            "value": "This is a sample text field. <mark>HTML is encoded.</mark> In Sitecore, editors will see a <input type=\"text\">."
                          },
                          "sample2": {
                            "value": "This is another sample text field using rendering options. <mark>HTML supported with encode=false.</mark> Cannot edit because editable=false."
                          }
                        }
                      },
                      {
                        "uid": "{F1EA3BB5-1175-5055-AB11-9C48BF69427A}",
                        "componentName": "Styleguide-FieldUsage-Text",
                        "dataSource": "available-in-connected-mode",
                        "params": {},
                        "fields": {
                          "heading": { "value": "Multi-Line Text" },
                          "description": {
                            "value": "<small>Multi-line text tells Sitecore to use a <code>textarea</code> for editing; consumption in JSS is the same as single-line text.</small>"
                          },
                          "sample": {
                            "value": "This is a sample multi-line text field. <mark>HTML is encoded.</mark> In Sitecore, editors will see a textarea."
                          },
                          "sample2": {
                            "value": "This is another sample multi-line text field using rendering options. <mark>HTML supported with encode=false.</mark>"
                          }
                        }
                      },
                      {
                        "uid": "{69CEBC00-446B-5141-AD1E-450B8D6EE0AD}",
                        "componentName": "Styleguide-FieldUsage-RichText",
                        "dataSource": "available-in-connected-mode",
                        "params": {},
                        "fields": {
                          "heading": { "value": "Rich Text" },
                          "sample": {
                            "value": "<p>This is a sample rich text field. <mark>HTML is always supported.</mark> In Sitecore, editors will see a WYSIWYG editor for these fields.</p>"
                          },
                          "sample2": {
                            "value": "<p>Another sample rich text field, using options. Keep markup entered in rich text fields as simple as possible - ideally bare tags only (no classes). Adding a wrapping class can help with styling within rich text blocks.</p>\n<marquee>But you can use any valid HTML in a rich text field!</marquee>\n"
                          }
                        }
                      },
                      {
                        "uid": "{5630C0E6-0430-5F6A-AF9E-2D09D600A386}",
                        "componentName": "Styleguide-FieldUsage-Image",
                        "dataSource": "available-in-connected-mode",
                        "params": {},
                        "fields": {
                          "heading": { "value": "Image" },
                          "sample1": {
                            "value": {
                              "src": "/data/media/img/sc_logo.png",
                              "alt": "Sitecore Logo"
                            }
                          },
                          "sample2": {
                            "value": {
                              "src": "/data/media/img/jss_logo.png",
                              "alt": "Sitecore JSS Logo"
                            }
                          }
                        }
                      },
                      {
                        "uid": "{BAD43EF7-8940-504D-A09B-976C17A9A30C}",
                        "componentName": "Styleguide-FieldUsage-File",
                        "dataSource": "available-in-connected-mode",
                        "params": {},
                        "fields": {
                          "heading": { "value": "File" },
                          "description": {
                            "value": "<small>Note: Sitecore does not support inline editing of File fields. The value must be edited in Experience Editor by using the edit rendering fields button (looks like a pencil) with the whole component selected.</small>\n"
                          },
                          "file": {
                            "value": {
                              "src": "/data/media/files/jss.pdf",
                              "title": "Example File",
                              "description": "This data will be added to the Sitecore Media Library on import"
                            }
                          }
                        }
                      },
                      {
                        "uid": "{FF90D4BD-E50D-5BBF-9213-D25968C9AE75}",
                        "componentName": "Styleguide-FieldUsage-Number",
                        "dataSource": "available-in-connected-mode",
                        "params": {},
                        "fields": {
                          "heading": { "value": "Number" },
                          "description": {
                            "value": "<small>Number tells Sitecore to use a number entry for editing.</small>"
                          },
                          "sample": { "value": 1.21 },
                          "sample2": { "value": 71 }
                        }
                      },
                      {
                        "uid": "{B5C1C74A-A81D-59B2-85D8-09BC109B1F70}",
                        "componentName": "Styleguide-FieldUsage-Checkbox",
                        "dataSource": "available-in-connected-mode",
                        "params": {},
                        "fields": {
                          "heading": { "value": "Checkbox" },
                          "description": {
                            "value": "<small>Note: Sitecore does not support inline editing of Checkbox fields. The value must be edited in Experience Editor by using the edit rendering fields button (looks like a pencil) with the whole component selected.</small>\n"
                          },
                          "checkbox": { "value": true },
                          "checkbox2": { "value": false }
                        }
                      },
                      {
                        "uid": "{F166A7D6-9EC8-5C53-B825-33405DB7F575}",
                        "componentName": "Styleguide-FieldUsage-Date",
                        "dataSource": "available-in-connected-mode",
                        "params": {},
                        "fields": {
                          "heading": { "value": "Date" },
                          "description": {
                            "value": "<p><small>Both <code>Date</code> and <code>DateTime</code> field types are available. Choosing <code>DateTime</code> will make Sitecore show editing UI for time; both types store complete date and time values internally. Date values in JSS are formatted using <a href=\"https://en.wikipedia.org/wiki/ISO_8601#Combined_date_and_time_representations\" target=\"_blank\">ISO 8601 formatted strings</a>, for example <code>2012-04-23T18:25:43.511Z</code>.</small></p>\n<div class=\"alert alert-warning\"><small>Note: this is a JavaScript date format (e.g. <code>new Date().toISOString()</code>), and is different from how Sitecore stores date field values internally. Sitecore-formatted dates will not work.</small></div>\n"
                          },
                          "date": { "value": "2012-05-04T00:00:00Z" },
                          "dateTime": { "value": "2018-03-14T15:00:00Z" }
                        }
                      },
                      {
                        "uid": "{56A9562A-6813-579B-8ED2-FDDAB1BFD3D2}",
                        "componentName": "Styleguide-FieldUsage-Link",
                        "dataSource": "available-in-connected-mode",
                        "params": {},
                        "fields": {
                          "heading": { "value": "General Link" },
                          "description": {
                            "value": "<p>A <em>General Link</em> is a field that represents an <code>&lt;a&gt;</code> tag.</p>"
                          },
                          "externalLink": {
                            "value": {
                              "href": "https://www.sitecore.com",
                              "text": "Link to Sitecore"
                            }
                          },
                          "internalLink": { "value": { "href": "/" } },
                          "mediaLink": {
                            "value": {
                              "href": "/data/media/files/jss.pdf",
                              "text": "Link to PDF"
                            }
                          },
                          "emailLink": {
                            "value": {
                              "href": "mailto:foo@bar.com",
                              "text": "Send an Email"
                            }
                          },
                          "paramsLink": {
                            "value": {
                              "href": "https://dev.sitecore.net",
                              "text": "Sitecore Dev Site",
                              "target": "_blank",
                              "class": "font-weight-bold",
                              "title": "<a> title attribute"
                            }
                          }
                        }
                      },
                      {
                        "uid": "{A44AD1F8-0582-5248-9DF9-52429193A68B}",
                        "componentName": "Styleguide-FieldUsage-ItemLink",
                        "dataSource": "available-in-connected-mode",
                        "params": {},
                        "fields": {
                          "heading": { "value": "Item Link" },
                          "description": {
                            "value": "<p>\n  <small>\n    Item Links are a way to reference another content item to use data from it.\n    Referenced items may be shared.\n    To reference multiple content items, use a <em>Content List</em> field.<br />\n    <strong>Note:</strong> Sitecore does not support inline editing of Item Link fields. The value must be edited in Experience Editor by using the edit rendering fields button (looks like a pencil) with the whole component selected.\n  </small>\n</p>\n"
                          },
                          "sharedItemLink": {
                            "id": "available-in-connected-mode",
                            "fields": {
                              "textField": {
                                "value": "ItemLink Demo (Shared) Item 1 Text Field"
                              }
                            }
                          },
                          "localItemLink": {
                            "id": "available-in-connected-mode",
                            "fields": {
                              "textField": {
                                "value": "Referenced item textField"
                              }
                            }
                          }
                        }
                      },
                      {
                        "uid": "{2F609D40-8AD9-540E-901E-23AA2600F3EB}",
                        "componentName": "Styleguide-FieldUsage-ContentList",
                        "dataSource": "available-in-connected-mode",
                        "params": {},
                        "fields": {
                          "heading": { "value": "Content List" },
                          "description": {
                            "value": "<p>\n  <small>\n    Content Lists are a way to reference zero or more other content items.\n    Referenced items may be shared.\n    To reference a single content item, use an <em>Item Link</em> field.<br />\n    <strong>Note:</strong> Sitecore does not support inline editing of Content List fields. The value must be edited in Experience Editor by using the edit rendering fields button (looks like a pencil) with the whole component selected.\n  </small>\n</p>\n"
                          },
                          "sharedContentList": [
                            {
                              "fields": {
                                "textField": {
                                  "value": "ContentList Demo (Shared) Item 1 Text Field"
                                }
                              },
                              "id": "available-in-connected-mode"
                            },
                            {
                              "fields": {
                                "textField": {
                                  "value": "ContentList Demo (Shared) Item 2 Text Field"
                                }
                              },
                              "id": "available-in-connected-mode"
                            }
                          ],
                          "localContentList": [
                            {
                              "fields": {
                                "textField": { "value": "Hello World Item 1" }
                              },
                              "id": "available-in-connected-mode"
                            },
                            {
                              "fields": {
                                "textField": { "value": "Hello World Item 2" }
                              },
                              "id": "available-in-connected-mode"
                            }
                          ]
                        }
                      },
                      {
                        "uid": "{352ED63D-796A-5523-89F5-9A991DDA4A8F}",
                        "componentName": "Styleguide-FieldUsage-Custom",
                        "dataSource": "available-in-connected-mode",
                        "params": {},
                        "fields": {
                          "heading": { "value": "Custom Fields" },
                          "description": {
                            "value": "<p>\n  <small>\n    Any Sitecore field type can be consumed by JSS.\n    In this sample we consume the <em>Integer</em> field type.<br />\n    <strong>Note:</strong> For field types with complex data, custom <code>FieldSerializer</code>s may need to be implemented on the Sitecore side.\n  </small>\n</p>\n"
                          },
                          "customIntField": { "value": 31337 }
                        }
                      }
                    ]
                  }
                },
                {
                  "uid": "{7DE41A1A-24E4-5963-8206-3BB0B7D9DD69}",
                  "componentName": "Styleguide-Section",
                  "dataSource": "available-in-connected-mode",
                  "params": {},
                  "fields": { "heading": { "value": "Layout Patterns" } },
                  "placeholders": {
                    "jss-header": [
                      {
                        "uid": "{1A6FCB1C-E97B-5325-8E4E-6E13997A4A1A}",
                        "componentName": "Styleguide-Layout-Reuse",
                        "dataSource": "available-in-connected-mode",
                        "params": {},
                        "fields": {}
                      }
                    ],
                    "jss-styleguide-section": [
                      {
                        "uid": "{3A5D9C50-D8C1-5A12-8DA8-5D56C2A5A69A}",
                        "componentName": "Styleguide-Layout-Reuse",
                        "dataSource": "available-in-connected-mode",
                        "params": {},
                        "fields": {
                          "heading": { "value": "Reusing Content" },
                          "description": {
                            "value": "<p>JSS provides powerful options to reuse content, whether it's sharing a common piece of text across pages or sketching out a site with repeating <em>lorem ipsum</em> content.</p>"
                          }
                        },
                        "placeholders": {
                          "jss-reuse-example": [
                            {
                              "uid": "{AA328B8A-D6E1-5B37-8143-250D2E93D6B8}",
                              "componentName": "ContentBlock",
                              "dataSource": "available-in-connected-mode",
                              "params": {},
                              "fields": {
                                "content": {
                                  "value": "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque felis mauris, pretium id neque vitae, vulputate pellentesque tortor. Mauris hendrerit dolor et ipsum lobortis bibendum non finibus neque. Morbi volutpat aliquam magna id posuere. Duis commodo cursus dui, nec interdum velit congue nec. Aliquam erat volutpat. Aliquam facilisis, sapien quis fringilla tincidunt, magna nulla feugiat neque, a consectetur arcu orci eu augue.</p>"
                                }
                              }
                            },
                            {
                              "uid": "{C4330D34-623C-556C-BF4C-97C93D40FB1E}",
                              "componentName": "ContentBlock",
                              "dataSource": "available-in-connected-mode",
                              "params": {},
                              "fields": {
                                "content": {
                                  "value": "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque felis mauris, pretium id neque vitae, vulputate pellentesque tortor. Mauris hendrerit dolor et ipsum lobortis bibendum non finibus neque. Morbi volutpat aliquam magna id posuere. Duis commodo cursus dui, nec interdum velit congue nec. Aliquam erat volutpat. Aliquam facilisis, sapien quis fringilla tincidunt, magna nulla feugiat neque, a consectetur arcu orci eu augue.</p>"
                                }
                              }
                            },
                            {
                              "uid": "{A42D8B1C-193D-5627-9130-F7F7F87617F1}",
                              "componentName": "ContentBlock",
                              "dataSource": "available-in-connected-mode",
                              "params": {},
                              "fields": {
                                "content": {
                                  "value": "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque felis mauris, pretium id neque vitae, vulputate pellentesque tortor. Mauris hendrerit dolor et ipsum lobortis bibendum non finibus neque. Morbi volutpat aliquam magna id posuere. Duis commodo cursus dui, nec interdum velit congue nec. Aliquam erat volutpat. Aliquam facilisis, sapien quis fringilla tincidunt, magna nulla feugiat neque, a consectetur arcu orci eu augue.</p>"
                                }
                              }
                            },
                            {
                              "uid": "{0F4CB47A-979E-5139-B50B-A8E40C73C236}",
                              "componentName": "ContentBlock",
                              "dataSource": "available-in-connected-mode",
                              "params": {},
                              "fields": {
                                "content": {
                                  "value": "<p>Mix and match reused and local content. Check out <code>/data/routes/styleguide/en.yml</code> to see how.</p>"
                                }
                              }
                            }
                          ]
                        }
                      },
                      {
                        "uid": "{538E4831-F157-50BB-AC74-277FCAC9FDDB}",
                        "componentName": "Styleguide-Layout-Tabs",
                        "dataSource": "available-in-connected-mode",
                        "params": {},
                        "fields": {
                          "heading": { "value": "Tabs" },
                          "description": {
                            "value": "<p>Creating hierarchical components like tabs is made simpler in JSS because it's easy to introspect the layout structure.</p>"
                          }
                        },
                        "placeholders": {
                          "jss-tabs": [
                            {
                              "uid": "{7ECB2ED2-AC9B-58D1-8365-10CA74824AF7}",
                              "componentName": "Styleguide-Layout-Tabs-Tab",
                              "dataSource": "available-in-connected-mode",
                              "params": {},
                              "fields": {
                                "title": { "value": "Tab 1" },
                                "content": { "value": "<p>Tab 1 contents!</p>" }
                              }
                            },
                            {
                              "uid": "{AFD64900-0A61-50EB-A674-A7A884E0D496}",
                              "componentName": "Styleguide-Layout-Tabs-Tab",
                              "dataSource": "available-in-connected-mode",
                              "params": {},
                              "fields": {
                                "title": { "value": "Tab 2" },
                                "content": { "value": "<p>Tab 2 contents!</p>" }
                              }
                            },
                            {
                              "uid": "{44C12983-3A84-5462-84C0-6CA1430050C8}",
                              "componentName": "Styleguide-Layout-Tabs-Tab",
                              "dataSource": "available-in-connected-mode",
                              "params": {},
                              "fields": {
                                "title": { "value": "Tab 3" },
                                "content": { "value": "<p>Tab 3 contents!</p>" }
                              }
                            }
                          ]
                        }
                      }
                    ]
                  }
                },
                {
                  "uid": "{2D806C25-DD46-51E3-93DE-63CF9035122C}",
                  "componentName": "Styleguide-Section",
                  "dataSource": "available-in-connected-mode",
                  "params": {},
                  "fields": { "heading": { "value": "Sitecore Patterns" } },
                  "placeholders": {
                    "jss-styleguide-section": [
                      {
                        "uid": "{471FA16A-BB82-5C42-9C95-E7EAB1E3BD30}",
                        "componentName": "Styleguide-SitecoreContext",
                        "dataSource": "available-in-connected-mode",
                        "params": {},
                        "fields": {
                          "heading": { "value": "Sitecore Context" },
                          "description": {
                            "value": "<p><small>The Sitecore Context contains route-level data about the current context - for example, <code>pageState</code> enables conditionally executing code based on whether Sitecore is in Experience Editor or not.</small></p>"
                          }
                        }
                      },
                      {
                        "uid": "{21F21053-8F8A-5436-BC79-E674E246A2FC}",
                        "componentName": "Styleguide-RouteFields",
                        "dataSource": "available-in-connected-mode",
                        "params": {},
                        "fields": {
                          "heading": { "value": "Route-level Fields" },
                          "description": {
                            "value": "<p><small>Route-level content fields are defined on the <em>route</em> instead of on a <em>component</em>. This allows multiple components to share the field data on the same route - and querying is much easier on route level fields, making <em>custom route types</em> ideal for filterable/queryable data such as articles.</small></p>"
                          }
                        }
                      },
                      {
                        "uid": "{A0A66136-C21F-52E8-A2EA-F04DCFA6A027}",
                        "componentName": "Styleguide-ComponentParams",
                        "dataSource": "available-in-connected-mode",
                        "params": {
                          "cssClass": "alert alert-success",
                          "columns": "5",
                          "useCallToAction": "true"
                        },
                        "fields": {
                          "heading": { "value": "Component Params" },
                          "description": {
                            "value": "<p><small>Component params (also called Rendering Parameters) allow storing non-content parameters for a component. These params should be used for more technical options such as CSS class names or structural settings.</small></p>"
                          }
                        }
                      },
                      {
                        "uid": "{7F765FCB-3B10-58FD-8AA7-B346EF38C9BB}",
                        "componentName": "Styleguide-Tracking",
                        "dataSource": "available-in-connected-mode",
                        "params": {},
                        "fields": {
                          "heading": { "value": "Tracking" },
                          "description": {
                            "value": "<p><small>JSS supports tracking Sitecore analytics events from within apps. Give it a try with this handy interactive demo.</small></p>"
                          }
                        }
                      }
                    ]
                  }
                },
                {
                  "uid": "{66AF8F03-0B52-5425-A6AF-6FB54F2D64D9}",
                  "componentName": "Styleguide-Section",
                  "dataSource": "available-in-connected-mode",
                  "params": {},
                  "fields": { "heading": { "value": "Multilingual Patterns" } },
                  "placeholders": {
                    "jss-styleguide-section": [
                      {
                        "uid": "{CF1B5D2B-C949-56E7-9594-66AFACEACA9D}",
                        "componentName": "Styleguide-Multilingual",
                        "dataSource": "available-in-connected-mode",
                        "params": {},
                        "fields": {
                          "heading": { "value": "Translation Patterns" },
                          "sample": {
                            "value": "This text can be translated in en.yml"
                          }
                        }
                      }
                    ]
                  }
                }
              ]
            }
          }
        ],
        "jss-styleguide-layout": [],
        "jss-styleguide-section": [],
        "jss-header": [],
        "jss-reuse-example": [],
        "jss-tabs": []
      }
    }
  }
}


const home = {
  "sitecore": {
    "context": {
      "pageEditing": false,
      "site": {
        "name": "JssDisconnectedLayoutService"
      },
      "pageState": "normal",
      "language": "en"
    },
    "route": {
      "databaseName": "available-in-connected-mode",
      "deviceId": "available-in-connected-mode",
      "itemId": "available-in-connected-mode",
      "itemLanguage": "en",
      "itemVersion": 1,
      "layoutId": "available-in-connected-mode",
      "templateId": "available-in-connected-mode",
      "templateName": "available-in-connected-mode",
      "name": "graphql",
      "fields": {
        "pageTitle": {
          "value": "GraphQL | Sitecore JSS"
        }
      },
      "placeholders": {
        "jss-main": [
          {
            "uid": "{FB82986A-A6CD-53FD-AA1D-BC5036B6979B}",
            "componentName": "ContentBlock",
            "dataSource": "available-in-connected-mode",
            "params": {},
            "fields": {
              "heading": {
                "value": "Using GraphQL with JSS"
              },
              "content": {
                "value": "<p>This is a live example of using Integrated GraphQL and Connected GraphQL with a JSS app.\nFor more information on GraphQL use in JSS, please see <a href=\"https://jss.sitecore.net\" target=\"_blank\" rel=\"noopener noreferrer\">the documentation</a>.</p>\n"
              }
            }
          },
          {
            "uid": "{A9FBC7CC-F00B-58A2-873B-4C70C726476C}",
            "componentName": "GraphQL-Layout",
            "dataSource": "available-in-connected-mode",
            "params": {},
            "fields": {},
            "placeholders": {
              "jss-graphql-layout": [
                {
                  "uid": "{B6BBD1E2-C0B2-5C34-ADD3-858333393BFD}",
                  "componentName": "GraphQL-IntegratedDemo",
                  "dataSource": "available-in-connected-mode",
                  "params": {},
                  "fields": {
                    "sample1": {
                      "value": "Hello integrated GraphQL world!"
                    },
                    "sample2": {
                      "value": {
                        "href": "https://www.sitecore.com",
                        "target": "_blank",
                        "text": "GraphQL lets you get structured field data too"
                      }
                    }
                  }
                },
                {
                  "uid": "{4E86EC75-DCF5-57A7-B43C-150ADD6BD099}",
                  "componentName": "GraphQL-ConnectedDemo",
                  "dataSource": "available-in-connected-mode",
                  "params": {},
                  "fields": {
                    "sample1": {
                      "value": "Hello connected GraphQL world!"
                    },
                    "sample2": {
                      "value": {
                        "href": "https://www.sitecore.com",
                        "target": "_blank",
                        "text": "GraphQL lets you get structured field data too"
                      }
                    }
                  }
                },
                {
                  "uid": "{EC633755-A803-521F-A378-A2CE203B80EC}",
                  "componentName": "GraphQL-SSRDemo",
                  "dataSource": "available-in-connected-mode",
                  "params": {},
                  "fields": {
                    "sample1": {
                      "value": "Hello connected GraphQL world!"
                    },
                    "sample2": {
                      "value": {
                        "href": "https://www.sitecore.com",
                        "target": "_blank",
                        "text": "GraphQL lets you get structured field data too"
                      }
                    }
                  }
                }
              ]
            }
          }
        ],
        "jss-graphql-layout": []
      }
    }
  }
}

const editor = {
  "sitecore": {
    "context": {
      "pageEditing": true,
      "user": {
        "domain": "sitecore",
        "name": "admin"
      },
      "site": {
        "name": "website"
      },
      "pageState": "edit",
      "language": "en"
    },
    "route": {
      "name": "styleguide",
      "displayName": "styleguide",
      "fields": {
        "pageTitle": {
          "value": "Styleguide | Sitecore JSS",
          "editable": "<input id='fld_302C626E8ADA516AAA71B8D5D4E96B3C_CF295A16CCA850C7BBBC3822BC092FB0_en_1_9558494f2d9948129fbefb564a000ce6_443' class='scFieldValue' name='fld_302C626E8ADA516AAA71B8D5D4E96B3C_CF295A16CCA850C7BBBC3822BC092FB0_en_1_9558494f2d9948129fbefb564a000ce6_443' type='hidden' value=\"Styleguide | Sitecore JSS\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{302C626E-8ADA-516A-AA71-B8D5D4E96B3C}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"Page Title\",\"expandedDisplayName\":null}</span><span id=\"fld_302C626E8ADA516AAA71B8D5D4E96B3C_CF295A16CCA850C7BBBC3822BC092FB0_en_1_9558494f2d9948129fbefb564a000ce6_443_edit\" sc_parameters=\"prevent-line-break=true\" contenteditable=\"true\" class=\"scWebEditInput\" scFieldType=\"single-line text\" scDefaultText=\"[No text in field]\">Styleguide | Sitecore JSS</span>"
        }
      },
      "databaseName": "master",
      "deviceId": "fe5d7fdf-89c0-4d99-9aa3-b5fbd009c9f3",
      "itemId": "302c626e-8ada-516a-aa71-b8d5d4e96b3c",
      "itemLanguage": "en",
      "itemVersion": 1,
      "layoutId": "f0b1e363-81bd-5edb-8754-ef9cd976b8c7",
      "templateId": "e8eb771a-4fd1-5c70-810d-41098d5b650c",
      "templateName": "App Route",
      "placeholders": {
        "jss-main": [
          {
            "name": "code",
            "type": "text/sitecore",
            "contents": "{\"commands\":[{\"click\":\"chrome:placeholder:addControl\",\"header\":\"Add to here\",\"icon\":\"/temp/iconcache/office/16x16/add.png\",\"disabledIcon\":\"/temp/add_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Add a new rendering to the '{0}' placeholder.\",\"type\":\"\"},{\"click\":\"chrome:placeholder:editSettings\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/window_gear.png\",\"disabledIcon\":\"/temp/window_gear_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the placeholder settings.\",\"type\":\"\"}],\"contextItemUri\":\"sitecore://master/{302C626E-8ADA-516A-AA71-B8D5D4E96B3C}?lang=en&ver=1\",\"custom\":{\"allowedRenderings\":[\"49C9C9892F425AE299F5B7DD1F2A7D20\",\"25833F66195F5675A46E96C26F5DDD90\",\"D24F19E40FB65E21B89DDA5D09F7A24C\",\"199015FF035F5186841F54554148E9A7\"],\"editable\":\"true\"},\"displayName\":\"Main\",\"expandedDisplayName\":null}",
            "attributes": {
              "type": "text/sitecore",
              "chrometype": "placeholder",
              "kind": "open",
              "id": "jss_main",
              "key": "jss-main",
              "class": "scpm",
              "data-selectable": "true"
            }
          },
          {
            "name": "code",
            "type": "text/sitecore",
            "contents": "{\"commands\":[{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:fieldeditor(command={2F7D34E2-EB93-47ED-8177-B1F429A9FCD7},fields=heading|content,id={28063B57-A5F6-5027-90E5-9D615AF8DB7A})',null,false)\",\"header\":\"Edit Fields\",\"icon\":\"/temp/iconcache/office/16x16/pencil.png\",\"disabledIcon\":\"/temp/pencil_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"\",\"type\":null},{\"click\":\"chrome:dummy\",\"header\":\"Separator\",\"icon\":\"\",\"disabledIcon\":\"\",\"isDivider\":false,\"tooltip\":null,\"type\":\"separator\"},{\"click\":\"chrome:rendering:sort\",\"header\":\"Change position\",\"icon\":\"/temp/iconcache/office/16x16/document_size.png\",\"disabledIcon\":\"/temp/document_size_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Move component.\",\"type\":\"\"},{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:componentoptions(referenceId={E02DDB9B-A062-5E50-924A-1940D7E053CE},renderingId={49C9C989-2F42-5AE2-99F5-B7DD1F2A7D20},id={28063B57-A5F6-5027-90E5-9D615AF8DB7A})',null,false)\",\"header\":\"Edit Experience Editor Options\",\"icon\":\"/temp/iconcache/office/16x16/clipboard_check_edit.png\",\"disabledIcon\":\"/temp/clipboard_check_edit_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the Experience Editor options for the component.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:properties\",\"header\":\"Edit component properties\",\"icon\":\"/temp/iconcache/office/16x16/elements_branch.png\",\"disabledIcon\":\"/temp/elements_branch_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the properties for the component.\",\"type\":\"common\"},{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:setdatasource(referenceId={E02DDB9B-A062-5E50-924A-1940D7E053CE},renderingId={49C9C989-2F42-5AE2-99F5-B7DD1F2A7D20},id={28063B57-A5F6-5027-90E5-9D615AF8DB7A})',null,false)\",\"header\":\"dsHeaderParameter\",\"icon\":\"/temp/iconcache/office/16x16/data.png\",\"disabledIcon\":\"/temp/data_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"dsTooltipParameter\",\"type\":\"datasourcesmenu\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Test the component.\",\"type\":\"sticky\"},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"datasourcesmenu\"},{\"click\":\"chrome:rendering:delete\",\"header\":\"Delete\",\"icon\":\"/temp/iconcache/office/16x16/delete.png\",\"disabledIcon\":\"/temp/delete_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Remove component.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{28063B57-A5F6-5027-90E5-9D615AF8DB7A}?lang=en&ver=1\",\"custom\":{\"renderingID\":\"49C9C9892F425AE299F5B7DD1F2A7D20\",\"editable\":\"true\"},\"displayName\":\"Content Block\",\"expandedDisplayName\":null}",
            "attributes": {
              "type": "text/sitecore",
              "chrometype": "rendering",
              "kind": "open",
              "hintname": "Content Block",
              "id": "r_E02DDB9BA0625E50924A1940D7E053CE",
              "class": "scpm",
              "data-selectable": "true"
            }
          },
          {
            "uid": "e02ddb9b-a062-5e50-924a-1940d7e053ce",
            "componentName": "ContentBlock",
            "dataSource": "{28063B57-A5F6-5027-90E5-9D615AF8DB7A}",
            "params": {},
            "fields": {
              "heading": {
                "value": "JSS Styleguide",
                "editable": "<input id='fld_28063B57A5F6502790E59D615AF8DB7A_3D6144C71C905864AB9732B1F0537ED7_en_1_6dd84d0c040245d19f45bb2c72b1c732_444' class='scFieldValue' name='fld_28063B57A5F6502790E59D615AF8DB7A_3D6144C71C905864AB9732B1F0537ED7_en_1_6dd84d0c040245d19f45bb2c72b1c732_444' type='hidden' value=\"JSS Styleguide\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{28063B57-A5F6-5027-90E5-9D615AF8DB7A}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"heading\",\"expandedDisplayName\":null}</span><span id=\"fld_28063B57A5F6502790E59D615AF8DB7A_3D6144C71C905864AB9732B1F0537ED7_en_1_6dd84d0c040245d19f45bb2c72b1c732_444_edit\" sc_parameters=\"prevent-line-break=true\" contenteditable=\"true\" class=\"scWebEditInput\" scFieldType=\"single-line text\" scDefaultText=\"[No text in field]\">JSS Styleguide</span>"
              },
              "content": {
                "value": "<p>This is a live set of examples of how to use JSS. For more information on using JSS, please see <a href=\"https://jss.sitecore.net\" target=\"_blank\" rel=\"noopener noreferrer\">the documentation</a>.</p>\n<p>The content and layout of this page is defined in <code>/data/routes/styleguide/en.yml</code></p>\n",
                "editable": "<input id='fld_28063B57A5F6502790E59D615AF8DB7A_764D22AA6E7950EB9D58FF0568203F48_en_1_6dd84d0c040245d19f45bb2c72b1c732_445' class='scFieldValue' name='fld_28063B57A5F6502790E59D615AF8DB7A_764D22AA6E7950EB9D58FF0568203F48_en_1_6dd84d0c040245d19f45bb2c72b1c732_445' type='hidden' value=\"&lt;p&gt;This is a live set of examples of how to use JSS. For more information on using JSS, please see &lt;a href=&quot;https://jss.sitecore.net&quot; target=&quot;_blank&quot; rel=&quot;noopener noreferrer&quot;&gt;the documentation&lt;/a&gt;.&lt;/p&gt;\n&lt;p&gt;The content and layout of this page is defined in &lt;code&gt;/data/routes/styleguide/en.yml&lt;/code&gt;&lt;/p&gt;\n\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:field:editcontrol({command:\\\"webedit:edithtml\\\"})\",\"header\":\"Edit Text\",\"icon\":\"/temp/iconcache/office/16x16/pencil.png\",\"disabledIcon\":\"/temp/pencil_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the text\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"bold\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_bold.png\",\"disabledIcon\":\"/temp/font_style_bold_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Bold\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"Italic\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_italics.png\",\"disabledIcon\":\"/temp/font_style_italics_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Italic\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"Underline\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_underline.png\",\"disabledIcon\":\"/temp/font_style_underline_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Underline\",\"type\":null},{\"click\":\"chrome:field:insertexternallink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/earth_link.png\",\"disabledIcon\":\"/temp/earth_link_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert an external link into the text field.\",\"type\":null},{\"click\":\"chrome:field:insertlink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/link.png\",\"disabledIcon\":\"/temp/link_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert a link into the text field.\",\"type\":null},{\"click\":\"chrome:field:removelink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/link_broken.png\",\"disabledIcon\":\"/temp/link_broken_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Remove link.\",\"type\":null},{\"click\":\"chrome:field:insertimage\",\"header\":\"Insert image\",\"icon\":\"/temp/iconcache/office/16x16/photo_landscape.png\",\"disabledIcon\":\"/temp/photo_landscape_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert an image into the text field.\",\"type\":null},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{28063B57-A5F6-5027-90E5-9D615AF8DB7A}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"content\",\"expandedDisplayName\":null}</span><span scFieldType=\"rich text\" scDefaultText=\"[No text in field]\" contenteditable=\"true\" class=\"scWebEditInput\" id=\"fld_28063B57A5F6502790E59D615AF8DB7A_764D22AA6E7950EB9D58FF0568203F48_en_1_6dd84d0c040245d19f45bb2c72b1c732_445_edit\"><p>This is a live set of examples of how to use JSS. For more information on using JSS, please see <a href=\"https://jss.sitecore.net\" target=\"_blank\" rel=\"noopener noreferrer\">the documentation</a>.</p>\n<p>The content and layout of this page is defined in <code>/data/routes/styleguide/en.yml</code></p>\n</span>"
              }
            }
          },
          {
            "name": "code",
            "type": "text/sitecore",
            "contents": "",
            "attributes": {
              "type": "text/sitecore",
              "id": "scEnclosingTag_r_",
              "chrometype": "rendering",
              "kind": "close",
              "hintkey": "Content Block",
              "class": "scpm"
            }
          },
          {
            "name": "code",
            "type": "text/sitecore",
            "contents": "{\"commands\":[{\"click\":\"chrome:rendering:sort\",\"header\":\"Change position\",\"icon\":\"/temp/iconcache/office/16x16/document_size.png\",\"disabledIcon\":\"/temp/document_size_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Move component.\",\"type\":\"\"},{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:componentoptions(referenceId={34A6553C-81DE-5CD3-989E-853F6CB6DF8C},renderingId={25833F66-195F-5675-A46E-96C26F5DDD90},id={302C626E-8ADA-516A-AA71-B8D5D4E96B3C})',null,false)\",\"header\":\"Edit Experience Editor Options\",\"icon\":\"/temp/iconcache/office/16x16/clipboard_check_edit.png\",\"disabledIcon\":\"/temp/clipboard_check_edit_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the Experience Editor options for the component.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:properties\",\"header\":\"Edit component properties\",\"icon\":\"/temp/iconcache/office/16x16/elements_branch.png\",\"disabledIcon\":\"/temp/elements_branch_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the properties for the component.\",\"type\":\"common\"},{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:setdatasource(referenceId={34A6553C-81DE-5CD3-989E-853F6CB6DF8C},renderingId={25833F66-195F-5675-A46E-96C26F5DDD90},id={302C626E-8ADA-516A-AA71-B8D5D4E96B3C})',null,false)\",\"header\":\"dsHeaderParameter\",\"icon\":\"/temp/iconcache/office/16x16/data.png\",\"disabledIcon\":\"/temp/data_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"dsTooltipParameter\",\"type\":\"datasourcesmenu\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Test the component.\",\"type\":\"sticky\"},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"datasourcesmenu\"},{\"click\":\"chrome:rendering:delete\",\"header\":\"Delete\",\"icon\":\"/temp/iconcache/office/16x16/delete.png\",\"disabledIcon\":\"/temp/delete_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Remove component.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{302C626E-8ADA-516A-AA71-B8D5D4E96B3C}?lang=en&ver=1\",\"custom\":{\"renderingID\":\"25833F66195F5675A46E96C26F5DDD90\",\"editable\":\"true\"},\"displayName\":\"Styleguide-Layout\",\"expandedDisplayName\":null}",
            "attributes": {
              "type": "text/sitecore",
              "chrometype": "rendering",
              "kind": "open",
              "hintname": "Styleguide-Layout",
              "id": "r_34A6553C81DE5CD3989E853F6CB6DF8C",
              "class": "scpm",
              "data-selectable": "true"
            }
          },
          {
            "uid": "34a6553c-81de-5cd3-989e-853f6cb6df8c",
            "componentName": "Styleguide-Layout",
            "dataSource": "",
            "params": {},
            "placeholders": {
              "jss-styleguide-layout": [
                {
                  "name": "code",
                  "type": "text/sitecore",
                  "contents": "{\"commands\":[{\"click\":\"chrome:placeholder:addControl\",\"header\":\"Add to here\",\"icon\":\"/temp/iconcache/office/16x16/add.png\",\"disabledIcon\":\"/temp/add_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Add a new rendering to the '{0}' placeholder.\",\"type\":\"\"},{\"click\":\"chrome:placeholder:editSettings\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/window_gear.png\",\"disabledIcon\":\"/temp/window_gear_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the placeholder settings.\",\"type\":\"\"}],\"contextItemUri\":\"sitecore://master/{302C626E-8ADA-516A-AA71-B8D5D4E96B3C}?lang=en&ver=1\",\"custom\":{\"allowedRenderings\":[\"DADE61AE8C275EC1A5B1B02CC436C4E1\"],\"editable\":\"true\"},\"displayName\":\"jss-styleguide-layout\",\"expandedDisplayName\":null}",
                  "attributes": {
                    "type": "text/sitecore",
                    "chrometype": "placeholder",
                    "kind": "open",
                    "id": "_jss_main_jss_styleguide_layout__34A6553C_81DE_5CD3_989E_853F6CB6DF8C__0",
                    "key": "/jss-main/jss-styleguide-layout-{34A6553C-81DE-5CD3-989E-853F6CB6DF8C}-0",
                    "class": "scpm",
                    "data-selectable": "true"
                  }
                },
                {
                  "name": "code",
                  "type": "text/sitecore",
                  "contents": "{\"commands\":[{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:fieldeditor(command={2F7D34E2-EB93-47ED-8177-B1F429A9FCD7},fields=heading,id={CF9E5B9B-1193-5153-891D-4265DEC8A9CB})',null,false)\",\"header\":\"Edit Fields\",\"icon\":\"/temp/iconcache/office/16x16/pencil.png\",\"disabledIcon\":\"/temp/pencil_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"\",\"type\":null},{\"click\":\"chrome:dummy\",\"header\":\"Separator\",\"icon\":\"\",\"disabledIcon\":\"\",\"isDivider\":false,\"tooltip\":null,\"type\":\"separator\"},{\"click\":\"chrome:rendering:sort\",\"header\":\"Change position\",\"icon\":\"/temp/iconcache/office/16x16/document_size.png\",\"disabledIcon\":\"/temp/document_size_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Move component.\",\"type\":\"\"},{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:componentoptions(referenceId={B7C779DA-2B75-586C-B40D-081FCB864256},renderingId={DADE61AE-8C27-5EC1-A5B1-B02CC436C4E1},id={CF9E5B9B-1193-5153-891D-4265DEC8A9CB})',null,false)\",\"header\":\"Edit Experience Editor Options\",\"icon\":\"/temp/iconcache/office/16x16/clipboard_check_edit.png\",\"disabledIcon\":\"/temp/clipboard_check_edit_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the Experience Editor options for the component.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:properties\",\"header\":\"Edit component properties\",\"icon\":\"/temp/iconcache/office/16x16/elements_branch.png\",\"disabledIcon\":\"/temp/elements_branch_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the properties for the component.\",\"type\":\"common\"},{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:setdatasource(referenceId={B7C779DA-2B75-586C-B40D-081FCB864256},renderingId={DADE61AE-8C27-5EC1-A5B1-B02CC436C4E1},id={CF9E5B9B-1193-5153-891D-4265DEC8A9CB})',null,false)\",\"header\":\"dsHeaderParameter\",\"icon\":\"/temp/iconcache/office/16x16/data.png\",\"disabledIcon\":\"/temp/data_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"dsTooltipParameter\",\"type\":\"datasourcesmenu\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Test the component.\",\"type\":\"sticky\"},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"datasourcesmenu\"},{\"click\":\"chrome:rendering:delete\",\"header\":\"Delete\",\"icon\":\"/temp/iconcache/office/16x16/delete.png\",\"disabledIcon\":\"/temp/delete_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Remove component.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{CF9E5B9B-1193-5153-891D-4265DEC8A9CB}?lang=en&ver=1\",\"custom\":{\"renderingID\":\"DADE61AE8C275EC1A5B1B02CC436C4E1\",\"editable\":\"true\"},\"displayName\":\"Styleguide-Section\",\"expandedDisplayName\":null}",
                  "attributes": {
                    "type": "text/sitecore",
                    "chrometype": "rendering",
                    "kind": "open",
                    "hintname": "Styleguide-Section",
                    "id": "r_B7C779DA2B75586CB40D081FCB864256",
                    "class": "scpm",
                    "data-selectable": "true"
                  }
                },
                {
                  "uid": "b7c779da-2b75-586c-b40d-081fcb864256",
                  "componentName": "Styleguide-Section",
                  "dataSource": "{CF9E5B9B-1193-5153-891D-4265DEC8A9CB}",
                  "params": {},
                  "fields": {
                    "heading": {
                      "value": "Content Data",
                      "editable": "<input id='fld_CF9E5B9B11935153891D4265DEC8A9CB_51D3C9FDEB395D6096E6EF8FB686BC75_en_1_33e2f8351a6e4c8b85b6e01f356ff56e_495' class='scFieldValue' name='fld_CF9E5B9B11935153891D4265DEC8A9CB_51D3C9FDEB395D6096E6EF8FB686BC75_en_1_33e2f8351a6e4c8b85b6e01f356ff56e_495' type='hidden' value=\"Content Data\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{CF9E5B9B-1193-5153-891D-4265DEC8A9CB}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"heading\",\"expandedDisplayName\":null}</span><span id=\"fld_CF9E5B9B11935153891D4265DEC8A9CB_51D3C9FDEB395D6096E6EF8FB686BC75_en_1_33e2f8351a6e4c8b85b6e01f356ff56e_495_edit\" sc_parameters=\"prevent-line-break=true\" contenteditable=\"true\" class=\"scWebEditInput\" scFieldType=\"single-line text\" scDefaultText=\"[No text in field]\">Content Data</span>"
                    }
                  },
                  "placeholders": {
                    "jss-styleguide-section": [
                      {
                        "name": "code",
                        "type": "text/sitecore",
                        "contents": "{\"commands\":[{\"click\":\"chrome:placeholder:addControl\",\"header\":\"Add to here\",\"icon\":\"/temp/iconcache/office/16x16/add.png\",\"disabledIcon\":\"/temp/add_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Add a new rendering to the '{0}' placeholder.\",\"type\":\"\"},{\"click\":\"chrome:placeholder:editSettings\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/window_gear.png\",\"disabledIcon\":\"/temp/window_gear_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the placeholder settings.\",\"type\":\"\"}],\"contextItemUri\":\"sitecore://master/{302C626E-8ADA-516A-AA71-B8D5D4E96B3C}?lang=en&ver=1\",\"custom\":{\"allowedRenderings\":[\"0B82E18FF08856D3BC52C4D054C4B218\",\"DAB81669A0825E998D52717D4EF215F8\",\"862F358DBB635D50ABDBE01FBD771B7F\",\"7CC402CBA3945DAAB45F32D5FCDFFF6B\",\"7C0EF2D278095B2E84221C677744445F\",\"3127DF7E6ED652DFBC2DFA4CDC65C9A6\",\"200369E789C4531BB076C53AB1C50154\",\"2D83A15B7EEF52C1BBA2EAB2D742D952\",\"0B711DC99DD2568AAECBD75DBA2F8DCD\",\"5E747641F9A457EEB62573D976639071\",\"5EB0D6136DAF5CAA8F565175867EC5BC\",\"DAC9212F32495BD883D9DC2D0F5F16F0\",\"FEACAB92BAD0551D914D19DFD91B1565\",\"6879CB46822C5F9E987B1041537629D1\",\"7F7E9A4D8FA952C18F87860195CB2C31\",\"690D327E36385C259E7F959A4D64E339\",\"2ABC73F365E85301A06165CF10397070\",\"4D4F0795D9B45A03BB91138B1BAE8A5F\"],\"editable\":\"true\"},\"displayName\":\"jss-styleguide-section\",\"expandedDisplayName\":null}",
                        "attributes": {
                          "type": "text/sitecore",
                          "chrometype": "placeholder",
                          "kind": "open",
                          "id": "_jss_main_jss_styleguide_layout__34A6553C_81DE_5CD3_989E_853F6CB6DF8C__0_jss_styleguide_section__B7C779DA_2B75_586C_B40D_081FCB864256__0",
                          "key": "/jss-main/jss-styleguide-layout-{34A6553C-81DE-5CD3-989E-853F6CB6DF8C}-0/jss-styleguide-section-{B7C779DA-2B75-586C-B40D-081FCB864256}-0",
                          "class": "scpm",
                          "data-selectable": "true"
                        }
                      },
                      {
                        "name": "code",
                        "type": "text/sitecore",
                        "contents": "{\"commands\":[{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:fieldeditor(command={2F7D34E2-EB93-47ED-8177-B1F429A9FCD7},fields=sample|sample2|heading|description,id={F5ADC3DC-78A9-5E90-B617-4EC532DB1B58})',null,false)\",\"header\":\"Edit Fields\",\"icon\":\"/temp/iconcache/office/16x16/pencil.png\",\"disabledIcon\":\"/temp/pencil_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"\",\"type\":null},{\"click\":\"chrome:dummy\",\"header\":\"Separator\",\"icon\":\"\",\"disabledIcon\":\"\",\"isDivider\":false,\"tooltip\":null,\"type\":\"separator\"},{\"click\":\"chrome:rendering:sort\",\"header\":\"Change position\",\"icon\":\"/temp/iconcache/office/16x16/document_size.png\",\"disabledIcon\":\"/temp/document_size_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Move component.\",\"type\":\"\"},{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:componentoptions(referenceId={63B0C99E-DAC7-5670-9D66-C26A78000EAE},renderingId={0B82E18F-F088-56D3-BC52-C4D054C4B218},id={F5ADC3DC-78A9-5E90-B617-4EC532DB1B58})',null,false)\",\"header\":\"Edit Experience Editor Options\",\"icon\":\"/temp/iconcache/office/16x16/clipboard_check_edit.png\",\"disabledIcon\":\"/temp/clipboard_check_edit_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the Experience Editor options for the component.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:properties\",\"header\":\"Edit component properties\",\"icon\":\"/temp/iconcache/office/16x16/elements_branch.png\",\"disabledIcon\":\"/temp/elements_branch_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the properties for the component.\",\"type\":\"common\"},{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:setdatasource(referenceId={63B0C99E-DAC7-5670-9D66-C26A78000EAE},renderingId={0B82E18F-F088-56D3-BC52-C4D054C4B218},id={F5ADC3DC-78A9-5E90-B617-4EC532DB1B58})',null,false)\",\"header\":\"dsHeaderParameter\",\"icon\":\"/temp/iconcache/office/16x16/data.png\",\"disabledIcon\":\"/temp/data_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"dsTooltipParameter\",\"type\":\"datasourcesmenu\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Test the component.\",\"type\":\"sticky\"},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"datasourcesmenu\"},{\"click\":\"chrome:rendering:delete\",\"header\":\"Delete\",\"icon\":\"/temp/iconcache/office/16x16/delete.png\",\"disabledIcon\":\"/temp/delete_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Remove component.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{F5ADC3DC-78A9-5E90-B617-4EC532DB1B58}?lang=en&ver=1\",\"custom\":{\"renderingID\":\"0B82E18FF08856D3BC52C4D054C4B218\",\"editable\":\"true\"},\"displayName\":\"Styleguide-FieldUsage-Text\",\"expandedDisplayName\":null}",
                        "attributes": {
                          "type": "text/sitecore",
                          "chrometype": "rendering",
                          "kind": "open",
                          "hintname": "Styleguide-FieldUsage-Text",
                          "id": "r_63B0C99EDAC756709D66C26A78000EAE",
                          "class": "scpm",
                          "data-selectable": "true"
                        }
                      },
                      {
                        "uid": "63b0c99e-dac7-5670-9d66-c26a78000eae",
                        "componentName": "Styleguide-FieldUsage-Text",
                        "dataSource": "{F5ADC3DC-78A9-5E90-B617-4EC532DB1B58}",
                        "params": {},
                        "fields": {
                          "sample": {
                            "value": "This is a sample text field. <mark>HTML is encoded.</mark> In Sitecore, editors will see a <input type=\"text\">.",
                            "editable": "<input id='fld_F5ADC3DC78A95E90B6174EC532DB1B58_8D3019B8F23052548F8E166E6675EE2F_en_1_31e6c9fda1c04733a9716a1f5edef0e8_446' class='scFieldValue' name='fld_F5ADC3DC78A95E90B6174EC532DB1B58_8D3019B8F23052548F8E166E6675EE2F_en_1_31e6c9fda1c04733a9716a1f5edef0e8_446' type='hidden' value=\"This is a sample text field. &lt;mark&gt;HTML is encoded.&lt;/mark&gt; In Sitecore, editors will see a &lt;input type=&quot;text&quot;&gt;.\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{F5ADC3DC-78A9-5E90-B617-4EC532DB1B58}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"sample\",\"expandedDisplayName\":null}</span><span id=\"fld_F5ADC3DC78A95E90B6174EC532DB1B58_8D3019B8F23052548F8E166E6675EE2F_en_1_31e6c9fda1c04733a9716a1f5edef0e8_446_edit\" sc_parameters=\"prevent-line-break=true\" contenteditable=\"true\" class=\"scWebEditInput\" scFieldType=\"single-line text\" scDefaultText=\"[No text in field]\">This is a sample text field. &lt;mark&gt;HTML is encoded.&lt;/mark&gt; In Sitecore, editors will see a &lt;input type=&quot;text&quot;&gt;.</span>"
                          },
                          "sample2": {
                            "value": "This is another sample text field using rendering options. <mark>HTML supported with encode=false.</mark> Cannot edit because editable=false.",
                            "editable": "<input id='fld_F5ADC3DC78A95E90B6174EC532DB1B58_A06470544D5458F38B873FBAE194A3EB_en_1_31e6c9fda1c04733a9716a1f5edef0e8_447' class='scFieldValue' name='fld_F5ADC3DC78A95E90B6174EC532DB1B58_A06470544D5458F38B873FBAE194A3EB_en_1_31e6c9fda1c04733a9716a1f5edef0e8_447' type='hidden' value=\"This is another sample text field using rendering options. &lt;mark&gt;HTML supported with encode=false.&lt;/mark&gt; Cannot edit because editable=false.\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{F5ADC3DC-78A9-5E90-B617-4EC532DB1B58}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"Customize Name Shown in Sitecore\",\"expandedDisplayName\":null}</span><span id=\"fld_F5ADC3DC78A95E90B6174EC532DB1B58_A06470544D5458F38B873FBAE194A3EB_en_1_31e6c9fda1c04733a9716a1f5edef0e8_447_edit\" sc_parameters=\"prevent-line-break=true\" contenteditable=\"true\" class=\"scWebEditInput\" scFieldType=\"single-line text\" scDefaultText=\"[No text in field]\">This is another sample text field using rendering options. &lt;mark&gt;HTML supported with encode=false.&lt;/mark&gt; Cannot edit because editable=false.</span>"
                          },
                          "heading": {
                            "value": "Single-Line Text",
                            "editable": "<input id='fld_F5ADC3DC78A95E90B6174EC532DB1B58_2EF2548084095250A76CC3D867340008_en_1_31e6c9fda1c04733a9716a1f5edef0e8_448' class='scFieldValue' name='fld_F5ADC3DC78A95E90B6174EC532DB1B58_2EF2548084095250A76CC3D867340008_en_1_31e6c9fda1c04733a9716a1f5edef0e8_448' type='hidden' value=\"Single-Line Text\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{F5ADC3DC-78A9-5E90-B617-4EC532DB1B58}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"heading\",\"expandedDisplayName\":null}</span><span id=\"fld_F5ADC3DC78A95E90B6174EC532DB1B58_2EF2548084095250A76CC3D867340008_en_1_31e6c9fda1c04733a9716a1f5edef0e8_448_edit\" sc_parameters=\"prevent-line-break=true\" contenteditable=\"true\" class=\"scWebEditInput\" scFieldType=\"single-line text\" scDefaultText=\"[No text in field]\">Single-Line Text</span>"
                          },
                          "description": {
                            "value": "",
                            "editable": "<input id='fld_F5ADC3DC78A95E90B6174EC532DB1B58_439F9C97BCD65D8FBE16CFECCC159EFB_en_1_31e6c9fda1c04733a9716a1f5edef0e8_449' class='scFieldValue' name='fld_F5ADC3DC78A95E90B6174EC532DB1B58_439F9C97BCD65D8FBE16CFECCC159EFB_en_1_31e6c9fda1c04733a9716a1f5edef0e8_449' type='hidden' value=\"\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:field:editcontrol({command:\\\"webedit:edithtml\\\"})\",\"header\":\"Edit Text\",\"icon\":\"/temp/iconcache/office/16x16/pencil.png\",\"disabledIcon\":\"/temp/pencil_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the text\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"bold\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_bold.png\",\"disabledIcon\":\"/temp/font_style_bold_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Bold\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"Italic\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_italics.png\",\"disabledIcon\":\"/temp/font_style_italics_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Italic\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"Underline\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_underline.png\",\"disabledIcon\":\"/temp/font_style_underline_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Underline\",\"type\":null},{\"click\":\"chrome:field:insertexternallink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/earth_link.png\",\"disabledIcon\":\"/temp/earth_link_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert an external link into the text field.\",\"type\":null},{\"click\":\"chrome:field:insertlink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/link.png\",\"disabledIcon\":\"/temp/link_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert a link into the text field.\",\"type\":null},{\"click\":\"chrome:field:removelink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/link_broken.png\",\"disabledIcon\":\"/temp/link_broken_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Remove link.\",\"type\":null},{\"click\":\"chrome:field:insertimage\",\"header\":\"Insert image\",\"icon\":\"/temp/iconcache/office/16x16/photo_landscape.png\",\"disabledIcon\":\"/temp/photo_landscape_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert an image into the text field.\",\"type\":null},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{F5ADC3DC-78A9-5E90-B617-4EC532DB1B58}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"description\",\"expandedDisplayName\":null}</span><span id=\"fld_F5ADC3DC78A95E90B6174EC532DB1B58_439F9C97BCD65D8FBE16CFECCC159EFB_en_1_31e6c9fda1c04733a9716a1f5edef0e8_449_edit\" contenteditable=\"true\" class=\"scWebEditInput\" scFieldType=\"rich text\" scDefaultText=\"[No text in field]\" scWatermark=\"true\">[No text in field]</span>"
                          }
                        }
                      },
                      {
                        "name": "code",
                        "type": "text/sitecore",
                        "contents": "",
                        "attributes": {
                          "type": "text/sitecore",
                          "id": "scEnclosingTag_r_",
                          "chrometype": "rendering",
                          "kind": "close",
                          "hintkey": "Styleguide-FieldUsage-Text",
                          "class": "scpm"
                        }
                      },
                      {
                        "name": "code",
                        "type": "text/sitecore",
                        "contents": "{\"commands\":[{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:fieldeditor(command={2F7D34E2-EB93-47ED-8177-B1F429A9FCD7},fields=sample|sample2|heading|description,id={01AAAD8E-E9A1-5274-8D17-D515CAF1906F})',null,false)\",\"header\":\"Edit Fields\",\"icon\":\"/temp/iconcache/office/16x16/pencil.png\",\"disabledIcon\":\"/temp/pencil_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"\",\"type\":null},{\"click\":\"chrome:dummy\",\"header\":\"Separator\",\"icon\":\"\",\"disabledIcon\":\"\",\"isDivider\":false,\"tooltip\":null,\"type\":\"separator\"},{\"click\":\"chrome:rendering:sort\",\"header\":\"Change position\",\"icon\":\"/temp/iconcache/office/16x16/document_size.png\",\"disabledIcon\":\"/temp/document_size_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Move component.\",\"type\":\"\"},{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:componentoptions(referenceId={F1EA3BB5-1175-5055-AB11-9C48BF69427A},renderingId={0B82E18F-F088-56D3-BC52-C4D054C4B218},id={01AAAD8E-E9A1-5274-8D17-D515CAF1906F})',null,false)\",\"header\":\"Edit Experience Editor Options\",\"icon\":\"/temp/iconcache/office/16x16/clipboard_check_edit.png\",\"disabledIcon\":\"/temp/clipboard_check_edit_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the Experience Editor options for the component.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:properties\",\"header\":\"Edit component properties\",\"icon\":\"/temp/iconcache/office/16x16/elements_branch.png\",\"disabledIcon\":\"/temp/elements_branch_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the properties for the component.\",\"type\":\"common\"},{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:setdatasource(referenceId={F1EA3BB5-1175-5055-AB11-9C48BF69427A},renderingId={0B82E18F-F088-56D3-BC52-C4D054C4B218},id={01AAAD8E-E9A1-5274-8D17-D515CAF1906F})',null,false)\",\"header\":\"dsHeaderParameter\",\"icon\":\"/temp/iconcache/office/16x16/data.png\",\"disabledIcon\":\"/temp/data_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"dsTooltipParameter\",\"type\":\"datasourcesmenu\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Test the component.\",\"type\":\"sticky\"},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"datasourcesmenu\"},{\"click\":\"chrome:rendering:delete\",\"header\":\"Delete\",\"icon\":\"/temp/iconcache/office/16x16/delete.png\",\"disabledIcon\":\"/temp/delete_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Remove component.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{01AAAD8E-E9A1-5274-8D17-D515CAF1906F}?lang=en&ver=1\",\"custom\":{\"renderingID\":\"0B82E18FF08856D3BC52C4D054C4B218\",\"editable\":\"true\"},\"displayName\":\"Styleguide-FieldUsage-Text\",\"expandedDisplayName\":null}",
                        "attributes": {
                          "type": "text/sitecore",
                          "chrometype": "rendering",
                          "kind": "open",
                          "hintname": "Styleguide-FieldUsage-Text",
                          "id": "r_F1EA3BB511755055AB119C48BF69427A",
                          "class": "scpm",
                          "data-selectable": "true"
                        }
                      },
                      {
                        "uid": "f1ea3bb5-1175-5055-ab11-9c48bf69427a",
                        "componentName": "Styleguide-FieldUsage-Text",
                        "dataSource": "{01AAAD8E-E9A1-5274-8D17-D515CAF1906F}",
                        "params": {},
                        "fields": {
                          "sample": {
                            "value": "This is a sample multi-line text field. <mark>HTML is encoded.</mark> In Sitecore, editors will see a textarea.",
                            "editable": "<input id='fld_01AAAD8EE9A152748D17D515CAF1906F_8D3019B8F23052548F8E166E6675EE2F_en_1_58fbf945c72a407c8c8a44317c7ee9b0_450' class='scFieldValue' name='fld_01AAAD8EE9A152748D17D515CAF1906F_8D3019B8F23052548F8E166E6675EE2F_en_1_58fbf945c72a407c8c8a44317c7ee9b0_450' type='hidden' value=\"This is a sample multi-line text field. &lt;mark&gt;HTML is encoded.&lt;/mark&gt; In Sitecore, editors will see a textarea.\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{01AAAD8E-E9A1-5274-8D17-D515CAF1906F}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"sample\",\"expandedDisplayName\":null}</span><span id=\"fld_01AAAD8EE9A152748D17D515CAF1906F_8D3019B8F23052548F8E166E6675EE2F_en_1_58fbf945c72a407c8c8a44317c7ee9b0_450_edit\" sc_parameters=\"prevent-line-break=true\" contenteditable=\"true\" class=\"scWebEditInput\" scFieldType=\"single-line text\" scDefaultText=\"[No text in field]\">This is a sample multi-line text field. &lt;mark&gt;HTML is encoded.&lt;/mark&gt; In Sitecore, editors will see a textarea.</span>"
                          },
                          "sample2": {
                            "value": "This is another sample multi-line text field using rendering options. <mark>HTML supported with encode=false.</mark>",
                            "editable": "<input id='fld_01AAAD8EE9A152748D17D515CAF1906F_A06470544D5458F38B873FBAE194A3EB_en_1_58fbf945c72a407c8c8a44317c7ee9b0_451' class='scFieldValue' name='fld_01AAAD8EE9A152748D17D515CAF1906F_A06470544D5458F38B873FBAE194A3EB_en_1_58fbf945c72a407c8c8a44317c7ee9b0_451' type='hidden' value=\"This is another sample multi-line text field using rendering options. &lt;mark&gt;HTML supported with encode=false.&lt;/mark&gt;\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{01AAAD8E-E9A1-5274-8D17-D515CAF1906F}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"Customize Name Shown in Sitecore\",\"expandedDisplayName\":null}</span><span id=\"fld_01AAAD8EE9A152748D17D515CAF1906F_A06470544D5458F38B873FBAE194A3EB_en_1_58fbf945c72a407c8c8a44317c7ee9b0_451_edit\" sc_parameters=\"prevent-line-break=true\" contenteditable=\"true\" class=\"scWebEditInput\" scFieldType=\"single-line text\" scDefaultText=\"[No text in field]\">This is another sample multi-line text field using rendering options. &lt;mark&gt;HTML supported with encode=false.&lt;/mark&gt;</span>"
                          },
                          "heading": {
                            "value": "Multi-Line Text",
                            "editable": "<input id='fld_01AAAD8EE9A152748D17D515CAF1906F_2EF2548084095250A76CC3D867340008_en_1_58fbf945c72a407c8c8a44317c7ee9b0_452' class='scFieldValue' name='fld_01AAAD8EE9A152748D17D515CAF1906F_2EF2548084095250A76CC3D867340008_en_1_58fbf945c72a407c8c8a44317c7ee9b0_452' type='hidden' value=\"Multi-Line Text\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{01AAAD8E-E9A1-5274-8D17-D515CAF1906F}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"heading\",\"expandedDisplayName\":null}</span><span id=\"fld_01AAAD8EE9A152748D17D515CAF1906F_2EF2548084095250A76CC3D867340008_en_1_58fbf945c72a407c8c8a44317c7ee9b0_452_edit\" sc_parameters=\"prevent-line-break=true\" contenteditable=\"true\" class=\"scWebEditInput\" scFieldType=\"single-line text\" scDefaultText=\"[No text in field]\">Multi-Line Text</span>"
                          },
                          "description": {
                            "value": "<small>Multi-line text tells Sitecore to use a <code>textarea</code> for editing; consumption in JSS is the same as single-line text.</small>",
                            "editable": "<input id='fld_01AAAD8EE9A152748D17D515CAF1906F_439F9C97BCD65D8FBE16CFECCC159EFB_en_1_58fbf945c72a407c8c8a44317c7ee9b0_453' class='scFieldValue' name='fld_01AAAD8EE9A152748D17D515CAF1906F_439F9C97BCD65D8FBE16CFECCC159EFB_en_1_58fbf945c72a407c8c8a44317c7ee9b0_453' type='hidden' value=\"&lt;small&gt;Multi-line text tells Sitecore to use a &lt;code&gt;textarea&lt;/code&gt; for editing; consumption in JSS is the same as single-line text.&lt;/small&gt;\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:field:editcontrol({command:\\\"webedit:edithtml\\\"})\",\"header\":\"Edit Text\",\"icon\":\"/temp/iconcache/office/16x16/pencil.png\",\"disabledIcon\":\"/temp/pencil_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the text\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"bold\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_bold.png\",\"disabledIcon\":\"/temp/font_style_bold_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Bold\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"Italic\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_italics.png\",\"disabledIcon\":\"/temp/font_style_italics_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Italic\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"Underline\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_underline.png\",\"disabledIcon\":\"/temp/font_style_underline_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Underline\",\"type\":null},{\"click\":\"chrome:field:insertexternallink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/earth_link.png\",\"disabledIcon\":\"/temp/earth_link_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert an external link into the text field.\",\"type\":null},{\"click\":\"chrome:field:insertlink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/link.png\",\"disabledIcon\":\"/temp/link_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert a link into the text field.\",\"type\":null},{\"click\":\"chrome:field:removelink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/link_broken.png\",\"disabledIcon\":\"/temp/link_broken_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Remove link.\",\"type\":null},{\"click\":\"chrome:field:insertimage\",\"header\":\"Insert image\",\"icon\":\"/temp/iconcache/office/16x16/photo_landscape.png\",\"disabledIcon\":\"/temp/photo_landscape_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert an image into the text field.\",\"type\":null},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{01AAAD8E-E9A1-5274-8D17-D515CAF1906F}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"description\",\"expandedDisplayName\":null}</span><span scFieldType=\"rich text\" scDefaultText=\"[No text in field]\" contenteditable=\"true\" class=\"scWebEditInput\" id=\"fld_01AAAD8EE9A152748D17D515CAF1906F_439F9C97BCD65D8FBE16CFECCC159EFB_en_1_58fbf945c72a407c8c8a44317c7ee9b0_453_edit\"><small>Multi-line text tells Sitecore to use a <code>textarea</code> for editing; consumption in JSS is the same as single-line text.</small></span>"
                          }
                        }
                      },
                      {
                        "name": "code",
                        "type": "text/sitecore",
                        "contents": "",
                        "attributes": {
                          "type": "text/sitecore",
                          "id": "scEnclosingTag_r_",
                          "chrometype": "rendering",
                          "kind": "close",
                          "hintkey": "Styleguide-FieldUsage-Text",
                          "class": "scpm"
                        }
                      },
                      {
                        "name": "code",
                        "type": "text/sitecore",
                        "contents": "{\"commands\":[{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:fieldeditor(command={2F7D34E2-EB93-47ED-8177-B1F429A9FCD7},fields=sample|sample2|heading|description,id={6D5A0DCA-AA96-526D-B77F-9832E4111509})',null,false)\",\"header\":\"Edit Fields\",\"icon\":\"/temp/iconcache/office/16x16/pencil.png\",\"disabledIcon\":\"/temp/pencil_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"\",\"type\":null},{\"click\":\"chrome:dummy\",\"header\":\"Separator\",\"icon\":\"\",\"disabledIcon\":\"\",\"isDivider\":false,\"tooltip\":null,\"type\":\"separator\"},{\"click\":\"chrome:rendering:sort\",\"header\":\"Change position\",\"icon\":\"/temp/iconcache/office/16x16/document_size.png\",\"disabledIcon\":\"/temp/document_size_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Move component.\",\"type\":\"\"},{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:componentoptions(referenceId={69CEBC00-446B-5141-AD1E-450B8D6EE0AD},renderingId={DAB81669-A082-5E99-8D52-717D4EF215F8},id={6D5A0DCA-AA96-526D-B77F-9832E4111509})',null,false)\",\"header\":\"Edit Experience Editor Options\",\"icon\":\"/temp/iconcache/office/16x16/clipboard_check_edit.png\",\"disabledIcon\":\"/temp/clipboard_check_edit_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the Experience Editor options for the component.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:properties\",\"header\":\"Edit component properties\",\"icon\":\"/temp/iconcache/office/16x16/elements_branch.png\",\"disabledIcon\":\"/temp/elements_branch_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the properties for the component.\",\"type\":\"common\"},{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:setdatasource(referenceId={69CEBC00-446B-5141-AD1E-450B8D6EE0AD},renderingId={DAB81669-A082-5E99-8D52-717D4EF215F8},id={6D5A0DCA-AA96-526D-B77F-9832E4111509})',null,false)\",\"header\":\"dsHeaderParameter\",\"icon\":\"/temp/iconcache/office/16x16/data.png\",\"disabledIcon\":\"/temp/data_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"dsTooltipParameter\",\"type\":\"datasourcesmenu\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Test the component.\",\"type\":\"sticky\"},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"datasourcesmenu\"},{\"click\":\"chrome:rendering:delete\",\"header\":\"Delete\",\"icon\":\"/temp/iconcache/office/16x16/delete.png\",\"disabledIcon\":\"/temp/delete_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Remove component.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{6D5A0DCA-AA96-526D-B77F-9832E4111509}?lang=en&ver=1\",\"custom\":{\"renderingID\":\"DAB81669A0825E998D52717D4EF215F8\",\"editable\":\"true\"},\"displayName\":\"Styleguide-FieldUsage-RichText\",\"expandedDisplayName\":null}",
                        "attributes": {
                          "type": "text/sitecore",
                          "chrometype": "rendering",
                          "kind": "open",
                          "hintname": "Styleguide-FieldUsage-RichText",
                          "id": "r_69CEBC00446B5141AD1E450B8D6EE0AD",
                          "class": "scpm",
                          "data-selectable": "true"
                        }
                      },
                      {
                        "uid": "69cebc00-446b-5141-ad1e-450b8d6ee0ad",
                        "componentName": "Styleguide-FieldUsage-RichText",
                        "dataSource": "{6D5A0DCA-AA96-526D-B77F-9832E4111509}",
                        "params": {},
                        "fields": {
                          "sample": {
                            "value": "<p>This is a sample rich text field. <mark>HTML is always supported.</mark> In Sitecore, editors will see a WYSIWYG editor for these fields.</p>",
                            "editable": "<input id='fld_6D5A0DCAAA96526DB77F9832E4111509_6A844160679B534AB1FF19040CD88C58_en_1_b62618cac7f54bb095378ea5e6d0da12_454' class='scFieldValue' name='fld_6D5A0DCAAA96526DB77F9832E4111509_6A844160679B534AB1FF19040CD88C58_en_1_b62618cac7f54bb095378ea5e6d0da12_454' type='hidden' value=\"&lt;p&gt;This is a sample rich text field. &lt;mark&gt;HTML is always supported.&lt;/mark&gt; In Sitecore, editors will see a WYSIWYG editor for these fields.&lt;/p&gt;\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:field:editcontrol({command:\\\"webedit:edithtml\\\"})\",\"header\":\"Edit Text\",\"icon\":\"/temp/iconcache/office/16x16/pencil.png\",\"disabledIcon\":\"/temp/pencil_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the text\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"bold\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_bold.png\",\"disabledIcon\":\"/temp/font_style_bold_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Bold\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"Italic\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_italics.png\",\"disabledIcon\":\"/temp/font_style_italics_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Italic\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"Underline\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_underline.png\",\"disabledIcon\":\"/temp/font_style_underline_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Underline\",\"type\":null},{\"click\":\"chrome:field:insertexternallink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/earth_link.png\",\"disabledIcon\":\"/temp/earth_link_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert an external link into the text field.\",\"type\":null},{\"click\":\"chrome:field:insertlink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/link.png\",\"disabledIcon\":\"/temp/link_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert a link into the text field.\",\"type\":null},{\"click\":\"chrome:field:removelink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/link_broken.png\",\"disabledIcon\":\"/temp/link_broken_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Remove link.\",\"type\":null},{\"click\":\"chrome:field:insertimage\",\"header\":\"Insert image\",\"icon\":\"/temp/iconcache/office/16x16/photo_landscape.png\",\"disabledIcon\":\"/temp/photo_landscape_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert an image into the text field.\",\"type\":null},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{6D5A0DCA-AA96-526D-B77F-9832E4111509}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"sample\",\"expandedDisplayName\":null}</span><span scFieldType=\"rich text\" scDefaultText=\"[No text in field]\" contenteditable=\"true\" class=\"scWebEditInput\" id=\"fld_6D5A0DCAAA96526DB77F9832E4111509_6A844160679B534AB1FF19040CD88C58_en_1_b62618cac7f54bb095378ea5e6d0da12_454_edit\"><p>This is a sample rich text field. <mark>HTML is always supported.</mark> In Sitecore, editors will see a WYSIWYG editor for these fields.</p></span>"
                          },
                          "sample2": {
                            "value": "<p>Another sample rich text field, using options. Keep markup entered in rich text fields as simple as possible - ideally bare tags only (no classes). Adding a wrapping class can help with styling within rich text blocks.</p>\n<marquee>But you can use any valid HTML in a rich text field!</marquee>\n",
                            "editable": "<input id='fld_6D5A0DCAAA96526DB77F9832E4111509_62478523464659EC94B7FDB244C177C7_en_1_b62618cac7f54bb095378ea5e6d0da12_455' class='scFieldValue' name='fld_6D5A0DCAAA96526DB77F9832E4111509_62478523464659EC94B7FDB244C177C7_en_1_b62618cac7f54bb095378ea5e6d0da12_455' type='hidden' value=\"&lt;p&gt;Another sample rich text field, using options. Keep markup entered in rich text fields as simple as possible - ideally bare tags only (no classes). Adding a wrapping class can help with styling within rich text blocks.&lt;/p&gt;\n&lt;marquee&gt;But you can use any valid HTML in a rich text field!&lt;/marquee&gt;\n\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:field:editcontrol({command:\\\"webedit:edithtml\\\"})\",\"header\":\"Edit Text\",\"icon\":\"/temp/iconcache/office/16x16/pencil.png\",\"disabledIcon\":\"/temp/pencil_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the text\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"bold\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_bold.png\",\"disabledIcon\":\"/temp/font_style_bold_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Bold\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"Italic\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_italics.png\",\"disabledIcon\":\"/temp/font_style_italics_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Italic\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"Underline\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_underline.png\",\"disabledIcon\":\"/temp/font_style_underline_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Underline\",\"type\":null},{\"click\":\"chrome:field:insertexternallink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/earth_link.png\",\"disabledIcon\":\"/temp/earth_link_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert an external link into the text field.\",\"type\":null},{\"click\":\"chrome:field:insertlink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/link.png\",\"disabledIcon\":\"/temp/link_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert a link into the text field.\",\"type\":null},{\"click\":\"chrome:field:removelink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/link_broken.png\",\"disabledIcon\":\"/temp/link_broken_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Remove link.\",\"type\":null},{\"click\":\"chrome:field:insertimage\",\"header\":\"Insert image\",\"icon\":\"/temp/iconcache/office/16x16/photo_landscape.png\",\"disabledIcon\":\"/temp/photo_landscape_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert an image into the text field.\",\"type\":null},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{6D5A0DCA-AA96-526D-B77F-9832E4111509}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"Customize Name Shown in Sitecore\",\"expandedDisplayName\":null}</span><span scFieldType=\"rich text\" scDefaultText=\"[No text in field]\" contenteditable=\"true\" class=\"scWebEditInput\" id=\"fld_6D5A0DCAAA96526DB77F9832E4111509_62478523464659EC94B7FDB244C177C7_en_1_b62618cac7f54bb095378ea5e6d0da12_455_edit\"><p>Another sample rich text field, using options. Keep markup entered in rich text fields as simple as possible - ideally bare tags only (no classes). Adding a wrapping class can help with styling within rich text blocks.</p>\n<marquee>But you can use any valid HTML in a rich text field!</marquee>\n</span>"
                          },
                          "heading": {
                            "value": "Rich Text",
                            "editable": "<input id='fld_6D5A0DCAAA96526DB77F9832E4111509_2EF2548084095250A76CC3D867340008_en_1_b62618cac7f54bb095378ea5e6d0da12_456' class='scFieldValue' name='fld_6D5A0DCAAA96526DB77F9832E4111509_2EF2548084095250A76CC3D867340008_en_1_b62618cac7f54bb095378ea5e6d0da12_456' type='hidden' value=\"Rich Text\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{6D5A0DCA-AA96-526D-B77F-9832E4111509}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"heading\",\"expandedDisplayName\":null}</span><span id=\"fld_6D5A0DCAAA96526DB77F9832E4111509_2EF2548084095250A76CC3D867340008_en_1_b62618cac7f54bb095378ea5e6d0da12_456_edit\" sc_parameters=\"prevent-line-break=true\" contenteditable=\"true\" class=\"scWebEditInput\" scFieldType=\"single-line text\" scDefaultText=\"[No text in field]\">Rich Text</span>"
                          },
                          "description": {
                            "value": "",
                            "editable": "<input id='fld_6D5A0DCAAA96526DB77F9832E4111509_439F9C97BCD65D8FBE16CFECCC159EFB_en_1_b62618cac7f54bb095378ea5e6d0da12_457' class='scFieldValue' name='fld_6D5A0DCAAA96526DB77F9832E4111509_439F9C97BCD65D8FBE16CFECCC159EFB_en_1_b62618cac7f54bb095378ea5e6d0da12_457' type='hidden' value=\"\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:field:editcontrol({command:\\\"webedit:edithtml\\\"})\",\"header\":\"Edit Text\",\"icon\":\"/temp/iconcache/office/16x16/pencil.png\",\"disabledIcon\":\"/temp/pencil_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the text\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"bold\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_bold.png\",\"disabledIcon\":\"/temp/font_style_bold_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Bold\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"Italic\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_italics.png\",\"disabledIcon\":\"/temp/font_style_italics_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Italic\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"Underline\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_underline.png\",\"disabledIcon\":\"/temp/font_style_underline_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Underline\",\"type\":null},{\"click\":\"chrome:field:insertexternallink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/earth_link.png\",\"disabledIcon\":\"/temp/earth_link_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert an external link into the text field.\",\"type\":null},{\"click\":\"chrome:field:insertlink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/link.png\",\"disabledIcon\":\"/temp/link_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert a link into the text field.\",\"type\":null},{\"click\":\"chrome:field:removelink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/link_broken.png\",\"disabledIcon\":\"/temp/link_broken_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Remove link.\",\"type\":null},{\"click\":\"chrome:field:insertimage\",\"header\":\"Insert image\",\"icon\":\"/temp/iconcache/office/16x16/photo_landscape.png\",\"disabledIcon\":\"/temp/photo_landscape_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert an image into the text field.\",\"type\":null},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{6D5A0DCA-AA96-526D-B77F-9832E4111509}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"description\",\"expandedDisplayName\":null}</span><span id=\"fld_6D5A0DCAAA96526DB77F9832E4111509_439F9C97BCD65D8FBE16CFECCC159EFB_en_1_b62618cac7f54bb095378ea5e6d0da12_457_edit\" contenteditable=\"true\" class=\"scWebEditInput\" scFieldType=\"rich text\" scDefaultText=\"[No text in field]\" scWatermark=\"true\">[No text in field]</span>"
                          }
                        }
                      },
                      {
                        "name": "code",
                        "type": "text/sitecore",
                        "contents": "",
                        "attributes": {
                          "type": "text/sitecore",
                          "id": "scEnclosingTag_r_",
                          "chrometype": "rendering",
                          "kind": "close",
                          "hintkey": "Styleguide-FieldUsage-RichText",
                          "class": "scpm"
                        }
                      },
                      {
                        "name": "code",
                        "type": "text/sitecore",
                        "contents": "{\"commands\":[{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:fieldeditor(command={2F7D34E2-EB93-47ED-8177-B1F429A9FCD7},fields=sample1|sample2|heading|description,id={28045737-B4D1-57B7-9ADC-CDD5F0A8771A})',null,false)\",\"header\":\"Edit Fields\",\"icon\":\"/temp/iconcache/office/16x16/pencil.png\",\"disabledIcon\":\"/temp/pencil_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"\",\"type\":null},{\"click\":\"chrome:dummy\",\"header\":\"Separator\",\"icon\":\"\",\"disabledIcon\":\"\",\"isDivider\":false,\"tooltip\":null,\"type\":\"separator\"},{\"click\":\"chrome:rendering:sort\",\"header\":\"Change position\",\"icon\":\"/temp/iconcache/office/16x16/document_size.png\",\"disabledIcon\":\"/temp/document_size_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Move component.\",\"type\":\"\"},{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:componentoptions(referenceId={5630C0E6-0430-5F6A-AF9E-2D09D600A386},renderingId={862F358D-BB63-5D50-ABDB-E01FBD771B7F},id={28045737-B4D1-57B7-9ADC-CDD5F0A8771A})',null,false)\",\"header\":\"Edit Experience Editor Options\",\"icon\":\"/temp/iconcache/office/16x16/clipboard_check_edit.png\",\"disabledIcon\":\"/temp/clipboard_check_edit_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the Experience Editor options for the component.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:properties\",\"header\":\"Edit component properties\",\"icon\":\"/temp/iconcache/office/16x16/elements_branch.png\",\"disabledIcon\":\"/temp/elements_branch_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the properties for the component.\",\"type\":\"common\"},{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:setdatasource(referenceId={5630C0E6-0430-5F6A-AF9E-2D09D600A386},renderingId={862F358D-BB63-5D50-ABDB-E01FBD771B7F},id={28045737-B4D1-57B7-9ADC-CDD5F0A8771A})',null,false)\",\"header\":\"dsHeaderParameter\",\"icon\":\"/temp/iconcache/office/16x16/data.png\",\"disabledIcon\":\"/temp/data_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"dsTooltipParameter\",\"type\":\"datasourcesmenu\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Test the component.\",\"type\":\"sticky\"},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"datasourcesmenu\"},{\"click\":\"chrome:rendering:delete\",\"header\":\"Delete\",\"icon\":\"/temp/iconcache/office/16x16/delete.png\",\"disabledIcon\":\"/temp/delete_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Remove component.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{28045737-B4D1-57B7-9ADC-CDD5F0A8771A}?lang=en&ver=1\",\"custom\":{\"renderingID\":\"862F358DBB635D50ABDBE01FBD771B7F\",\"editable\":\"true\"},\"displayName\":\"Styleguide-FieldUsage-Image\",\"expandedDisplayName\":null}",
                        "attributes": {
                          "type": "text/sitecore",
                          "chrometype": "rendering",
                          "kind": "open",
                          "hintname": "Styleguide-FieldUsage-Image",
                          "id": "r_5630C0E604305F6AAF9E2D09D600A386",
                          "class": "scpm",
                          "data-selectable": "true"
                        }
                      },
                      {
                        "uid": "5630c0e6-0430-5f6a-af9e-2d09d600a386",
                        "componentName": "Styleguide-FieldUsage-Image",
                        "dataSource": "{28045737-B4D1-57B7-9ADC-CDD5F0A8771A}",
                        "params": {},
                        "fields": {
                          "sample1": {
                            "value": {
                              "src": "/sitecore/shell/-/media/jss/data/media/img/sc_logo.ashx?h=51&iar=0&w=204",
                              "alt": "Sitecore Logo",
                              "width": "204",
                              "height": "51"
                            },
                            "editable": "<input id='fld_28045737B4D157B79ADCCDD5F0A8771A_480076993604525DAB17B5255A582C73_en_1_cb55820d56ae41d8953ec7ef9b3075b5_458' class='scFieldValue' name='fld_28045737B4D157B79ADCCDD5F0A8771A_480076993604525DAB17B5255A582C73_en_1_cb55820d56ae41d8953ec7ef9b3075b5_458' type='hidden' value=\"&lt;image mediaid=&quot;{F00F53C9-F1B3-52D3-8F42-53194DBE5020}&quot; alt=&quot;Sitecore Logo&quot; /&gt;\" /><code id=\"fld_28045737B4D157B79ADCCDD5F0A8771A_480076993604525DAB17B5255A582C73_en_1_cb55820d56ae41d8953ec7ef9b3075b5_458_edit\" type=\"text/sitecore\" chromeType=\"field\" scFieldType=\"image\" class=\"scpm\" kind=\"open\">{\"commands\":[{\"click\":\"chrome:field:editcontrol({command:\\\"webedit:chooseimage\\\"})\",\"header\":\"Choose Image\",\"icon\":\"/sitecore/shell/themes/standard/custom/16x16/photo_landscape2.png\",\"disabledIcon\":\"/temp/photo_landscape2_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Choose an image.\",\"type\":\"\"},{\"click\":\"chrome:field:editcontrol({command:\\\"webedit:editimage\\\"})\",\"header\":\"Properties\",\"icon\":\"/sitecore/shell/themes/standard/custom/16x16/photo_landscape2_edit.png\",\"disabledIcon\":\"/temp/photo_landscape2_edit_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Modify image appearance.\",\"type\":\"\"},{\"click\":\"chrome:field:editcontrol({command:\\\"webedit:clearimage\\\"})\",\"header\":\"Clear\",\"icon\":\"/sitecore/shell/themes/standard/custom/16x16/photo_landscape2_delete.png\",\"disabledIcon\":\"/temp/photo_landscape2_delete_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Remove the image.\",\"type\":\"\"},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{28045737-B4D1-57B7-9ADC-CDD5F0A8771A}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"sample1\",\"expandedDisplayName\":null}</code><img src=\"/sitecore/shell/-/media/jss/data/media/img/sc_logo.ashx?h=51&amp;iar=0&amp;w=204\" alt=\"Sitecore Logo\" width=\"204\" height=\"51\" /><code class=\"scpm\" type=\"text/sitecore\" chromeType=\"field\" kind=\"close\"></code>"
                          },
                          "sample2": {
                            "value": {
                              "src": "/sitecore/shell/-/media/jss/data/media/img/jss_logo.ashx?h=160&iar=0&w=300",
                              "alt": "Sitecore JSS Logo",
                              "width": "300",
                              "height": "160"
                            },
                            "editable": "<input id='fld_28045737B4D157B79ADCCDD5F0A8771A_10CEFEAA46B85D3D923FA27BC356FC3A_en_1_cb55820d56ae41d8953ec7ef9b3075b5_459' class='scFieldValue' name='fld_28045737B4D157B79ADCCDD5F0A8771A_10CEFEAA46B85D3D923FA27BC356FC3A_en_1_cb55820d56ae41d8953ec7ef9b3075b5_459' type='hidden' value=\"&lt;image mediaid=&quot;{2E8B9ED2-331E-5760-9FA5-B45052ACD392}&quot; alt=&quot;Sitecore JSS Logo&quot; /&gt;\" /><code id=\"fld_28045737B4D157B79ADCCDD5F0A8771A_10CEFEAA46B85D3D923FA27BC356FC3A_en_1_cb55820d56ae41d8953ec7ef9b3075b5_459_edit\" type=\"text/sitecore\" chromeType=\"field\" scFieldType=\"image\" class=\"scpm\" kind=\"open\">{\"commands\":[{\"click\":\"chrome:field:editcontrol({command:\\\"webedit:chooseimage\\\"})\",\"header\":\"Choose Image\",\"icon\":\"/sitecore/shell/themes/standard/custom/16x16/photo_landscape2.png\",\"disabledIcon\":\"/temp/photo_landscape2_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Choose an image.\",\"type\":\"\"},{\"click\":\"chrome:field:editcontrol({command:\\\"webedit:editimage\\\"})\",\"header\":\"Properties\",\"icon\":\"/sitecore/shell/themes/standard/custom/16x16/photo_landscape2_edit.png\",\"disabledIcon\":\"/temp/photo_landscape2_edit_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Modify image appearance.\",\"type\":\"\"},{\"click\":\"chrome:field:editcontrol({command:\\\"webedit:clearimage\\\"})\",\"header\":\"Clear\",\"icon\":\"/sitecore/shell/themes/standard/custom/16x16/photo_landscape2_delete.png\",\"disabledIcon\":\"/temp/photo_landscape2_delete_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Remove the image.\",\"type\":\"\"},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{28045737-B4D1-57B7-9ADC-CDD5F0A8771A}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"sample2\",\"expandedDisplayName\":null}</code><img src=\"/sitecore/shell/-/media/jss/data/media/img/jss_logo.ashx?h=160&amp;iar=0&amp;w=300\" alt=\"Sitecore JSS Logo\" width=\"300\" height=\"160\" /><code class=\"scpm\" type=\"text/sitecore\" chromeType=\"field\" kind=\"close\"></code>"
                          },
                          "heading": {
                            "value": "Image",
                            "editable": "<input id='fld_28045737B4D157B79ADCCDD5F0A8771A_2EF2548084095250A76CC3D867340008_en_1_cb55820d56ae41d8953ec7ef9b3075b5_460' class='scFieldValue' name='fld_28045737B4D157B79ADCCDD5F0A8771A_2EF2548084095250A76CC3D867340008_en_1_cb55820d56ae41d8953ec7ef9b3075b5_460' type='hidden' value=\"Image\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{28045737-B4D1-57B7-9ADC-CDD5F0A8771A}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"heading\",\"expandedDisplayName\":null}</span><span id=\"fld_28045737B4D157B79ADCCDD5F0A8771A_2EF2548084095250A76CC3D867340008_en_1_cb55820d56ae41d8953ec7ef9b3075b5_460_edit\" sc_parameters=\"prevent-line-break=true\" contenteditable=\"true\" class=\"scWebEditInput\" scFieldType=\"single-line text\" scDefaultText=\"[No text in field]\">Image</span>"
                          },
                          "description": {
                            "value": "",
                            "editable": "<input id='fld_28045737B4D157B79ADCCDD5F0A8771A_439F9C97BCD65D8FBE16CFECCC159EFB_en_1_cb55820d56ae41d8953ec7ef9b3075b5_461' class='scFieldValue' name='fld_28045737B4D157B79ADCCDD5F0A8771A_439F9C97BCD65D8FBE16CFECCC159EFB_en_1_cb55820d56ae41d8953ec7ef9b3075b5_461' type='hidden' value=\"\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:field:editcontrol({command:\\\"webedit:edithtml\\\"})\",\"header\":\"Edit Text\",\"icon\":\"/temp/iconcache/office/16x16/pencil.png\",\"disabledIcon\":\"/temp/pencil_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the text\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"bold\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_bold.png\",\"disabledIcon\":\"/temp/font_style_bold_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Bold\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"Italic\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_italics.png\",\"disabledIcon\":\"/temp/font_style_italics_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Italic\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"Underline\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_underline.png\",\"disabledIcon\":\"/temp/font_style_underline_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Underline\",\"type\":null},{\"click\":\"chrome:field:insertexternallink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/earth_link.png\",\"disabledIcon\":\"/temp/earth_link_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert an external link into the text field.\",\"type\":null},{\"click\":\"chrome:field:insertlink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/link.png\",\"disabledIcon\":\"/temp/link_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert a link into the text field.\",\"type\":null},{\"click\":\"chrome:field:removelink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/link_broken.png\",\"disabledIcon\":\"/temp/link_broken_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Remove link.\",\"type\":null},{\"click\":\"chrome:field:insertimage\",\"header\":\"Insert image\",\"icon\":\"/temp/iconcache/office/16x16/photo_landscape.png\",\"disabledIcon\":\"/temp/photo_landscape_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert an image into the text field.\",\"type\":null},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{28045737-B4D1-57B7-9ADC-CDD5F0A8771A}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"description\",\"expandedDisplayName\":null}</span><span id=\"fld_28045737B4D157B79ADCCDD5F0A8771A_439F9C97BCD65D8FBE16CFECCC159EFB_en_1_cb55820d56ae41d8953ec7ef9b3075b5_461_edit\" contenteditable=\"true\" class=\"scWebEditInput\" scFieldType=\"rich text\" scDefaultText=\"[No text in field]\" scWatermark=\"true\">[No text in field]</span>"
                          }
                        }
                      },
                      {
                        "name": "code",
                        "type": "text/sitecore",
                        "contents": "",
                        "attributes": {
                          "type": "text/sitecore",
                          "id": "scEnclosingTag_r_",
                          "chrometype": "rendering",
                          "kind": "close",
                          "hintkey": "Styleguide-FieldUsage-Image",
                          "class": "scpm"
                        }
                      },
                      {
                        "name": "code",
                        "type": "text/sitecore",
                        "contents": "{\"commands\":[{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:fieldeditor(command={2F7D34E2-EB93-47ED-8177-B1F429A9FCD7},fields=file|heading|description,id={312AEE66-27B7-5A84-AE2F-71C14E2B8984})',null,false)\",\"header\":\"Edit Fields\",\"icon\":\"/temp/iconcache/office/16x16/pencil.png\",\"disabledIcon\":\"/temp/pencil_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"\",\"type\":null},{\"click\":\"chrome:dummy\",\"header\":\"Separator\",\"icon\":\"\",\"disabledIcon\":\"\",\"isDivider\":false,\"tooltip\":null,\"type\":\"separator\"},{\"click\":\"chrome:rendering:sort\",\"header\":\"Change position\",\"icon\":\"/temp/iconcache/office/16x16/document_size.png\",\"disabledIcon\":\"/temp/document_size_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Move component.\",\"type\":\"\"},{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:componentoptions(referenceId={BAD43EF7-8940-504D-A09B-976C17A9A30C},renderingId={7CC402CB-A394-5DAA-B45F-32D5FCDFFF6B},id={312AEE66-27B7-5A84-AE2F-71C14E2B8984})',null,false)\",\"header\":\"Edit Experience Editor Options\",\"icon\":\"/temp/iconcache/office/16x16/clipboard_check_edit.png\",\"disabledIcon\":\"/temp/clipboard_check_edit_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the Experience Editor options for the component.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:properties\",\"header\":\"Edit component properties\",\"icon\":\"/temp/iconcache/office/16x16/elements_branch.png\",\"disabledIcon\":\"/temp/elements_branch_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the properties for the component.\",\"type\":\"common\"},{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:setdatasource(referenceId={BAD43EF7-8940-504D-A09B-976C17A9A30C},renderingId={7CC402CB-A394-5DAA-B45F-32D5FCDFFF6B},id={312AEE66-27B7-5A84-AE2F-71C14E2B8984})',null,false)\",\"header\":\"dsHeaderParameter\",\"icon\":\"/temp/iconcache/office/16x16/data.png\",\"disabledIcon\":\"/temp/data_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"dsTooltipParameter\",\"type\":\"datasourcesmenu\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Test the component.\",\"type\":\"sticky\"},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"datasourcesmenu\"},{\"click\":\"chrome:rendering:delete\",\"header\":\"Delete\",\"icon\":\"/temp/iconcache/office/16x16/delete.png\",\"disabledIcon\":\"/temp/delete_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Remove component.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{312AEE66-27B7-5A84-AE2F-71C14E2B8984}?lang=en&ver=1\",\"custom\":{\"renderingID\":\"7CC402CBA3945DAAB45F32D5FCDFFF6B\",\"editable\":\"true\"},\"displayName\":\"Styleguide-FieldUsage-File\",\"expandedDisplayName\":null}",
                        "attributes": {
                          "type": "text/sitecore",
                          "chrometype": "rendering",
                          "kind": "open",
                          "hintname": "Styleguide-FieldUsage-File",
                          "id": "r_BAD43EF78940504DA09B976C17A9A30C",
                          "class": "scpm",
                          "data-selectable": "true"
                        }
                      },
                      {
                        "uid": "bad43ef7-8940-504d-a09b-976c17a9a30c",
                        "componentName": "Styleguide-FieldUsage-File",
                        "dataSource": "{312AEE66-27B7-5A84-AE2F-71C14E2B8984}",
                        "params": {},
                        "fields": {
                          "file": {
                            "value": {
                              "src": "/sitecore/shell/-/media/jss/data/media/files/jss.ashx",
                              "name": "jss",
                              "displayName": "jss",
                              "title": "Example File",
                              "keywords": "",
                              "description": "This data will be added to the Sitecore Media Library on import",
                              "extension": "pdf",
                              "mimeType": "application/pdf",
                              "size": "156897"
                            },
                            "editable": "<input id='fld_312AEE6627B75A84AE2F71C14E2B8984_79AECD92523252BC88EF2BA82875E200_en_1_5cb7d5f5659741f5ba971554f4bfa2f9_462' class='scFieldValue' name='fld_312AEE6627B75A84AE2F71C14E2B8984_79AECD92523252BC88EF2BA82875E200_en_1_5cb7d5f5659741f5ba971554f4bfa2f9_462' type='hidden' value=\"&lt;file mediaid=&quot;{6DC1D6C2-8471-5EE6-A4E7-83F794A6D5E6}&quot; /&gt;\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{312AEE66-27B7-5A84-AE2F-71C14E2B8984}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"file\",\"expandedDisplayName\":null}</span><span scFieldType=\"file\" scDefaultText=\"[No text in field]\" contenteditable=\"true\" class=\"scWebEditInput\" id=\"fld_312AEE6627B75A84AE2F71C14E2B8984_79AECD92523252BC88EF2BA82875E200_en_1_5cb7d5f5659741f5ba971554f4bfa2f9_462_edit\"><file mediaid=\"{6DC1D6C2-8471-5EE6-A4E7-83F794A6D5E6}\" /></span>"
                          },
                          "heading": {
                            "value": "File",
                            "editable": "<input id='fld_312AEE6627B75A84AE2F71C14E2B8984_2EF2548084095250A76CC3D867340008_en_1_5cb7d5f5659741f5ba971554f4bfa2f9_463' class='scFieldValue' name='fld_312AEE6627B75A84AE2F71C14E2B8984_2EF2548084095250A76CC3D867340008_en_1_5cb7d5f5659741f5ba971554f4bfa2f9_463' type='hidden' value=\"File\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{312AEE66-27B7-5A84-AE2F-71C14E2B8984}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"heading\",\"expandedDisplayName\":null}</span><span id=\"fld_312AEE6627B75A84AE2F71C14E2B8984_2EF2548084095250A76CC3D867340008_en_1_5cb7d5f5659741f5ba971554f4bfa2f9_463_edit\" sc_parameters=\"prevent-line-break=true\" contenteditable=\"true\" class=\"scWebEditInput\" scFieldType=\"single-line text\" scDefaultText=\"[No text in field]\">File</span>"
                          },
                          "description": {
                            "value": "<small>Note: Sitecore does not support inline editing of File fields. The value must be edited in Experience Editor by using the edit rendering fields button (looks like a pencil) with the whole component selected.</small>\n",
                            "editable": "<input id='fld_312AEE6627B75A84AE2F71C14E2B8984_439F9C97BCD65D8FBE16CFECCC159EFB_en_1_5cb7d5f5659741f5ba971554f4bfa2f9_464' class='scFieldValue' name='fld_312AEE6627B75A84AE2F71C14E2B8984_439F9C97BCD65D8FBE16CFECCC159EFB_en_1_5cb7d5f5659741f5ba971554f4bfa2f9_464' type='hidden' value=\"&lt;small&gt;Note: Sitecore does not support inline editing of File fields. The value must be edited in Experience Editor by using the edit rendering fields button (looks like a pencil) with the whole component selected.&lt;/small&gt;\n\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:field:editcontrol({command:\\\"webedit:edithtml\\\"})\",\"header\":\"Edit Text\",\"icon\":\"/temp/iconcache/office/16x16/pencil.png\",\"disabledIcon\":\"/temp/pencil_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the text\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"bold\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_bold.png\",\"disabledIcon\":\"/temp/font_style_bold_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Bold\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"Italic\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_italics.png\",\"disabledIcon\":\"/temp/font_style_italics_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Italic\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"Underline\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_underline.png\",\"disabledIcon\":\"/temp/font_style_underline_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Underline\",\"type\":null},{\"click\":\"chrome:field:insertexternallink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/earth_link.png\",\"disabledIcon\":\"/temp/earth_link_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert an external link into the text field.\",\"type\":null},{\"click\":\"chrome:field:insertlink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/link.png\",\"disabledIcon\":\"/temp/link_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert a link into the text field.\",\"type\":null},{\"click\":\"chrome:field:removelink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/link_broken.png\",\"disabledIcon\":\"/temp/link_broken_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Remove link.\",\"type\":null},{\"click\":\"chrome:field:insertimage\",\"header\":\"Insert image\",\"icon\":\"/temp/iconcache/office/16x16/photo_landscape.png\",\"disabledIcon\":\"/temp/photo_landscape_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert an image into the text field.\",\"type\":null},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{312AEE66-27B7-5A84-AE2F-71C14E2B8984}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"description\",\"expandedDisplayName\":null}</span><span scFieldType=\"rich text\" scDefaultText=\"[No text in field]\" contenteditable=\"true\" class=\"scWebEditInput\" id=\"fld_312AEE6627B75A84AE2F71C14E2B8984_439F9C97BCD65D8FBE16CFECCC159EFB_en_1_5cb7d5f5659741f5ba971554f4bfa2f9_464_edit\"><small>Note: Sitecore does not support inline editing of File fields. The value must be edited in Experience Editor by using the edit rendering fields button (looks like a pencil) with the whole component selected.</small>\n</span>"
                          }
                        }
                      },
                      {
                        "name": "code",
                        "type": "text/sitecore",
                        "contents": "",
                        "attributes": {
                          "type": "text/sitecore",
                          "id": "scEnclosingTag_r_",
                          "chrometype": "rendering",
                          "kind": "close",
                          "hintkey": "Styleguide-FieldUsage-File",
                          "class": "scpm"
                        }
                      },
                      {
                        "name": "code",
                        "type": "text/sitecore",
                        "contents": "{\"commands\":[{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:fieldeditor(command={2F7D34E2-EB93-47ED-8177-B1F429A9FCD7},fields=sample|heading|description,id={F7B73F4A-8000-5046-8BF6-402F2F2CCD78})',null,false)\",\"header\":\"Edit Fields\",\"icon\":\"/temp/iconcache/office/16x16/pencil.png\",\"disabledIcon\":\"/temp/pencil_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"\",\"type\":null},{\"click\":\"chrome:dummy\",\"header\":\"Separator\",\"icon\":\"\",\"disabledIcon\":\"\",\"isDivider\":false,\"tooltip\":null,\"type\":\"separator\"},{\"click\":\"chrome:rendering:sort\",\"header\":\"Change position\",\"icon\":\"/temp/iconcache/office/16x16/document_size.png\",\"disabledIcon\":\"/temp/document_size_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Move component.\",\"type\":\"\"},{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:componentoptions(referenceId={FF90D4BD-E50D-5BBF-9213-D25968C9AE75},renderingId={7C0EF2D2-7809-5B2E-8422-1C677744445F},id={F7B73F4A-8000-5046-8BF6-402F2F2CCD78})',null,false)\",\"header\":\"Edit Experience Editor Options\",\"icon\":\"/temp/iconcache/office/16x16/clipboard_check_edit.png\",\"disabledIcon\":\"/temp/clipboard_check_edit_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the Experience Editor options for the component.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:properties\",\"header\":\"Edit component properties\",\"icon\":\"/temp/iconcache/office/16x16/elements_branch.png\",\"disabledIcon\":\"/temp/elements_branch_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the properties for the component.\",\"type\":\"common\"},{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:setdatasource(referenceId={FF90D4BD-E50D-5BBF-9213-D25968C9AE75},renderingId={7C0EF2D2-7809-5B2E-8422-1C677744445F},id={F7B73F4A-8000-5046-8BF6-402F2F2CCD78})',null,false)\",\"header\":\"dsHeaderParameter\",\"icon\":\"/temp/iconcache/office/16x16/data.png\",\"disabledIcon\":\"/temp/data_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"dsTooltipParameter\",\"type\":\"datasourcesmenu\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Test the component.\",\"type\":\"sticky\"},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"datasourcesmenu\"},{\"click\":\"chrome:rendering:delete\",\"header\":\"Delete\",\"icon\":\"/temp/iconcache/office/16x16/delete.png\",\"disabledIcon\":\"/temp/delete_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Remove component.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{F7B73F4A-8000-5046-8BF6-402F2F2CCD78}?lang=en&ver=1\",\"custom\":{\"renderingID\":\"7C0EF2D278095B2E84221C677744445F\",\"editable\":\"true\"},\"displayName\":\"Styleguide-FieldUsage-Number\",\"expandedDisplayName\":null}",
                        "attributes": {
                          "type": "text/sitecore",
                          "chrometype": "rendering",
                          "kind": "open",
                          "hintname": "Styleguide-FieldUsage-Number",
                          "id": "r_FF90D4BDE50D5BBF9213D25968C9AE75",
                          "class": "scpm",
                          "data-selectable": "true"
                        }
                      },
                      {
                        "uid": "ff90d4bd-e50d-5bbf-9213-d25968c9ae75",
                        "componentName": "Styleguide-FieldUsage-Number",
                        "dataSource": "{F7B73F4A-8000-5046-8BF6-402F2F2CCD78}",
                        "params": {},
                        "fields": {
                          "sample": {
                            "value": "1.21",
                            "editable": "<input id='fld_F7B73F4A800050468BF6402F2F2CCD78_5C59410D151A56C8AAFA2C78CD91B56C_en_1_af43661c80c649c68dbd1885c115fa48_465' class='scFieldValue' name='fld_F7B73F4A800050468BF6402F2F2CCD78_5C59410D151A56C8AAFA2C78CD91B56C_en_1_af43661c80c649c68dbd1885c115fa48_465' type='hidden' value=\"1.21\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:field:editcontrol({command:\\\"webedit:editnumber\\\"})\",\"header\":\"Edit number\",\"icon\":\"/temp/iconcache/wordprocessing/16x16/word_count.png\",\"disabledIcon\":\"/temp/word_count_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit number\",\"type\":\"\"},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{F7B73F4A-8000-5046-8BF6-402F2F2CCD78}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"sample\",\"expandedDisplayName\":null}</span><span scFieldType=\"number\" scDefaultText=\"[No text in field]\" contenteditable=\"true\" class=\"scWebEditInput\" id=\"fld_F7B73F4A800050468BF6402F2F2CCD78_5C59410D151A56C8AAFA2C78CD91B56C_en_1_af43661c80c649c68dbd1885c115fa48_465_edit\">1.21</span>"
                          },
                          "heading": {
                            "value": "Number",
                            "editable": "<input id='fld_F7B73F4A800050468BF6402F2F2CCD78_2EF2548084095250A76CC3D867340008_en_1_af43661c80c649c68dbd1885c115fa48_466' class='scFieldValue' name='fld_F7B73F4A800050468BF6402F2F2CCD78_2EF2548084095250A76CC3D867340008_en_1_af43661c80c649c68dbd1885c115fa48_466' type='hidden' value=\"Number\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{F7B73F4A-8000-5046-8BF6-402F2F2CCD78}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"heading\",\"expandedDisplayName\":null}</span><span id=\"fld_F7B73F4A800050468BF6402F2F2CCD78_2EF2548084095250A76CC3D867340008_en_1_af43661c80c649c68dbd1885c115fa48_466_edit\" sc_parameters=\"prevent-line-break=true\" contenteditable=\"true\" class=\"scWebEditInput\" scFieldType=\"single-line text\" scDefaultText=\"[No text in field]\">Number</span>"
                          },
                          "description": {
                            "value": "<small>Number tells Sitecore to use a number entry for editing.</small>",
                            "editable": "<input id='fld_F7B73F4A800050468BF6402F2F2CCD78_439F9C97BCD65D8FBE16CFECCC159EFB_en_1_af43661c80c649c68dbd1885c115fa48_467' class='scFieldValue' name='fld_F7B73F4A800050468BF6402F2F2CCD78_439F9C97BCD65D8FBE16CFECCC159EFB_en_1_af43661c80c649c68dbd1885c115fa48_467' type='hidden' value=\"&lt;small&gt;Number tells Sitecore to use a number entry for editing.&lt;/small&gt;\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:field:editcontrol({command:\\\"webedit:edithtml\\\"})\",\"header\":\"Edit Text\",\"icon\":\"/temp/iconcache/office/16x16/pencil.png\",\"disabledIcon\":\"/temp/pencil_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the text\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"bold\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_bold.png\",\"disabledIcon\":\"/temp/font_style_bold_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Bold\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"Italic\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_italics.png\",\"disabledIcon\":\"/temp/font_style_italics_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Italic\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"Underline\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_underline.png\",\"disabledIcon\":\"/temp/font_style_underline_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Underline\",\"type\":null},{\"click\":\"chrome:field:insertexternallink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/earth_link.png\",\"disabledIcon\":\"/temp/earth_link_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert an external link into the text field.\",\"type\":null},{\"click\":\"chrome:field:insertlink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/link.png\",\"disabledIcon\":\"/temp/link_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert a link into the text field.\",\"type\":null},{\"click\":\"chrome:field:removelink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/link_broken.png\",\"disabledIcon\":\"/temp/link_broken_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Remove link.\",\"type\":null},{\"click\":\"chrome:field:insertimage\",\"header\":\"Insert image\",\"icon\":\"/temp/iconcache/office/16x16/photo_landscape.png\",\"disabledIcon\":\"/temp/photo_landscape_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert an image into the text field.\",\"type\":null},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{F7B73F4A-8000-5046-8BF6-402F2F2CCD78}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"description\",\"expandedDisplayName\":null}</span><span scFieldType=\"rich text\" scDefaultText=\"[No text in field]\" contenteditable=\"true\" class=\"scWebEditInput\" id=\"fld_F7B73F4A800050468BF6402F2F2CCD78_439F9C97BCD65D8FBE16CFECCC159EFB_en_1_af43661c80c649c68dbd1885c115fa48_467_edit\"><small>Number tells Sitecore to use a number entry for editing.</small></span>"
                          }
                        }
                      },
                      {
                        "name": "code",
                        "type": "text/sitecore",
                        "contents": "",
                        "attributes": {
                          "type": "text/sitecore",
                          "id": "scEnclosingTag_r_",
                          "chrometype": "rendering",
                          "kind": "close",
                          "hintkey": "Styleguide-FieldUsage-Number",
                          "class": "scpm"
                        }
                      },
                      {
                        "name": "code",
                        "type": "text/sitecore",
                        "contents": "{\"commands\":[{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:fieldeditor(command={2F7D34E2-EB93-47ED-8177-B1F429A9FCD7},fields=checkbox|checkbox2|heading|description,id={00F1438B-0C19-592A-AFD6-E374B10A38C7})',null,false)\",\"header\":\"Edit Fields\",\"icon\":\"/temp/iconcache/office/16x16/pencil.png\",\"disabledIcon\":\"/temp/pencil_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"\",\"type\":null},{\"click\":\"chrome:dummy\",\"header\":\"Separator\",\"icon\":\"\",\"disabledIcon\":\"\",\"isDivider\":false,\"tooltip\":null,\"type\":\"separator\"},{\"click\":\"chrome:rendering:sort\",\"header\":\"Change position\",\"icon\":\"/temp/iconcache/office/16x16/document_size.png\",\"disabledIcon\":\"/temp/document_size_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Move component.\",\"type\":\"\"},{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:componentoptions(referenceId={B5C1C74A-A81D-59B2-85D8-09BC109B1F70},renderingId={3127DF7E-6ED6-52DF-BC2D-FA4CDC65C9A6},id={00F1438B-0C19-592A-AFD6-E374B10A38C7})',null,false)\",\"header\":\"Edit Experience Editor Options\",\"icon\":\"/temp/iconcache/office/16x16/clipboard_check_edit.png\",\"disabledIcon\":\"/temp/clipboard_check_edit_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the Experience Editor options for the component.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:properties\",\"header\":\"Edit component properties\",\"icon\":\"/temp/iconcache/office/16x16/elements_branch.png\",\"disabledIcon\":\"/temp/elements_branch_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the properties for the component.\",\"type\":\"common\"},{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:setdatasource(referenceId={B5C1C74A-A81D-59B2-85D8-09BC109B1F70},renderingId={3127DF7E-6ED6-52DF-BC2D-FA4CDC65C9A6},id={00F1438B-0C19-592A-AFD6-E374B10A38C7})',null,false)\",\"header\":\"dsHeaderParameter\",\"icon\":\"/temp/iconcache/office/16x16/data.png\",\"disabledIcon\":\"/temp/data_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"dsTooltipParameter\",\"type\":\"datasourcesmenu\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Test the component.\",\"type\":\"sticky\"},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"datasourcesmenu\"},{\"click\":\"chrome:rendering:delete\",\"header\":\"Delete\",\"icon\":\"/temp/iconcache/office/16x16/delete.png\",\"disabledIcon\":\"/temp/delete_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Remove component.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{00F1438B-0C19-592A-AFD6-E374B10A38C7}?lang=en&ver=1\",\"custom\":{\"renderingID\":\"3127DF7E6ED652DFBC2DFA4CDC65C9A6\",\"editable\":\"true\"},\"displayName\":\"Styleguide-FieldUsage-Checkbox\",\"expandedDisplayName\":null}",
                        "attributes": {
                          "type": "text/sitecore",
                          "chrometype": "rendering",
                          "kind": "open",
                          "hintname": "Styleguide-FieldUsage-Checkbox",
                          "id": "r_B5C1C74AA81D59B285D809BC109B1F70",
                          "class": "scpm",
                          "data-selectable": "true"
                        }
                      },
                      {
                        "uid": "b5c1c74a-a81d-59b2-85d8-09bc109b1f70",
                        "componentName": "Styleguide-FieldUsage-Checkbox",
                        "dataSource": "{00F1438B-0C19-592A-AFD6-E374B10A38C7}",
                        "params": {},
                        "fields": {
                          "checkbox": {
                            "value": true,
                            "editable": "<input id='fld_00F1438B0C19592AAFD6E374B10A38C7_11851347E2FC5121B5089A532A16C34B_en_1_cd2d05101f4146159dd5dfd23f22e0e8_468' class='scFieldValue' name='fld_00F1438B0C19592AAFD6E374B10A38C7_11851347E2FC5121B5089A532A16C34B_en_1_cd2d05101f4146159dd5dfd23f22e0e8_468' type='hidden' value=\"1\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{00F1438B-0C19-592A-AFD6-E374B10A38C7}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"checkbox\",\"expandedDisplayName\":null}</span><span scFieldType=\"checkbox\" scDefaultText=\"[No text in field]\" contenteditable=\"true\" class=\"scWebEditInput\" id=\"fld_00F1438B0C19592AAFD6E374B10A38C7_11851347E2FC5121B5089A532A16C34B_en_1_cd2d05101f4146159dd5dfd23f22e0e8_468_edit\">1</span>"
                          },
                          "checkbox2": {
                            "value": false,
                            "editable": "<input id='fld_00F1438B0C19592AAFD6E374B10A38C7_AC86E2AE9EAA5C5FA01B51D865FEDD2E_en_1_cd2d05101f4146159dd5dfd23f22e0e8_469' class='scFieldValue' name='fld_00F1438B0C19592AAFD6E374B10A38C7_AC86E2AE9EAA5C5FA01B51D865FEDD2E_en_1_cd2d05101f4146159dd5dfd23f22e0e8_469' type='hidden' value=\"\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{00F1438B-0C19-592A-AFD6-E374B10A38C7}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"checkbox2\",\"expandedDisplayName\":null}</span><span id=\"fld_00F1438B0C19592AAFD6E374B10A38C7_AC86E2AE9EAA5C5FA01B51D865FEDD2E_en_1_cd2d05101f4146159dd5dfd23f22e0e8_469_edit\" contenteditable=\"true\" class=\"scWebEditInput\" scFieldType=\"checkbox\" scDefaultText=\"[No text in field]\" scWatermark=\"true\">[No text in field]</span>"
                          },
                          "heading": {
                            "value": "Checkbox",
                            "editable": "<input id='fld_00F1438B0C19592AAFD6E374B10A38C7_2EF2548084095250A76CC3D867340008_en_1_cd2d05101f4146159dd5dfd23f22e0e8_470' class='scFieldValue' name='fld_00F1438B0C19592AAFD6E374B10A38C7_2EF2548084095250A76CC3D867340008_en_1_cd2d05101f4146159dd5dfd23f22e0e8_470' type='hidden' value=\"Checkbox\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{00F1438B-0C19-592A-AFD6-E374B10A38C7}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"heading\",\"expandedDisplayName\":null}</span><span id=\"fld_00F1438B0C19592AAFD6E374B10A38C7_2EF2548084095250A76CC3D867340008_en_1_cd2d05101f4146159dd5dfd23f22e0e8_470_edit\" sc_parameters=\"prevent-line-break=true\" contenteditable=\"true\" class=\"scWebEditInput\" scFieldType=\"single-line text\" scDefaultText=\"[No text in field]\">Checkbox</span>"
                          },
                          "description": {
                            "value": "<small>Note: Sitecore does not support inline editing of Checkbox fields. The value must be edited in Experience Editor by using the edit rendering fields button (looks like a pencil) with the whole component selected.</small>\n",
                            "editable": "<input id='fld_00F1438B0C19592AAFD6E374B10A38C7_439F9C97BCD65D8FBE16CFECCC159EFB_en_1_cd2d05101f4146159dd5dfd23f22e0e8_471' class='scFieldValue' name='fld_00F1438B0C19592AAFD6E374B10A38C7_439F9C97BCD65D8FBE16CFECCC159EFB_en_1_cd2d05101f4146159dd5dfd23f22e0e8_471' type='hidden' value=\"&lt;small&gt;Note: Sitecore does not support inline editing of Checkbox fields. The value must be edited in Experience Editor by using the edit rendering fields button (looks like a pencil) with the whole component selected.&lt;/small&gt;\n\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:field:editcontrol({command:\\\"webedit:edithtml\\\"})\",\"header\":\"Edit Text\",\"icon\":\"/temp/iconcache/office/16x16/pencil.png\",\"disabledIcon\":\"/temp/pencil_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the text\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"bold\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_bold.png\",\"disabledIcon\":\"/temp/font_style_bold_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Bold\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"Italic\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_italics.png\",\"disabledIcon\":\"/temp/font_style_italics_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Italic\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"Underline\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_underline.png\",\"disabledIcon\":\"/temp/font_style_underline_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Underline\",\"type\":null},{\"click\":\"chrome:field:insertexternallink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/earth_link.png\",\"disabledIcon\":\"/temp/earth_link_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert an external link into the text field.\",\"type\":null},{\"click\":\"chrome:field:insertlink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/link.png\",\"disabledIcon\":\"/temp/link_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert a link into the text field.\",\"type\":null},{\"click\":\"chrome:field:removelink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/link_broken.png\",\"disabledIcon\":\"/temp/link_broken_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Remove link.\",\"type\":null},{\"click\":\"chrome:field:insertimage\",\"header\":\"Insert image\",\"icon\":\"/temp/iconcache/office/16x16/photo_landscape.png\",\"disabledIcon\":\"/temp/photo_landscape_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert an image into the text field.\",\"type\":null},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{00F1438B-0C19-592A-AFD6-E374B10A38C7}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"description\",\"expandedDisplayName\":null}</span><span scFieldType=\"rich text\" scDefaultText=\"[No text in field]\" contenteditable=\"true\" class=\"scWebEditInput\" id=\"fld_00F1438B0C19592AAFD6E374B10A38C7_439F9C97BCD65D8FBE16CFECCC159EFB_en_1_cd2d05101f4146159dd5dfd23f22e0e8_471_edit\"><small>Note: Sitecore does not support inline editing of Checkbox fields. The value must be edited in Experience Editor by using the edit rendering fields button (looks like a pencil) with the whole component selected.</small>\n</span>"
                          }
                        }
                      },
                      {
                        "name": "code",
                        "type": "text/sitecore",
                        "contents": "",
                        "attributes": {
                          "type": "text/sitecore",
                          "id": "scEnclosingTag_r_",
                          "chrometype": "rendering",
                          "kind": "close",
                          "hintkey": "Styleguide-FieldUsage-Checkbox",
                          "class": "scpm"
                        }
                      },
                      {
                        "name": "code",
                        "type": "text/sitecore",
                        "contents": "{\"commands\":[{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:fieldeditor(command={2F7D34E2-EB93-47ED-8177-B1F429A9FCD7},fields=date|dateTime|heading|description,id={694E167B-AE87-5A1B-AED8-3FF4D4D1884C})',null,false)\",\"header\":\"Edit Fields\",\"icon\":\"/temp/iconcache/office/16x16/pencil.png\",\"disabledIcon\":\"/temp/pencil_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"\",\"type\":null},{\"click\":\"chrome:dummy\",\"header\":\"Separator\",\"icon\":\"\",\"disabledIcon\":\"\",\"isDivider\":false,\"tooltip\":null,\"type\":\"separator\"},{\"click\":\"chrome:rendering:sort\",\"header\":\"Change position\",\"icon\":\"/temp/iconcache/office/16x16/document_size.png\",\"disabledIcon\":\"/temp/document_size_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Move component.\",\"type\":\"\"},{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:componentoptions(referenceId={F166A7D6-9EC8-5C53-B825-33405DB7F575},renderingId={200369E7-89C4-531B-B076-C53AB1C50154},id={694E167B-AE87-5A1B-AED8-3FF4D4D1884C})',null,false)\",\"header\":\"Edit Experience Editor Options\",\"icon\":\"/temp/iconcache/office/16x16/clipboard_check_edit.png\",\"disabledIcon\":\"/temp/clipboard_check_edit_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the Experience Editor options for the component.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:properties\",\"header\":\"Edit component properties\",\"icon\":\"/temp/iconcache/office/16x16/elements_branch.png\",\"disabledIcon\":\"/temp/elements_branch_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the properties for the component.\",\"type\":\"common\"},{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:setdatasource(referenceId={F166A7D6-9EC8-5C53-B825-33405DB7F575},renderingId={200369E7-89C4-531B-B076-C53AB1C50154},id={694E167B-AE87-5A1B-AED8-3FF4D4D1884C})',null,false)\",\"header\":\"dsHeaderParameter\",\"icon\":\"/temp/iconcache/office/16x16/data.png\",\"disabledIcon\":\"/temp/data_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"dsTooltipParameter\",\"type\":\"datasourcesmenu\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Test the component.\",\"type\":\"sticky\"},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"datasourcesmenu\"},{\"click\":\"chrome:rendering:delete\",\"header\":\"Delete\",\"icon\":\"/temp/iconcache/office/16x16/delete.png\",\"disabledIcon\":\"/temp/delete_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Remove component.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{694E167B-AE87-5A1B-AED8-3FF4D4D1884C}?lang=en&ver=1\",\"custom\":{\"renderingID\":\"200369E789C4531BB076C53AB1C50154\",\"editable\":\"true\"},\"displayName\":\"Styleguide-FieldUsage-Date\",\"expandedDisplayName\":null}",
                        "attributes": {
                          "type": "text/sitecore",
                          "chrometype": "rendering",
                          "kind": "open",
                          "hintname": "Styleguide-FieldUsage-Date",
                          "id": "r_F166A7D69EC85C53B82533405DB7F575",
                          "class": "scpm",
                          "data-selectable": "true"
                        }
                      },
                      {
                        "uid": "f166a7d6-9ec8-5c53-b825-33405db7f575",
                        "componentName": "Styleguide-FieldUsage-Date",
                        "dataSource": "{694E167B-AE87-5A1B-AED8-3FF4D4D1884C}",
                        "params": {},
                        "fields": {
                          "date": {
                            "value": "2012-05-04T00:00:00Z",
                            "editable": "<input id='fld_694E167BAE875A1BAED83FF4D4D1884C_B3FBEBF53C85532B902AF97A39F4D000_en_1_0921640eef2947358608f355fbd71cb1_472' class='scFieldValue' name='fld_694E167BAE875A1BAED83FF4D4D1884C_B3FBEBF53C85532B902AF97A39F4D000_en_1_0921640eef2947358608f355fbd71cb1_472' type='hidden' value=\"20120504T000000Z\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:field:editcontrol({command:\\\"webedit:editdate\\\"})\",\"header\":\"Show calendar\",\"icon\":\"/temp/iconcache/business/16x16/calendar.png\",\"disabledIcon\":\"/temp/calendar_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Shows the calendar\",\"type\":\"\"},{\"click\":\"chrome:field:editcontrol({command:\\\"webedit:cleardate\\\"})\",\"header\":\"Clear calender\",\"icon\":\"/temp/iconcache/applications/16x16/delete2.png\",\"disabledIcon\":\"/temp/delete2_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Clear date\",\"type\":\"\"},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{694E167B-AE87-5A1B-AED8-3FF4D4D1884C}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"date\",\"expandedDisplayName\":null}</span><span scFieldType=\"date\" scDefaultText=\"[No text in field]\" class=\"scWebEditInput\" id=\"fld_694E167BAE875A1BAED83FF4D4D1884C_B3FBEBF53C85532B902AF97A39F4D000_en_1_0921640eef2947358608f355fbd71cb1_472_edit\">5/4/2012</span>"
                          },
                          "dateTime": {
                            "value": "2018-03-14T15:00:00Z",
                            "editable": "<input id='fld_694E167BAE875A1BAED83FF4D4D1884C_185C71FCD0FC56368B9E5448EB0A4C04_en_1_0921640eef2947358608f355fbd71cb1_473' class='scFieldValue' name='fld_694E167BAE875A1BAED83FF4D4D1884C_185C71FCD0FC56368B9E5448EB0A4C04_en_1_0921640eef2947358608f355fbd71cb1_473' type='hidden' value=\"20180314T150000Z\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:field:editcontrol({command:\\\"webedit:editdatetime\\\"})\",\"header\":\"Show calendar\",\"icon\":\"/temp/iconcache/business/16x16/calendar.png\",\"disabledIcon\":\"/temp/calendar_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Shows the calendar\",\"type\":\"\"},{\"click\":\"chrome:field:editcontrol({command:\\\"webedit:cleardate\\\"})\",\"header\":\"Clear calender\",\"icon\":\"/temp/iconcache/applications/16x16/delete2.png\",\"disabledIcon\":\"/temp/delete2_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Clear date and time\",\"type\":\"\"},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{694E167B-AE87-5A1B-AED8-3FF4D4D1884C}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"dateTime\",\"expandedDisplayName\":null}</span><span scFieldType=\"datetime\" scDefaultText=\"[No text in field]\" class=\"scWebEditInput\" id=\"fld_694E167BAE875A1BAED83FF4D4D1884C_185C71FCD0FC56368B9E5448EB0A4C04_en_1_0921640eef2947358608f355fbd71cb1_473_edit\">3/14/2018 3:00:00 PM</span>"
                          },
                          "heading": {
                            "value": "Date",
                            "editable": "<input id='fld_694E167BAE875A1BAED83FF4D4D1884C_2EF2548084095250A76CC3D867340008_en_1_0921640eef2947358608f355fbd71cb1_474' class='scFieldValue' name='fld_694E167BAE875A1BAED83FF4D4D1884C_2EF2548084095250A76CC3D867340008_en_1_0921640eef2947358608f355fbd71cb1_474' type='hidden' value=\"Date\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{694E167B-AE87-5A1B-AED8-3FF4D4D1884C}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"heading\",\"expandedDisplayName\":null}</span><span id=\"fld_694E167BAE875A1BAED83FF4D4D1884C_2EF2548084095250A76CC3D867340008_en_1_0921640eef2947358608f355fbd71cb1_474_edit\" sc_parameters=\"prevent-line-break=true\" contenteditable=\"true\" class=\"scWebEditInput\" scFieldType=\"single-line text\" scDefaultText=\"[No text in field]\">Date</span>"
                          },
                          "description": {
                            "value": "<p><small>Both <code>Date</code> and <code>DateTime</code> field types are available. Choosing <code>DateTime</code> will make Sitecore show editing UI for time; both types store complete date and time values internally. Date values in JSS are formatted using <a href=\"https://en.wikipedia.org/wiki/ISO_8601#Combined_date_and_time_representations\" target=\"_blank\">ISO 8601 formatted strings</a>, for example <code>2012-04-23T18:25:43.511Z</code>.</small></p>\n<div class=\"alert alert-warning\"><small>Note: this is a JavaScript date format (e.g. <code>new Date().toISOString()</code>), and is different from how Sitecore stores date field values internally. Sitecore-formatted dates will not work.</small></div>\n",
                            "editable": "<input id='fld_694E167BAE875A1BAED83FF4D4D1884C_439F9C97BCD65D8FBE16CFECCC159EFB_en_1_0921640eef2947358608f355fbd71cb1_475' class='scFieldValue' name='fld_694E167BAE875A1BAED83FF4D4D1884C_439F9C97BCD65D8FBE16CFECCC159EFB_en_1_0921640eef2947358608f355fbd71cb1_475' type='hidden' value=\"&lt;p&gt;&lt;small&gt;Both &lt;code&gt;Date&lt;/code&gt; and &lt;code&gt;DateTime&lt;/code&gt; field types are available. Choosing &lt;code&gt;DateTime&lt;/code&gt; will make Sitecore show editing UI for time; both types store complete date and time values internally. Date values in JSS are formatted using &lt;a href=&quot;https://en.wikipedia.org/wiki/ISO_8601#Combined_date_and_time_representations&quot; target=&quot;_blank&quot;&gt;ISO 8601 formatted strings&lt;/a&gt;, for example &lt;code&gt;2012-04-23T18:25:43.511Z&lt;/code&gt;.&lt;/small&gt;&lt;/p&gt;\n&lt;div class=&quot;alert alert-warning&quot;&gt;&lt;small&gt;Note: this is a JavaScript date format (e.g. &lt;code&gt;new Date().toISOString()&lt;/code&gt;), and is different from how Sitecore stores date field values internally. Sitecore-formatted dates will not work.&lt;/small&gt;&lt;/div&gt;\n\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:field:editcontrol({command:\\\"webedit:edithtml\\\"})\",\"header\":\"Edit Text\",\"icon\":\"/temp/iconcache/office/16x16/pencil.png\",\"disabledIcon\":\"/temp/pencil_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the text\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"bold\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_bold.png\",\"disabledIcon\":\"/temp/font_style_bold_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Bold\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"Italic\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_italics.png\",\"disabledIcon\":\"/temp/font_style_italics_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Italic\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"Underline\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_underline.png\",\"disabledIcon\":\"/temp/font_style_underline_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Underline\",\"type\":null},{\"click\":\"chrome:field:insertexternallink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/earth_link.png\",\"disabledIcon\":\"/temp/earth_link_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert an external link into the text field.\",\"type\":null},{\"click\":\"chrome:field:insertlink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/link.png\",\"disabledIcon\":\"/temp/link_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert a link into the text field.\",\"type\":null},{\"click\":\"chrome:field:removelink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/link_broken.png\",\"disabledIcon\":\"/temp/link_broken_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Remove link.\",\"type\":null},{\"click\":\"chrome:field:insertimage\",\"header\":\"Insert image\",\"icon\":\"/temp/iconcache/office/16x16/photo_landscape.png\",\"disabledIcon\":\"/temp/photo_landscape_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert an image into the text field.\",\"type\":null},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{694E167B-AE87-5A1B-AED8-3FF4D4D1884C}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"description\",\"expandedDisplayName\":null}</span><span scFieldType=\"rich text\" scDefaultText=\"[No text in field]\" contenteditable=\"true\" class=\"scWebEditInput\" id=\"fld_694E167BAE875A1BAED83FF4D4D1884C_439F9C97BCD65D8FBE16CFECCC159EFB_en_1_0921640eef2947358608f355fbd71cb1_475_edit\"><p><small>Both <code>Date</code> and <code>DateTime</code> field types are available. Choosing <code>DateTime</code> will make Sitecore show editing UI for time; both types store complete date and time values internally. Date values in JSS are formatted using <a href=\"https://en.wikipedia.org/wiki/ISO_8601#Combined_date_and_time_representations\" target=\"_blank\">ISO 8601 formatted strings</a>, for example <code>2012-04-23T18:25:43.511Z</code>.</small></p>\n<div class=\"alert alert-warning\"><small>Note: this is a JavaScript date format (e.g. <code>new Date().toISOString()</code>), and is different from how Sitecore stores date field values internally. Sitecore-formatted dates will not work.</small></div>\n</span>"
                          }
                        }
                      },
                      {
                        "name": "code",
                        "type": "text/sitecore",
                        "contents": "",
                        "attributes": {
                          "type": "text/sitecore",
                          "id": "scEnclosingTag_r_",
                          "chrometype": "rendering",
                          "kind": "close",
                          "hintkey": "Styleguide-FieldUsage-Date",
                          "class": "scpm"
                        }
                      },
                      {
                        "name": "code",
                        "type": "text/sitecore",
                        "contents": "{\"commands\":[{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:fieldeditor(command={2F7D34E2-EB93-47ED-8177-B1F429A9FCD7},fields=externalLink|internalLink|emailLink|paramsLink|heading|description,id={8DF16CAD-F1FD-526C-846D-03616E8111C1})',null,false)\",\"header\":\"Edit Fields\",\"icon\":\"/temp/iconcache/office/16x16/pencil.png\",\"disabledIcon\":\"/temp/pencil_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"\",\"type\":null},{\"click\":\"chrome:dummy\",\"header\":\"Separator\",\"icon\":\"\",\"disabledIcon\":\"\",\"isDivider\":false,\"tooltip\":null,\"type\":\"separator\"},{\"click\":\"chrome:rendering:sort\",\"header\":\"Change position\",\"icon\":\"/temp/iconcache/office/16x16/document_size.png\",\"disabledIcon\":\"/temp/document_size_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Move component.\",\"type\":\"\"},{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:componentoptions(referenceId={56A9562A-6813-579B-8ED2-FDDAB1BFD3D2},renderingId={2D83A15B-7EEF-52C1-BBA2-EAB2D742D952},id={8DF16CAD-F1FD-526C-846D-03616E8111C1})',null,false)\",\"header\":\"Edit Experience Editor Options\",\"icon\":\"/temp/iconcache/office/16x16/clipboard_check_edit.png\",\"disabledIcon\":\"/temp/clipboard_check_edit_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the Experience Editor options for the component.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:properties\",\"header\":\"Edit component properties\",\"icon\":\"/temp/iconcache/office/16x16/elements_branch.png\",\"disabledIcon\":\"/temp/elements_branch_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the properties for the component.\",\"type\":\"common\"},{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:setdatasource(referenceId={56A9562A-6813-579B-8ED2-FDDAB1BFD3D2},renderingId={2D83A15B-7EEF-52C1-BBA2-EAB2D742D952},id={8DF16CAD-F1FD-526C-846D-03616E8111C1})',null,false)\",\"header\":\"dsHeaderParameter\",\"icon\":\"/temp/iconcache/office/16x16/data.png\",\"disabledIcon\":\"/temp/data_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"dsTooltipParameter\",\"type\":\"datasourcesmenu\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Test the component.\",\"type\":\"sticky\"},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"datasourcesmenu\"},{\"click\":\"chrome:rendering:delete\",\"header\":\"Delete\",\"icon\":\"/temp/iconcache/office/16x16/delete.png\",\"disabledIcon\":\"/temp/delete_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Remove component.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{8DF16CAD-F1FD-526C-846D-03616E8111C1}?lang=en&ver=1\",\"custom\":{\"renderingID\":\"2D83A15B7EEF52C1BBA2EAB2D742D952\",\"editable\":\"true\"},\"displayName\":\"Styleguide-FieldUsage-Link\",\"expandedDisplayName\":null}",
                        "attributes": {
                          "type": "text/sitecore",
                          "chrometype": "rendering",
                          "kind": "open",
                          "hintname": "Styleguide-FieldUsage-Link",
                          "id": "r_56A9562A6813579B8ED2FDDAB1BFD3D2",
                          "class": "scpm",
                          "data-selectable": "true"
                        }
                      },
                      {
                        "uid": "56a9562a-6813-579b-8ed2-fddab1bfd3d2",
                        "componentName": "Styleguide-FieldUsage-Link",
                        "dataSource": "{8DF16CAD-F1FD-526C-846D-03616E8111C1}",
                        "params": {},
                        "fields": {
                          "externalLink": {
                            "value": {
                              "href": "https://www.sitecore.com",
                              "text": "Link to Sitecore",
                              "url": "https://www.sitecore.com",
                              "linktype": "external"
                            },
                            "editable": "<input id='fld_8DF16CADF1FD526C846D03616E8111C1_BEA695CBC6555C71BA89D9324D2DFA7B_en_1_d420104dd70341c4b52a681428ac687e_476' class='scFieldValue' name='fld_8DF16CADF1FD526C846D03616E8111C1_BEA695CBC6555C71BA89D9324D2DFA7B_en_1_d420104dd70341c4b52a681428ac687e_476' type='hidden' value=\"&lt;link text=&quot;Link to Sitecore&quot; url=&quot;https://www.sitecore.com&quot; linktype=&quot;external&quot; /&gt;\" /><code id=\"fld_8DF16CADF1FD526C846D03616E8111C1_BEA695CBC6555C71BA89D9324D2DFA7B_en_1_d420104dd70341c4b52a681428ac687e_476_edit\" type=\"text/sitecore\" chromeType=\"field\" scFieldType=\"general link\" class=\"scpm\" kind=\"open\">{\"commands\":[{\"click\":\"chrome:field:editcontrol({command:\\\"webedit:editlink\\\"})\",\"header\":\"Edit link\",\"icon\":\"/temp/iconcache/networkv2/16x16/link_edit.png\",\"disabledIcon\":\"/temp/link_edit_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edits the link destination and appearance\",\"type\":\"\"},{\"click\":\"chrome:field:editcontrol({command:\\\"webedit:clearlink\\\"})\",\"header\":\"Clear Link\",\"icon\":\"/temp/iconcache/networkv2/16x16/link_delete.png\",\"disabledIcon\":\"/temp/link_delete_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Clears The Link\",\"type\":\"\"},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{8DF16CAD-F1FD-526C-846D-03616E8111C1}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"externalLink\",\"expandedDisplayName\":null}</code><a href=\"https://www.sitecore.com\">Link to Sitecore</a><code class=\"scpm\" type=\"text/sitecore\" chromeType=\"field\" kind=\"close\"></code>",
                            "editableFirstPart": "<input id='fld_8DF16CADF1FD526C846D03616E8111C1_BEA695CBC6555C71BA89D9324D2DFA7B_en_1_d420104dd70341c4b52a681428ac687e_476' class='scFieldValue' name='fld_8DF16CADF1FD526C846D03616E8111C1_BEA695CBC6555C71BA89D9324D2DFA7B_en_1_d420104dd70341c4b52a681428ac687e_476' type='hidden' value=\"&lt;link text=&quot;Link to Sitecore&quot; url=&quot;https://www.sitecore.com&quot; linktype=&quot;external&quot; /&gt;\" /><code id=\"fld_8DF16CADF1FD526C846D03616E8111C1_BEA695CBC6555C71BA89D9324D2DFA7B_en_1_d420104dd70341c4b52a681428ac687e_476_edit\" type=\"text/sitecore\" chromeType=\"field\" scFieldType=\"general link\" class=\"scpm\" kind=\"open\">{\"commands\":[{\"click\":\"chrome:field:editcontrol({command:\\\"webedit:editlink\\\"})\",\"header\":\"Edit link\",\"icon\":\"/temp/iconcache/networkv2/16x16/link_edit.png\",\"disabledIcon\":\"/temp/link_edit_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edits the link destination and appearance\",\"type\":\"\"},{\"click\":\"chrome:field:editcontrol({command:\\\"webedit:clearlink\\\"})\",\"header\":\"Clear Link\",\"icon\":\"/temp/iconcache/networkv2/16x16/link_delete.png\",\"disabledIcon\":\"/temp/link_delete_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Clears The Link\",\"type\":\"\"},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{8DF16CAD-F1FD-526C-846D-03616E8111C1}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"externalLink\",\"expandedDisplayName\":null}</code><a href=\"https://www.sitecore.com\">Link to Sitecore",
                            "editableLastPart": "</a><code class=\"scpm\" type=\"text/sitecore\" chromeType=\"field\" kind=\"close\"></code>"
                          },
                          "internalLink": {
                            "value": {
                              "href": "/en/jss/home",
                              "linktype": "internal",
                              "id": "{01B9B7C5-9A38-5B92-8E5E-F79A8C305189}"
                            },
                            "editable": "<input id='fld_8DF16CADF1FD526C846D03616E8111C1_82F22C6696505F9EA5766368B0BB2CF6_en_1_d420104dd70341c4b52a681428ac687e_477' class='scFieldValue' name='fld_8DF16CADF1FD526C846D03616E8111C1_82F22C6696505F9EA5766368B0BB2CF6_en_1_d420104dd70341c4b52a681428ac687e_477' type='hidden' value=\"&lt;link linktype=&quot;internal&quot; id=&quot;{01B9B7C5-9A38-5B92-8E5E-F79A8C305189}&quot; /&gt;\" /><code id=\"fld_8DF16CADF1FD526C846D03616E8111C1_82F22C6696505F9EA5766368B0BB2CF6_en_1_d420104dd70341c4b52a681428ac687e_477_edit\" type=\"text/sitecore\" chromeType=\"field\" scFieldType=\"general link\" class=\"scpm\" kind=\"open\">{\"commands\":[{\"click\":\"chrome:field:editcontrol({command:\\\"webedit:editlink\\\"})\",\"header\":\"Edit link\",\"icon\":\"/temp/iconcache/networkv2/16x16/link_edit.png\",\"disabledIcon\":\"/temp/link_edit_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edits the link destination and appearance\",\"type\":\"\"},{\"click\":\"chrome:field:editcontrol({command:\\\"webedit:clearlink\\\"})\",\"header\":\"Clear Link\",\"icon\":\"/temp/iconcache/networkv2/16x16/link_delete.png\",\"disabledIcon\":\"/temp/link_delete_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Clears The Link\",\"type\":\"\"},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{8DF16CAD-F1FD-526C-846D-03616E8111C1}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"internalLink\",\"expandedDisplayName\":null}</code><a href=\"/en/jss/home\">home</a><code class=\"scpm\" type=\"text/sitecore\" chromeType=\"field\" kind=\"close\"></code>",
                            "editableFirstPart": "<input id='fld_8DF16CADF1FD526C846D03616E8111C1_82F22C6696505F9EA5766368B0BB2CF6_en_1_d420104dd70341c4b52a681428ac687e_477' class='scFieldValue' name='fld_8DF16CADF1FD526C846D03616E8111C1_82F22C6696505F9EA5766368B0BB2CF6_en_1_d420104dd70341c4b52a681428ac687e_477' type='hidden' value=\"&lt;link linktype=&quot;internal&quot; id=&quot;{01B9B7C5-9A38-5B92-8E5E-F79A8C305189}&quot; /&gt;\" /><code id=\"fld_8DF16CADF1FD526C846D03616E8111C1_82F22C6696505F9EA5766368B0BB2CF6_en_1_d420104dd70341c4b52a681428ac687e_477_edit\" type=\"text/sitecore\" chromeType=\"field\" scFieldType=\"general link\" class=\"scpm\" kind=\"open\">{\"commands\":[{\"click\":\"chrome:field:editcontrol({command:\\\"webedit:editlink\\\"})\",\"header\":\"Edit link\",\"icon\":\"/temp/iconcache/networkv2/16x16/link_edit.png\",\"disabledIcon\":\"/temp/link_edit_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edits the link destination and appearance\",\"type\":\"\"},{\"click\":\"chrome:field:editcontrol({command:\\\"webedit:clearlink\\\"})\",\"header\":\"Clear Link\",\"icon\":\"/temp/iconcache/networkv2/16x16/link_delete.png\",\"disabledIcon\":\"/temp/link_delete_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Clears The Link\",\"type\":\"\"},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{8DF16CAD-F1FD-526C-846D-03616E8111C1}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"internalLink\",\"expandedDisplayName\":null}</code><a href=\"/en/jss/home\">home",
                            "editableLastPart": "</a><code class=\"scpm\" type=\"text/sitecore\" chromeType=\"field\" kind=\"close\"></code>"
                          },
                          "emailLink": {
                            "value": {
                              "href": "mailto:foo@bar.com",
                              "text": "Send an Email",
                              "url": "mailto:foo@bar.com",
                              "linktype": "mailto"
                            },
                            "editable": "<input id='fld_8DF16CADF1FD526C846D03616E8111C1_C17D9797721154BFBBF2F0B61559CEB6_en_1_d420104dd70341c4b52a681428ac687e_478' class='scFieldValue' name='fld_8DF16CADF1FD526C846D03616E8111C1_C17D9797721154BFBBF2F0B61559CEB6_en_1_d420104dd70341c4b52a681428ac687e_478' type='hidden' value=\"&lt;link text=&quot;Send an Email&quot; url=&quot;mailto:foo@bar.com&quot; linktype=&quot;mailto&quot; /&gt;\" /><code id=\"fld_8DF16CADF1FD526C846D03616E8111C1_C17D9797721154BFBBF2F0B61559CEB6_en_1_d420104dd70341c4b52a681428ac687e_478_edit\" type=\"text/sitecore\" chromeType=\"field\" scFieldType=\"general link\" class=\"scpm\" kind=\"open\">{\"commands\":[{\"click\":\"chrome:field:editcontrol({command:\\\"webedit:editlink\\\"})\",\"header\":\"Edit link\",\"icon\":\"/temp/iconcache/networkv2/16x16/link_edit.png\",\"disabledIcon\":\"/temp/link_edit_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edits the link destination and appearance\",\"type\":\"\"},{\"click\":\"chrome:field:editcontrol({command:\\\"webedit:clearlink\\\"})\",\"header\":\"Clear Link\",\"icon\":\"/temp/iconcache/networkv2/16x16/link_delete.png\",\"disabledIcon\":\"/temp/link_delete_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Clears The Link\",\"type\":\"\"},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{8DF16CAD-F1FD-526C-846D-03616E8111C1}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"emailLink\",\"expandedDisplayName\":null}</code><a href=\"mailto:foo@bar.com\">Send an Email</a><code class=\"scpm\" type=\"text/sitecore\" chromeType=\"field\" kind=\"close\"></code>",
                            "editableFirstPart": "<input id='fld_8DF16CADF1FD526C846D03616E8111C1_C17D9797721154BFBBF2F0B61559CEB6_en_1_d420104dd70341c4b52a681428ac687e_478' class='scFieldValue' name='fld_8DF16CADF1FD526C846D03616E8111C1_C17D9797721154BFBBF2F0B61559CEB6_en_1_d420104dd70341c4b52a681428ac687e_478' type='hidden' value=\"&lt;link text=&quot;Send an Email&quot; url=&quot;mailto:foo@bar.com&quot; linktype=&quot;mailto&quot; /&gt;\" /><code id=\"fld_8DF16CADF1FD526C846D03616E8111C1_C17D9797721154BFBBF2F0B61559CEB6_en_1_d420104dd70341c4b52a681428ac687e_478_edit\" type=\"text/sitecore\" chromeType=\"field\" scFieldType=\"general link\" class=\"scpm\" kind=\"open\">{\"commands\":[{\"click\":\"chrome:field:editcontrol({command:\\\"webedit:editlink\\\"})\",\"header\":\"Edit link\",\"icon\":\"/temp/iconcache/networkv2/16x16/link_edit.png\",\"disabledIcon\":\"/temp/link_edit_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edits the link destination and appearance\",\"type\":\"\"},{\"click\":\"chrome:field:editcontrol({command:\\\"webedit:clearlink\\\"})\",\"header\":\"Clear Link\",\"icon\":\"/temp/iconcache/networkv2/16x16/link_delete.png\",\"disabledIcon\":\"/temp/link_delete_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Clears The Link\",\"type\":\"\"},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{8DF16CAD-F1FD-526C-846D-03616E8111C1}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"emailLink\",\"expandedDisplayName\":null}</code><a href=\"mailto:foo@bar.com\">Send an Email",
                            "editableLastPart": "</a><code class=\"scpm\" type=\"text/sitecore\" chromeType=\"field\" kind=\"close\"></code>"
                          },
                          "paramsLink": {
                            "value": {
                              "href": "https://dev.sitecore.net",
                              "target": "_blank",
                              "text": "Sitecore Dev Site",
                              "title": "<a> title attribute",
                              "url": "https://dev.sitecore.net",
                              "class": "font-weight-bold",
                              "linktype": "external"
                            },
                            "editable": "<input id='fld_8DF16CADF1FD526C846D03616E8111C1_39EA2D44460558D4A7F380E839BD91C7_en_1_d420104dd70341c4b52a681428ac687e_479' class='scFieldValue' name='fld_8DF16CADF1FD526C846D03616E8111C1_39EA2D44460558D4A7F380E839BD91C7_en_1_d420104dd70341c4b52a681428ac687e_479' type='hidden' value=\"&lt;link target=&quot;_blank&quot; text=&quot;Sitecore Dev Site&quot; title=&quot;&amp;lt;a&amp;gt; title attribute&quot; url=&quot;https://dev.sitecore.net&quot; class=&quot;font-weight-bold&quot; linktype=&quot;external&quot; /&gt;\" /><code id=\"fld_8DF16CADF1FD526C846D03616E8111C1_39EA2D44460558D4A7F380E839BD91C7_en_1_d420104dd70341c4b52a681428ac687e_479_edit\" type=\"text/sitecore\" chromeType=\"field\" scFieldType=\"general link\" class=\"scpm\" kind=\"open\">{\"commands\":[{\"click\":\"chrome:field:editcontrol({command:\\\"webedit:editlink\\\"})\",\"header\":\"Edit link\",\"icon\":\"/temp/iconcache/networkv2/16x16/link_edit.png\",\"disabledIcon\":\"/temp/link_edit_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edits the link destination and appearance\",\"type\":\"\"},{\"click\":\"chrome:field:editcontrol({command:\\\"webedit:clearlink\\\"})\",\"header\":\"Clear Link\",\"icon\":\"/temp/iconcache/networkv2/16x16/link_delete.png\",\"disabledIcon\":\"/temp/link_delete_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Clears The Link\",\"type\":\"\"},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{8DF16CAD-F1FD-526C-846D-03616E8111C1}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"paramsLink\",\"expandedDisplayName\":null}</code><a class=\"font-weight-bold\" rel=\"noopener noreferrer\" title=\"&lt;a> title attribute\" href=\"https://dev.sitecore.net\" target=\"_blank\">Sitecore Dev Site</a><code class=\"scpm\" type=\"text/sitecore\" chromeType=\"field\" kind=\"close\"></code>",
                            "editableFirstPart": "<input id='fld_8DF16CADF1FD526C846D03616E8111C1_39EA2D44460558D4A7F380E839BD91C7_en_1_d420104dd70341c4b52a681428ac687e_479' class='scFieldValue' name='fld_8DF16CADF1FD526C846D03616E8111C1_39EA2D44460558D4A7F380E839BD91C7_en_1_d420104dd70341c4b52a681428ac687e_479' type='hidden' value=\"&lt;link target=&quot;_blank&quot; text=&quot;Sitecore Dev Site&quot; title=&quot;&amp;lt;a&amp;gt; title attribute&quot; url=&quot;https://dev.sitecore.net&quot; class=&quot;font-weight-bold&quot; linktype=&quot;external&quot; /&gt;\" /><code id=\"fld_8DF16CADF1FD526C846D03616E8111C1_39EA2D44460558D4A7F380E839BD91C7_en_1_d420104dd70341c4b52a681428ac687e_479_edit\" type=\"text/sitecore\" chromeType=\"field\" scFieldType=\"general link\" class=\"scpm\" kind=\"open\">{\"commands\":[{\"click\":\"chrome:field:editcontrol({command:\\\"webedit:editlink\\\"})\",\"header\":\"Edit link\",\"icon\":\"/temp/iconcache/networkv2/16x16/link_edit.png\",\"disabledIcon\":\"/temp/link_edit_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edits the link destination and appearance\",\"type\":\"\"},{\"click\":\"chrome:field:editcontrol({command:\\\"webedit:clearlink\\\"})\",\"header\":\"Clear Link\",\"icon\":\"/temp/iconcache/networkv2/16x16/link_delete.png\",\"disabledIcon\":\"/temp/link_delete_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Clears The Link\",\"type\":\"\"},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{8DF16CAD-F1FD-526C-846D-03616E8111C1}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"paramsLink\",\"expandedDisplayName\":null}</code><a class=\"font-weight-bold\" rel=\"noopener noreferrer\" title=\"&lt;a> title attribute\" href=\"https://dev.sitecore.net\" target=\"_blank\">Sitecore Dev Site",
                            "editableLastPart": "</a><code class=\"scpm\" type=\"text/sitecore\" chromeType=\"field\" kind=\"close\"></code>"
                          },
                          "heading": {
                            "value": "General Link",
                            "editable": "<input id='fld_8DF16CADF1FD526C846D03616E8111C1_2EF2548084095250A76CC3D867340008_en_1_d420104dd70341c4b52a681428ac687e_480' class='scFieldValue' name='fld_8DF16CADF1FD526C846D03616E8111C1_2EF2548084095250A76CC3D867340008_en_1_d420104dd70341c4b52a681428ac687e_480' type='hidden' value=\"General Link\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{8DF16CAD-F1FD-526C-846D-03616E8111C1}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"heading\",\"expandedDisplayName\":null}</span><span id=\"fld_8DF16CADF1FD526C846D03616E8111C1_2EF2548084095250A76CC3D867340008_en_1_d420104dd70341c4b52a681428ac687e_480_edit\" sc_parameters=\"prevent-line-break=true\" contenteditable=\"true\" class=\"scWebEditInput\" scFieldType=\"single-line text\" scDefaultText=\"[No text in field]\">General Link</span>"
                          },
                          "description": {
                            "value": "<p>A <em>General Link</em> is a field that represents an <code>&lt;a&gt;</code> tag.</p>",
                            "editable": "<input id='fld_8DF16CADF1FD526C846D03616E8111C1_439F9C97BCD65D8FBE16CFECCC159EFB_en_1_d420104dd70341c4b52a681428ac687e_481' class='scFieldValue' name='fld_8DF16CADF1FD526C846D03616E8111C1_439F9C97BCD65D8FBE16CFECCC159EFB_en_1_d420104dd70341c4b52a681428ac687e_481' type='hidden' value=\"&lt;p&gt;A &lt;em&gt;General Link&lt;/em&gt; is a field that represents an &lt;code&gt;&amp;lt;a&amp;gt;&lt;/code&gt; tag.&lt;/p&gt;\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:field:editcontrol({command:\\\"webedit:edithtml\\\"})\",\"header\":\"Edit Text\",\"icon\":\"/temp/iconcache/office/16x16/pencil.png\",\"disabledIcon\":\"/temp/pencil_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the text\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"bold\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_bold.png\",\"disabledIcon\":\"/temp/font_style_bold_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Bold\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"Italic\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_italics.png\",\"disabledIcon\":\"/temp/font_style_italics_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Italic\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"Underline\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_underline.png\",\"disabledIcon\":\"/temp/font_style_underline_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Underline\",\"type\":null},{\"click\":\"chrome:field:insertexternallink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/earth_link.png\",\"disabledIcon\":\"/temp/earth_link_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert an external link into the text field.\",\"type\":null},{\"click\":\"chrome:field:insertlink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/link.png\",\"disabledIcon\":\"/temp/link_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert a link into the text field.\",\"type\":null},{\"click\":\"chrome:field:removelink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/link_broken.png\",\"disabledIcon\":\"/temp/link_broken_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Remove link.\",\"type\":null},{\"click\":\"chrome:field:insertimage\",\"header\":\"Insert image\",\"icon\":\"/temp/iconcache/office/16x16/photo_landscape.png\",\"disabledIcon\":\"/temp/photo_landscape_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert an image into the text field.\",\"type\":null},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{8DF16CAD-F1FD-526C-846D-03616E8111C1}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"description\",\"expandedDisplayName\":null}</span><span scFieldType=\"rich text\" scDefaultText=\"[No text in field]\" contenteditable=\"true\" class=\"scWebEditInput\" id=\"fld_8DF16CADF1FD526C846D03616E8111C1_439F9C97BCD65D8FBE16CFECCC159EFB_en_1_d420104dd70341c4b52a681428ac687e_481_edit\"><p>A <em>General Link</em> is a field that represents an <code>&lt;a&gt;</code> tag.</p></span>"
                          }
                        }
                      },
                      {
                        "name": "code",
                        "type": "text/sitecore",
                        "contents": "",
                        "attributes": {
                          "type": "text/sitecore",
                          "id": "scEnclosingTag_r_",
                          "chrometype": "rendering",
                          "kind": "close",
                          "hintkey": "Styleguide-FieldUsage-Link",
                          "class": "scpm"
                        }
                      },
                      {
                        "name": "code",
                        "type": "text/sitecore",
                        "contents": "{\"commands\":[{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:fieldeditor(command={2F7D34E2-EB93-47ED-8177-B1F429A9FCD7},fields=sharedItemLink|localItemLink|heading|description,id={E10C4780-6266-52E3-9E85-6749541E387D})',null,false)\",\"header\":\"Edit Fields\",\"icon\":\"/temp/iconcache/office/16x16/pencil.png\",\"disabledIcon\":\"/temp/pencil_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"\",\"type\":null},{\"click\":\"chrome:dummy\",\"header\":\"Separator\",\"icon\":\"\",\"disabledIcon\":\"\",\"isDivider\":false,\"tooltip\":null,\"type\":\"separator\"},{\"click\":\"chrome:rendering:sort\",\"header\":\"Change position\",\"icon\":\"/temp/iconcache/office/16x16/document_size.png\",\"disabledIcon\":\"/temp/document_size_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Move component.\",\"type\":\"\"},{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:componentoptions(referenceId={A44AD1F8-0582-5248-9DF9-52429193A68B},renderingId={0B711DC9-9DD2-568A-AECB-D75DBA2F8DCD},id={E10C4780-6266-52E3-9E85-6749541E387D})',null,false)\",\"header\":\"Edit Experience Editor Options\",\"icon\":\"/temp/iconcache/office/16x16/clipboard_check_edit.png\",\"disabledIcon\":\"/temp/clipboard_check_edit_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the Experience Editor options for the component.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:properties\",\"header\":\"Edit component properties\",\"icon\":\"/temp/iconcache/office/16x16/elements_branch.png\",\"disabledIcon\":\"/temp/elements_branch_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the properties for the component.\",\"type\":\"common\"},{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:setdatasource(referenceId={A44AD1F8-0582-5248-9DF9-52429193A68B},renderingId={0B711DC9-9DD2-568A-AECB-D75DBA2F8DCD},id={E10C4780-6266-52E3-9E85-6749541E387D})',null,false)\",\"header\":\"dsHeaderParameter\",\"icon\":\"/temp/iconcache/office/16x16/data.png\",\"disabledIcon\":\"/temp/data_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"dsTooltipParameter\",\"type\":\"datasourcesmenu\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Test the component.\",\"type\":\"sticky\"},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"datasourcesmenu\"},{\"click\":\"chrome:rendering:delete\",\"header\":\"Delete\",\"icon\":\"/temp/iconcache/office/16x16/delete.png\",\"disabledIcon\":\"/temp/delete_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Remove component.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{E10C4780-6266-52E3-9E85-6749541E387D}?lang=en&ver=1\",\"custom\":{\"renderingID\":\"0B711DC99DD2568AAECBD75DBA2F8DCD\",\"editable\":\"true\"},\"displayName\":\"Styleguide-FieldUsage-ItemLink\",\"expandedDisplayName\":null}",
                        "attributes": {
                          "type": "text/sitecore",
                          "chrometype": "rendering",
                          "kind": "open",
                          "hintname": "Styleguide-FieldUsage-ItemLink",
                          "id": "r_A44AD1F8058252489DF952429193A68B",
                          "class": "scpm",
                          "data-selectable": "true"
                        }
                      },
                      {
                        "uid": "a44ad1f8-0582-5248-9df9-52429193a68b",
                        "componentName": "Styleguide-FieldUsage-ItemLink",
                        "dataSource": "{E10C4780-6266-52E3-9E85-6749541E387D}",
                        "params": {},
                        "fields": {
                          "sharedItemLink": {
                            "id": "8be4935f-8729-5b02-b39b-395958ecf125",
                            "url": "/jss/Content/Styleguide/ItemLinkField/Item1",
                            "fields": {
                              "textField": {
                                "value": "ItemLink Demo (Shared) Item 1 Text Field",
                                "editable": "<input id='fld_8BE4935F87295B02B39B395958ECF125_4C2F73338F5D5086B0A022BA2A426A29_en_1_2276569e04594561ad003026bd698093_482' class='scFieldValue' name='fld_8BE4935F87295B02B39B395958ECF125_4C2F73338F5D5086B0A022BA2A426A29_en_1_2276569e04594561ad003026bd698093_482' type='hidden' value=\"ItemLink Demo (Shared) Item 1 Text Field\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{8BE4935F-8729-5B02-B39B-395958ECF125}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"textField\",\"expandedDisplayName\":null}</span><span id=\"fld_8BE4935F87295B02B39B395958ECF125_4C2F73338F5D5086B0A022BA2A426A29_en_1_2276569e04594561ad003026bd698093_482_edit\" sc_parameters=\"prevent-line-break=true\" contenteditable=\"true\" class=\"scWebEditInput\" scFieldType=\"single-line text\" scDefaultText=\"[No text in field]\">ItemLink Demo (Shared) Item 1 Text Field</span>"
                              }
                            }
                          },
                          "localItemLink": {
                            "id": "155c19a5-0694-508b-ab6c-099ed7e825c7",
                            "url": "/jss/home/styleguide/Page-Components/styleguide-jss-styleguide-section-B73482E131E5A083D77A50554BC74A4758E29636DF6824F6E2F272EE778C28A095/styleguide-jss-styleguide-section-B75151F05CFDC4CAFFE44E5BAED9D59BEA82565EC11CE75B7DEF3634495EC1DAB7",
                            "fields": {
                              "textField": {
                                "value": "Referenced item textField",
                                "editable": "<input id='fld_155C19A50694508BAB6C099ED7E825C7_4C2F73338F5D5086B0A022BA2A426A29_en_1_c49102fe47f541f68c6999bc59b73c55_483' class='scFieldValue' name='fld_155C19A50694508BAB6C099ED7E825C7_4C2F73338F5D5086B0A022BA2A426A29_en_1_c49102fe47f541f68c6999bc59b73c55_483' type='hidden' value=\"Referenced item textField\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{155C19A5-0694-508B-AB6C-099ED7E825C7}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"textField\",\"expandedDisplayName\":null}</span><span id=\"fld_155C19A50694508BAB6C099ED7E825C7_4C2F73338F5D5086B0A022BA2A426A29_en_1_c49102fe47f541f68c6999bc59b73c55_483_edit\" sc_parameters=\"prevent-line-break=true\" contenteditable=\"true\" class=\"scWebEditInput\" scFieldType=\"single-line text\" scDefaultText=\"[No text in field]\">Referenced item textField</span>"
                              }
                            }
                          },
                          "heading": {
                            "value": "Item Link",
                            "editable": "<input id='fld_E10C4780626652E39E856749541E387D_2EF2548084095250A76CC3D867340008_en_1_f14101c406f347b7adb686a80c0537a0_484' class='scFieldValue' name='fld_E10C4780626652E39E856749541E387D_2EF2548084095250A76CC3D867340008_en_1_f14101c406f347b7adb686a80c0537a0_484' type='hidden' value=\"Item Link\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{E10C4780-6266-52E3-9E85-6749541E387D}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"heading\",\"expandedDisplayName\":null}</span><span id=\"fld_E10C4780626652E39E856749541E387D_2EF2548084095250A76CC3D867340008_en_1_f14101c406f347b7adb686a80c0537a0_484_edit\" sc_parameters=\"prevent-line-break=true\" contenteditable=\"true\" class=\"scWebEditInput\" scFieldType=\"single-line text\" scDefaultText=\"[No text in field]\">Item Link</span>"
                          },
                          "description": {
                            "value": "<p>\n  <small>\n    Item Links are a way to reference another content item to use data from it.\n    Referenced items may be shared.\n    To reference multiple content items, use a <em>Content List</em> field.<br />\n    <strong>Note:</strong> Sitecore does not support inline editing of Item Link fields. The value must be edited in Experience Editor by using the edit rendering fields button (looks like a pencil) with the whole component selected.\n  </small>\n</p>\n",
                            "editable": "<input id='fld_E10C4780626652E39E856749541E387D_439F9C97BCD65D8FBE16CFECCC159EFB_en_1_f14101c406f347b7adb686a80c0537a0_485' class='scFieldValue' name='fld_E10C4780626652E39E856749541E387D_439F9C97BCD65D8FBE16CFECCC159EFB_en_1_f14101c406f347b7adb686a80c0537a0_485' type='hidden' value=\"&lt;p&gt;\n  &lt;small&gt;\n    Item Links are a way to reference another content item to use data from it.\n    Referenced items may be shared.\n    To reference multiple content items, use a &lt;em&gt;Content List&lt;/em&gt; field.&lt;br /&gt;\n    &lt;strong&gt;Note:&lt;/strong&gt; Sitecore does not support inline editing of Item Link fields. The value must be edited in Experience Editor by using the edit rendering fields button (looks like a pencil) with the whole component selected.\n  &lt;/small&gt;\n&lt;/p&gt;\n\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:field:editcontrol({command:\\\"webedit:edithtml\\\"})\",\"header\":\"Edit Text\",\"icon\":\"/temp/iconcache/office/16x16/pencil.png\",\"disabledIcon\":\"/temp/pencil_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the text\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"bold\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_bold.png\",\"disabledIcon\":\"/temp/font_style_bold_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Bold\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"Italic\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_italics.png\",\"disabledIcon\":\"/temp/font_style_italics_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Italic\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"Underline\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_underline.png\",\"disabledIcon\":\"/temp/font_style_underline_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Underline\",\"type\":null},{\"click\":\"chrome:field:insertexternallink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/earth_link.png\",\"disabledIcon\":\"/temp/earth_link_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert an external link into the text field.\",\"type\":null},{\"click\":\"chrome:field:insertlink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/link.png\",\"disabledIcon\":\"/temp/link_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert a link into the text field.\",\"type\":null},{\"click\":\"chrome:field:removelink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/link_broken.png\",\"disabledIcon\":\"/temp/link_broken_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Remove link.\",\"type\":null},{\"click\":\"chrome:field:insertimage\",\"header\":\"Insert image\",\"icon\":\"/temp/iconcache/office/16x16/photo_landscape.png\",\"disabledIcon\":\"/temp/photo_landscape_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert an image into the text field.\",\"type\":null},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{E10C4780-6266-52E3-9E85-6749541E387D}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"description\",\"expandedDisplayName\":null}</span><span scFieldType=\"rich text\" scDefaultText=\"[No text in field]\" contenteditable=\"true\" class=\"scWebEditInput\" id=\"fld_E10C4780626652E39E856749541E387D_439F9C97BCD65D8FBE16CFECCC159EFB_en_1_f14101c406f347b7adb686a80c0537a0_485_edit\"><p>\n  <small>\n    Item Links are a way to reference another content item to use data from it.\n    Referenced items may be shared.\n    To reference multiple content items, use a <em>Content List</em> field.<br />\n    <strong>Note:</strong> Sitecore does not support inline editing of Item Link fields. The value must be edited in Experience Editor by using the edit rendering fields button (looks like a pencil) with the whole component selected.\n  </small>\n</p>\n</span>"
                          }
                        }
                      },
                      {
                        "name": "code",
                        "type": "text/sitecore",
                        "contents": "",
                        "attributes": {
                          "type": "text/sitecore",
                          "id": "scEnclosingTag_r_",
                          "chrometype": "rendering",
                          "kind": "close",
                          "hintkey": "Styleguide-FieldUsage-ItemLink",
                          "class": "scpm"
                        }
                      },
                      {
                        "name": "code",
                        "type": "text/sitecore",
                        "contents": "{\"commands\":[{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:fieldeditor(command={2F7D34E2-EB93-47ED-8177-B1F429A9FCD7},fields=sharedContentList|localContentList|heading|description,id={00F84515-7381-5B95-9A96-FE75422FC267})',null,false)\",\"header\":\"Edit Fields\",\"icon\":\"/temp/iconcache/office/16x16/pencil.png\",\"disabledIcon\":\"/temp/pencil_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"\",\"type\":null},{\"click\":\"chrome:dummy\",\"header\":\"Separator\",\"icon\":\"\",\"disabledIcon\":\"\",\"isDivider\":false,\"tooltip\":null,\"type\":\"separator\"},{\"click\":\"chrome:rendering:sort\",\"header\":\"Change position\",\"icon\":\"/temp/iconcache/office/16x16/document_size.png\",\"disabledIcon\":\"/temp/document_size_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Move component.\",\"type\":\"\"},{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:componentoptions(referenceId={2F609D40-8AD9-540E-901E-23AA2600F3EB},renderingId={5E747641-F9A4-57EE-B625-73D976639071},id={00F84515-7381-5B95-9A96-FE75422FC267})',null,false)\",\"header\":\"Edit Experience Editor Options\",\"icon\":\"/temp/iconcache/office/16x16/clipboard_check_edit.png\",\"disabledIcon\":\"/temp/clipboard_check_edit_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the Experience Editor options for the component.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:properties\",\"header\":\"Edit component properties\",\"icon\":\"/temp/iconcache/office/16x16/elements_branch.png\",\"disabledIcon\":\"/temp/elements_branch_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the properties for the component.\",\"type\":\"common\"},{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:setdatasource(referenceId={2F609D40-8AD9-540E-901E-23AA2600F3EB},renderingId={5E747641-F9A4-57EE-B625-73D976639071},id={00F84515-7381-5B95-9A96-FE75422FC267})',null,false)\",\"header\":\"dsHeaderParameter\",\"icon\":\"/temp/iconcache/office/16x16/data.png\",\"disabledIcon\":\"/temp/data_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"dsTooltipParameter\",\"type\":\"datasourcesmenu\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Test the component.\",\"type\":\"sticky\"},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"datasourcesmenu\"},{\"click\":\"chrome:rendering:delete\",\"header\":\"Delete\",\"icon\":\"/temp/iconcache/office/16x16/delete.png\",\"disabledIcon\":\"/temp/delete_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Remove component.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{00F84515-7381-5B95-9A96-FE75422FC267}?lang=en&ver=1\",\"custom\":{\"renderingID\":\"5E747641F9A457EEB62573D976639071\",\"editable\":\"true\"},\"displayName\":\"Styleguide-FieldUsage-ContentList\",\"expandedDisplayName\":null}",
                        "attributes": {
                          "type": "text/sitecore",
                          "chrometype": "rendering",
                          "kind": "open",
                          "hintname": "Styleguide-FieldUsage-ContentList",
                          "id": "r_2F609D408AD9540E901E23AA2600F3EB",
                          "class": "scpm",
                          "data-selectable": "true"
                        }
                      },
                      {
                        "uid": "2f609d40-8ad9-540e-901e-23aa2600f3eb",
                        "componentName": "Styleguide-FieldUsage-ContentList",
                        "dataSource": "{00F84515-7381-5B95-9A96-FE75422FC267}",
                        "params": {},
                        "fields": {
                          "sharedContentList": [
                            {
                              "id": "421d783c-ab27-5590-9e2e-ffecd8ba31e2",
                              "fields": {
                                "textField": {
                                  "value": "ContentList Demo (Shared) Item 1 Text Field",
                                  "editable": "<input id='fld_421D783CAB2755909E2EFFECD8BA31E2_629EFFF1395955A2A2C905CF4B1F7D3E_en_1_0f7a179c72ab4fdaa41dbcecca8ca5be_486' class='scFieldValue' name='fld_421D783CAB2755909E2EFFECD8BA31E2_629EFFF1395955A2A2C905CF4B1F7D3E_en_1_0f7a179c72ab4fdaa41dbcecca8ca5be_486' type='hidden' value=\"ContentList Demo (Shared) Item 1 Text Field\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{421D783C-AB27-5590-9E2E-FFECD8BA31E2}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"textField\",\"expandedDisplayName\":null}</span><span id=\"fld_421D783CAB2755909E2EFFECD8BA31E2_629EFFF1395955A2A2C905CF4B1F7D3E_en_1_0f7a179c72ab4fdaa41dbcecca8ca5be_486_edit\" sc_parameters=\"prevent-line-break=true\" contenteditable=\"true\" class=\"scWebEditInput\" scFieldType=\"single-line text\" scDefaultText=\"[No text in field]\">ContentList Demo (Shared) Item 1 Text Field</span>"
                                }
                              }
                            },
                            {
                              "id": "6f4568ca-b1fe-51ad-b313-ef4840dce734",
                              "fields": {
                                "textField": {
                                  "value": "ContentList Demo (Shared) Item 2 Text Field",
                                  "editable": "<input id='fld_6F4568CAB1FE51ADB313EF4840DCE734_629EFFF1395955A2A2C905CF4B1F7D3E_en_1_7e2e880d0f724574bd03f5bb391971fb_487' class='scFieldValue' name='fld_6F4568CAB1FE51ADB313EF4840DCE734_629EFFF1395955A2A2C905CF4B1F7D3E_en_1_7e2e880d0f724574bd03f5bb391971fb_487' type='hidden' value=\"ContentList Demo (Shared) Item 2 Text Field\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{6F4568CA-B1FE-51AD-B313-EF4840DCE734}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"textField\",\"expandedDisplayName\":null}</span><span id=\"fld_6F4568CAB1FE51ADB313EF4840DCE734_629EFFF1395955A2A2C905CF4B1F7D3E_en_1_7e2e880d0f724574bd03f5bb391971fb_487_edit\" sc_parameters=\"prevent-line-break=true\" contenteditable=\"true\" class=\"scWebEditInput\" scFieldType=\"single-line text\" scDefaultText=\"[No text in field]\">ContentList Demo (Shared) Item 2 Text Field</span>"
                                }
                              }
                            }
                          ],
                          "localContentList": [
                            {
                              "id": "3b967777-c8bf-56a2-b100-5d91bd39d7b9",
                              "fields": {
                                "textField": {
                                  "value": "Hello World Item 1",
                                  "editable": "<input id='fld_3B967777C8BF56A2B1005D91BD39D7B9_629EFFF1395955A2A2C905CF4B1F7D3E_en_1_181fa45564c94ffbb75a7ad24e48c5d6_488' class='scFieldValue' name='fld_3B967777C8BF56A2B1005D91BD39D7B9_629EFFF1395955A2A2C905CF4B1F7D3E_en_1_181fa45564c94ffbb75a7ad24e48c5d6_488' type='hidden' value=\"Hello World Item 1\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{3B967777-C8BF-56A2-B100-5D91BD39D7B9}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"textField\",\"expandedDisplayName\":null}</span><span id=\"fld_3B967777C8BF56A2B1005D91BD39D7B9_629EFFF1395955A2A2C905CF4B1F7D3E_en_1_181fa45564c94ffbb75a7ad24e48c5d6_488_edit\" sc_parameters=\"prevent-line-break=true\" contenteditable=\"true\" class=\"scWebEditInput\" scFieldType=\"single-line text\" scDefaultText=\"[No text in field]\">Hello World Item 1</span>"
                                }
                              }
                            },
                            {
                              "id": "0c8049eb-b256-5b45-8b97-023bc43b2121",
                              "fields": {
                                "textField": {
                                  "value": "Hello World Item 2",
                                  "editable": "<input id='fld_0C8049EBB2565B458B97023BC43B2121_629EFFF1395955A2A2C905CF4B1F7D3E_en_1_8511c12f670c4f928655e09b801050ae_489' class='scFieldValue' name='fld_0C8049EBB2565B458B97023BC43B2121_629EFFF1395955A2A2C905CF4B1F7D3E_en_1_8511c12f670c4f928655e09b801050ae_489' type='hidden' value=\"Hello World Item 2\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{0C8049EB-B256-5B45-8B97-023BC43B2121}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"textField\",\"expandedDisplayName\":null}</span><span id=\"fld_0C8049EBB2565B458B97023BC43B2121_629EFFF1395955A2A2C905CF4B1F7D3E_en_1_8511c12f670c4f928655e09b801050ae_489_edit\" sc_parameters=\"prevent-line-break=true\" contenteditable=\"true\" class=\"scWebEditInput\" scFieldType=\"single-line text\" scDefaultText=\"[No text in field]\">Hello World Item 2</span>"
                                }
                              }
                            }
                          ],
                          "heading": {
                            "value": "Content List",
                            "editable": "<input id='fld_00F8451573815B959A96FE75422FC267_2EF2548084095250A76CC3D867340008_en_1_a1ae5c7571b74b0b9b74761c7779602e_490' class='scFieldValue' name='fld_00F8451573815B959A96FE75422FC267_2EF2548084095250A76CC3D867340008_en_1_a1ae5c7571b74b0b9b74761c7779602e_490' type='hidden' value=\"Content List\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{00F84515-7381-5B95-9A96-FE75422FC267}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"heading\",\"expandedDisplayName\":null}</span><span id=\"fld_00F8451573815B959A96FE75422FC267_2EF2548084095250A76CC3D867340008_en_1_a1ae5c7571b74b0b9b74761c7779602e_490_edit\" sc_parameters=\"prevent-line-break=true\" contenteditable=\"true\" class=\"scWebEditInput\" scFieldType=\"single-line text\" scDefaultText=\"[No text in field]\">Content List</span>"
                          },
                          "description": {
                            "value": "<p>\n  <small>\n    Content Lists are a way to reference zero or more other content items.\n    Referenced items may be shared.\n    To reference a single content item, use an <em>Item Link</em> field.<br />\n    <strong>Note:</strong> Sitecore does not support inline editing of Content List fields. The value must be edited in Experience Editor by using the edit rendering fields button (looks like a pencil) with the whole component selected.\n  </small>\n</p>\n",
                            "editable": "<input id='fld_00F8451573815B959A96FE75422FC267_439F9C97BCD65D8FBE16CFECCC159EFB_en_1_a1ae5c7571b74b0b9b74761c7779602e_491' class='scFieldValue' name='fld_00F8451573815B959A96FE75422FC267_439F9C97BCD65D8FBE16CFECCC159EFB_en_1_a1ae5c7571b74b0b9b74761c7779602e_491' type='hidden' value=\"&lt;p&gt;\n  &lt;small&gt;\n    Content Lists are a way to reference zero or more other content items.\n    Referenced items may be shared.\n    To reference a single content item, use an &lt;em&gt;Item Link&lt;/em&gt; field.&lt;br /&gt;\n    &lt;strong&gt;Note:&lt;/strong&gt; Sitecore does not support inline editing of Content List fields. The value must be edited in Experience Editor by using the edit rendering fields button (looks like a pencil) with the whole component selected.\n  &lt;/small&gt;\n&lt;/p&gt;\n\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:field:editcontrol({command:\\\"webedit:edithtml\\\"})\",\"header\":\"Edit Text\",\"icon\":\"/temp/iconcache/office/16x16/pencil.png\",\"disabledIcon\":\"/temp/pencil_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the text\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"bold\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_bold.png\",\"disabledIcon\":\"/temp/font_style_bold_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Bold\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"Italic\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_italics.png\",\"disabledIcon\":\"/temp/font_style_italics_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Italic\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"Underline\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_underline.png\",\"disabledIcon\":\"/temp/font_style_underline_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Underline\",\"type\":null},{\"click\":\"chrome:field:insertexternallink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/earth_link.png\",\"disabledIcon\":\"/temp/earth_link_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert an external link into the text field.\",\"type\":null},{\"click\":\"chrome:field:insertlink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/link.png\",\"disabledIcon\":\"/temp/link_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert a link into the text field.\",\"type\":null},{\"click\":\"chrome:field:removelink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/link_broken.png\",\"disabledIcon\":\"/temp/link_broken_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Remove link.\",\"type\":null},{\"click\":\"chrome:field:insertimage\",\"header\":\"Insert image\",\"icon\":\"/temp/iconcache/office/16x16/photo_landscape.png\",\"disabledIcon\":\"/temp/photo_landscape_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert an image into the text field.\",\"type\":null},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{00F84515-7381-5B95-9A96-FE75422FC267}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"description\",\"expandedDisplayName\":null}</span><span scFieldType=\"rich text\" scDefaultText=\"[No text in field]\" contenteditable=\"true\" class=\"scWebEditInput\" id=\"fld_00F8451573815B959A96FE75422FC267_439F9C97BCD65D8FBE16CFECCC159EFB_en_1_a1ae5c7571b74b0b9b74761c7779602e_491_edit\"><p>\n  <small>\n    Content Lists are a way to reference zero or more other content items.\n    Referenced items may be shared.\n    To reference a single content item, use an <em>Item Link</em> field.<br />\n    <strong>Note:</strong> Sitecore does not support inline editing of Content List fields. The value must be edited in Experience Editor by using the edit rendering fields button (looks like a pencil) with the whole component selected.\n  </small>\n</p>\n</span>"
                          }
                        }
                      },
                      {
                        "name": "code",
                        "type": "text/sitecore",
                        "contents": "",
                        "attributes": {
                          "type": "text/sitecore",
                          "id": "scEnclosingTag_r_",
                          "chrometype": "rendering",
                          "kind": "close",
                          "hintkey": "Styleguide-FieldUsage-ContentList",
                          "class": "scpm"
                        }
                      },
                      {
                        "name": "code",
                        "type": "text/sitecore",
                        "contents": "{\"commands\":[{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:fieldeditor(command={2F7D34E2-EB93-47ED-8177-B1F429A9FCD7},fields=customIntField|heading|description,id={9B399679-88D1-5496-A507-37548FC072B1})',null,false)\",\"header\":\"Edit Fields\",\"icon\":\"/temp/iconcache/office/16x16/pencil.png\",\"disabledIcon\":\"/temp/pencil_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"\",\"type\":null},{\"click\":\"chrome:dummy\",\"header\":\"Separator\",\"icon\":\"\",\"disabledIcon\":\"\",\"isDivider\":false,\"tooltip\":null,\"type\":\"separator\"},{\"click\":\"chrome:rendering:sort\",\"header\":\"Change position\",\"icon\":\"/temp/iconcache/office/16x16/document_size.png\",\"disabledIcon\":\"/temp/document_size_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Move component.\",\"type\":\"\"},{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:componentoptions(referenceId={352ED63D-796A-5523-89F5-9A991DDA4A8F},renderingId={5EB0D613-6DAF-5CAA-8F56-5175867EC5BC},id={9B399679-88D1-5496-A507-37548FC072B1})',null,false)\",\"header\":\"Edit Experience Editor Options\",\"icon\":\"/temp/iconcache/office/16x16/clipboard_check_edit.png\",\"disabledIcon\":\"/temp/clipboard_check_edit_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the Experience Editor options for the component.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:properties\",\"header\":\"Edit component properties\",\"icon\":\"/temp/iconcache/office/16x16/elements_branch.png\",\"disabledIcon\":\"/temp/elements_branch_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the properties for the component.\",\"type\":\"common\"},{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:setdatasource(referenceId={352ED63D-796A-5523-89F5-9A991DDA4A8F},renderingId={5EB0D613-6DAF-5CAA-8F56-5175867EC5BC},id={9B399679-88D1-5496-A507-37548FC072B1})',null,false)\",\"header\":\"dsHeaderParameter\",\"icon\":\"/temp/iconcache/office/16x16/data.png\",\"disabledIcon\":\"/temp/data_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"dsTooltipParameter\",\"type\":\"datasourcesmenu\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Test the component.\",\"type\":\"sticky\"},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"datasourcesmenu\"},{\"click\":\"chrome:rendering:delete\",\"header\":\"Delete\",\"icon\":\"/temp/iconcache/office/16x16/delete.png\",\"disabledIcon\":\"/temp/delete_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Remove component.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{9B399679-88D1-5496-A507-37548FC072B1}?lang=en&ver=1\",\"custom\":{\"renderingID\":\"5EB0D6136DAF5CAA8F565175867EC5BC\",\"editable\":\"true\"},\"displayName\":\"Styleguide-FieldUsage-Custom\",\"expandedDisplayName\":null}",
                        "attributes": {
                          "type": "text/sitecore",
                          "chrometype": "rendering",
                          "kind": "open",
                          "hintname": "Styleguide-FieldUsage-Custom",
                          "id": "r_352ED63D796A552389F59A991DDA4A8F",
                          "class": "scpm",
                          "data-selectable": "true"
                        }
                      },
                      {
                        "uid": "352ed63d-796a-5523-89f5-9a991dda4a8f",
                        "componentName": "Styleguide-FieldUsage-Custom",
                        "dataSource": "{9B399679-88D1-5496-A507-37548FC072B1}",
                        "params": {},
                        "fields": {
                          "customIntField": {
                            "value": "31337",
                            "editable": "<input id='fld_9B39967988D15496A50737548FC072B1_38F8E561D5315C08A9C8B102DEEFEB82_en_1_6df942289c5d4986925e026b4c9e70ff_492' class='scFieldValue' name='fld_9B39967988D15496A50737548FC072B1_38F8E561D5315C08A9C8B102DEEFEB82_en_1_6df942289c5d4986925e026b4c9e70ff_492' type='hidden' value=\"31337\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:field:editcontrol({command:\\\"webedit:editinteger\\\"})\",\"header\":\"Edit integer\",\"icon\":\"/temp/iconcache/wordprocessing/16x16/word_count.png\",\"disabledIcon\":\"/temp/word_count_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit integer\",\"type\":\"\"},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{9B399679-88D1-5496-A507-37548FC072B1}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"customIntField\",\"expandedDisplayName\":null}</span><span scFieldType=\"integer\" scDefaultText=\"[No text in field]\" contenteditable=\"true\" class=\"scWebEditInput\" id=\"fld_9B39967988D15496A50737548FC072B1_38F8E561D5315C08A9C8B102DEEFEB82_en_1_6df942289c5d4986925e026b4c9e70ff_492_edit\">31337</span>"
                          },
                          "heading": {
                            "value": "Custom Fields",
                            "editable": "<input id='fld_9B39967988D15496A50737548FC072B1_2EF2548084095250A76CC3D867340008_en_1_6df942289c5d4986925e026b4c9e70ff_493' class='scFieldValue' name='fld_9B39967988D15496A50737548FC072B1_2EF2548084095250A76CC3D867340008_en_1_6df942289c5d4986925e026b4c9e70ff_493' type='hidden' value=\"Custom Fields\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{9B399679-88D1-5496-A507-37548FC072B1}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"heading\",\"expandedDisplayName\":null}</span><span id=\"fld_9B39967988D15496A50737548FC072B1_2EF2548084095250A76CC3D867340008_en_1_6df942289c5d4986925e026b4c9e70ff_493_edit\" sc_parameters=\"prevent-line-break=true\" contenteditable=\"true\" class=\"scWebEditInput\" scFieldType=\"single-line text\" scDefaultText=\"[No text in field]\">Custom Fields</span>"
                          },
                          "description": {
                            "value": "<p>\n  <small>\n    Any Sitecore field type can be consumed by JSS.\n    In this sample we consume the <em>Integer</em> field type.<br />\n    <strong>Note:</strong> For field types with complex data, custom <code>FieldSerializer</code>s may need to be implemented on the Sitecore side.\n  </small>\n</p>\n",
                            "editable": "<input id='fld_9B39967988D15496A50737548FC072B1_439F9C97BCD65D8FBE16CFECCC159EFB_en_1_6df942289c5d4986925e026b4c9e70ff_494' class='scFieldValue' name='fld_9B39967988D15496A50737548FC072B1_439F9C97BCD65D8FBE16CFECCC159EFB_en_1_6df942289c5d4986925e026b4c9e70ff_494' type='hidden' value=\"&lt;p&gt;\n  &lt;small&gt;\n    Any Sitecore field type can be consumed by JSS.\n    In this sample we consume the &lt;em&gt;Integer&lt;/em&gt; field type.&lt;br /&gt;\n    &lt;strong&gt;Note:&lt;/strong&gt; For field types with complex data, custom &lt;code&gt;FieldSerializer&lt;/code&gt;s may need to be implemented on the Sitecore side.\n  &lt;/small&gt;\n&lt;/p&gt;\n\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:field:editcontrol({command:\\\"webedit:edithtml\\\"})\",\"header\":\"Edit Text\",\"icon\":\"/temp/iconcache/office/16x16/pencil.png\",\"disabledIcon\":\"/temp/pencil_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the text\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"bold\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_bold.png\",\"disabledIcon\":\"/temp/font_style_bold_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Bold\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"Italic\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_italics.png\",\"disabledIcon\":\"/temp/font_style_italics_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Italic\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"Underline\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_underline.png\",\"disabledIcon\":\"/temp/font_style_underline_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Underline\",\"type\":null},{\"click\":\"chrome:field:insertexternallink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/earth_link.png\",\"disabledIcon\":\"/temp/earth_link_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert an external link into the text field.\",\"type\":null},{\"click\":\"chrome:field:insertlink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/link.png\",\"disabledIcon\":\"/temp/link_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert a link into the text field.\",\"type\":null},{\"click\":\"chrome:field:removelink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/link_broken.png\",\"disabledIcon\":\"/temp/link_broken_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Remove link.\",\"type\":null},{\"click\":\"chrome:field:insertimage\",\"header\":\"Insert image\",\"icon\":\"/temp/iconcache/office/16x16/photo_landscape.png\",\"disabledIcon\":\"/temp/photo_landscape_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert an image into the text field.\",\"type\":null},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{9B399679-88D1-5496-A507-37548FC072B1}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"description\",\"expandedDisplayName\":null}</span><span scFieldType=\"rich text\" scDefaultText=\"[No text in field]\" contenteditable=\"true\" class=\"scWebEditInput\" id=\"fld_9B39967988D15496A50737548FC072B1_439F9C97BCD65D8FBE16CFECCC159EFB_en_1_6df942289c5d4986925e026b4c9e70ff_494_edit\"><p>\n  <small>\n    Any Sitecore field type can be consumed by JSS.\n    In this sample we consume the <em>Integer</em> field type.<br />\n    <strong>Note:</strong> For field types with complex data, custom <code>FieldSerializer</code>s may need to be implemented on the Sitecore side.\n  </small>\n</p>\n</span>"
                          }
                        }
                      },
                      {
                        "name": "code",
                        "type": "text/sitecore",
                        "contents": "",
                        "attributes": {
                          "type": "text/sitecore",
                          "id": "scEnclosingTag_r_",
                          "chrometype": "rendering",
                          "kind": "close",
                          "hintkey": "Styleguide-FieldUsage-Custom",
                          "class": "scpm"
                        }
                      },
                      {
                        "name": "code",
                        "type": "text/sitecore",
                        "contents": "",
                        "attributes": {
                          "type": "text/sitecore",
                          "id": "scEnclosingTag_",
                          "chrometype": "placeholder",
                          "kind": "close",
                          "hintname": "jss-styleguide-section",
                          "class": "scpm"
                        }
                      }
                    ]
                  }
                },
                {
                  "name": "code",
                  "type": "text/sitecore",
                  "contents": "",
                  "attributes": {
                    "type": "text/sitecore",
                    "id": "scEnclosingTag_r_",
                    "chrometype": "rendering",
                    "kind": "close",
                    "hintkey": "Styleguide-Section",
                    "class": "scpm"
                  }
                },
                {
                  "name": "code",
                  "type": "text/sitecore",
                  "contents": "{\"commands\":[{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:fieldeditor(command={2F7D34E2-EB93-47ED-8177-B1F429A9FCD7},fields=heading,id={271FD7C0-3D11-5E65-8E76-7F0B0DDB4D1C})',null,false)\",\"header\":\"Edit Fields\",\"icon\":\"/temp/iconcache/office/16x16/pencil.png\",\"disabledIcon\":\"/temp/pencil_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"\",\"type\":null},{\"click\":\"chrome:dummy\",\"header\":\"Separator\",\"icon\":\"\",\"disabledIcon\":\"\",\"isDivider\":false,\"tooltip\":null,\"type\":\"separator\"},{\"click\":\"chrome:rendering:sort\",\"header\":\"Change position\",\"icon\":\"/temp/iconcache/office/16x16/document_size.png\",\"disabledIcon\":\"/temp/document_size_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Move component.\",\"type\":\"\"},{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:componentoptions(referenceId={7DE41A1A-24E4-5963-8206-3BB0B7D9DD69},renderingId={DADE61AE-8C27-5EC1-A5B1-B02CC436C4E1},id={271FD7C0-3D11-5E65-8E76-7F0B0DDB4D1C})',null,false)\",\"header\":\"Edit Experience Editor Options\",\"icon\":\"/temp/iconcache/office/16x16/clipboard_check_edit.png\",\"disabledIcon\":\"/temp/clipboard_check_edit_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the Experience Editor options for the component.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:properties\",\"header\":\"Edit component properties\",\"icon\":\"/temp/iconcache/office/16x16/elements_branch.png\",\"disabledIcon\":\"/temp/elements_branch_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the properties for the component.\",\"type\":\"common\"},{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:setdatasource(referenceId={7DE41A1A-24E4-5963-8206-3BB0B7D9DD69},renderingId={DADE61AE-8C27-5EC1-A5B1-B02CC436C4E1},id={271FD7C0-3D11-5E65-8E76-7F0B0DDB4D1C})',null,false)\",\"header\":\"dsHeaderParameter\",\"icon\":\"/temp/iconcache/office/16x16/data.png\",\"disabledIcon\":\"/temp/data_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"dsTooltipParameter\",\"type\":\"datasourcesmenu\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Test the component.\",\"type\":\"sticky\"},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"datasourcesmenu\"},{\"click\":\"chrome:rendering:delete\",\"header\":\"Delete\",\"icon\":\"/temp/iconcache/office/16x16/delete.png\",\"disabledIcon\":\"/temp/delete_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Remove component.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{271FD7C0-3D11-5E65-8E76-7F0B0DDB4D1C}?lang=en&ver=1\",\"custom\":{\"renderingID\":\"DADE61AE8C275EC1A5B1B02CC436C4E1\",\"editable\":\"true\"},\"displayName\":\"Styleguide-Section\",\"expandedDisplayName\":null}",
                  "attributes": {
                    "type": "text/sitecore",
                    "chrometype": "rendering",
                    "kind": "open",
                    "hintname": "Styleguide-Section",
                    "id": "r_7DE41A1A24E4596382063BB0B7D9DD69",
                    "class": "scpm",
                    "data-selectable": "true"
                  }
                },
                {
                  "uid": "7de41a1a-24e4-5963-8206-3bb0b7d9dd69",
                  "componentName": "Styleguide-Section",
                  "dataSource": "{271FD7C0-3D11-5E65-8E76-7F0B0DDB4D1C}",
                  "params": {},
                  "fields": {
                    "heading": {
                      "value": "Layout Patterns",
                      "editable": "<input id='fld_271FD7C03D115E658E767F0B0DDB4D1C_51D3C9FDEB395D6096E6EF8FB686BC75_en_1_c272d7da38f24a8c892dfc85acce16a0_514' class='scFieldValue' name='fld_271FD7C03D115E658E767F0B0DDB4D1C_51D3C9FDEB395D6096E6EF8FB686BC75_en_1_c272d7da38f24a8c892dfc85acce16a0_514' type='hidden' value=\"Layout Patterns\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{271FD7C0-3D11-5E65-8E76-7F0B0DDB4D1C}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"heading\",\"expandedDisplayName\":null}</span><span id=\"fld_271FD7C03D115E658E767F0B0DDB4D1C_51D3C9FDEB395D6096E6EF8FB686BC75_en_1_c272d7da38f24a8c892dfc85acce16a0_514_edit\" sc_parameters=\"prevent-line-break=true\" contenteditable=\"true\" class=\"scWebEditInput\" scFieldType=\"single-line text\" scDefaultText=\"[No text in field]\">Layout Patterns</span>"
                    }
                  },
                  "placeholders": {
                    "jss-styleguide-section": [
                      {
                        "name": "code",
                        "type": "text/sitecore",
                        "contents": "{\"commands\":[{\"click\":\"chrome:placeholder:addControl\",\"header\":\"Add to here\",\"icon\":\"/temp/iconcache/office/16x16/add.png\",\"disabledIcon\":\"/temp/add_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Add a new rendering to the '{0}' placeholder.\",\"type\":\"\"},{\"click\":\"chrome:placeholder:editSettings\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/window_gear.png\",\"disabledIcon\":\"/temp/window_gear_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the placeholder settings.\",\"type\":\"\"}],\"contextItemUri\":\"sitecore://master/{302C626E-8ADA-516A-AA71-B8D5D4E96B3C}?lang=en&ver=1\",\"custom\":{\"allowedRenderings\":[\"0B82E18FF08856D3BC52C4D054C4B218\",\"DAB81669A0825E998D52717D4EF215F8\",\"862F358DBB635D50ABDBE01FBD771B7F\",\"7CC402CBA3945DAAB45F32D5FCDFFF6B\",\"7C0EF2D278095B2E84221C677744445F\",\"3127DF7E6ED652DFBC2DFA4CDC65C9A6\",\"200369E789C4531BB076C53AB1C50154\",\"2D83A15B7EEF52C1BBA2EAB2D742D952\",\"0B711DC99DD2568AAECBD75DBA2F8DCD\",\"5E747641F9A457EEB62573D976639071\",\"5EB0D6136DAF5CAA8F565175867EC5BC\",\"DAC9212F32495BD883D9DC2D0F5F16F0\",\"FEACAB92BAD0551D914D19DFD91B1565\",\"6879CB46822C5F9E987B1041537629D1\",\"7F7E9A4D8FA952C18F87860195CB2C31\",\"690D327E36385C259E7F959A4D64E339\",\"2ABC73F365E85301A06165CF10397070\",\"4D4F0795D9B45A03BB91138B1BAE8A5F\"],\"editable\":\"true\"},\"displayName\":\"jss-styleguide-section\",\"expandedDisplayName\":null}",
                        "attributes": {
                          "type": "text/sitecore",
                          "chrometype": "placeholder",
                          "kind": "open",
                          "id": "_jss_main_jss_styleguide_layout__34A6553C_81DE_5CD3_989E_853F6CB6DF8C__0_jss_styleguide_section__7DE41A1A_24E4_5963_8206_3BB0B7D9DD69__0",
                          "key": "/jss-main/jss-styleguide-layout-{34A6553C-81DE-5CD3-989E-853F6CB6DF8C}-0/jss-styleguide-section-{7DE41A1A-24E4-5963-8206-3BB0B7D9DD69}-0",
                          "class": "scpm",
                          "data-selectable": "true"
                        }
                      },
                      {
                        "name": "code",
                        "type": "text/sitecore",
                        "contents": "{\"commands\":[{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:fieldeditor(command={2F7D34E2-EB93-47ED-8177-B1F429A9FCD7},fields=heading|description,id={301F77BF-6DC1-5F42-AEFF-DAB07C9C0F71})',null,false)\",\"header\":\"Edit Fields\",\"icon\":\"/temp/iconcache/office/16x16/pencil.png\",\"disabledIcon\":\"/temp/pencil_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"\",\"type\":null},{\"click\":\"chrome:dummy\",\"header\":\"Separator\",\"icon\":\"\",\"disabledIcon\":\"\",\"isDivider\":false,\"tooltip\":null,\"type\":\"separator\"},{\"click\":\"chrome:rendering:sort\",\"header\":\"Change position\",\"icon\":\"/temp/iconcache/office/16x16/document_size.png\",\"disabledIcon\":\"/temp/document_size_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Move component.\",\"type\":\"\"},{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:componentoptions(referenceId={3A5D9C50-D8C1-5A12-8DA8-5D56C2A5A69A},renderingId={DAC9212F-3249-5BD8-83D9-DC2D0F5F16F0},id={301F77BF-6DC1-5F42-AEFF-DAB07C9C0F71})',null,false)\",\"header\":\"Edit Experience Editor Options\",\"icon\":\"/temp/iconcache/office/16x16/clipboard_check_edit.png\",\"disabledIcon\":\"/temp/clipboard_check_edit_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the Experience Editor options for the component.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:properties\",\"header\":\"Edit component properties\",\"icon\":\"/temp/iconcache/office/16x16/elements_branch.png\",\"disabledIcon\":\"/temp/elements_branch_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the properties for the component.\",\"type\":\"common\"},{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:setdatasource(referenceId={3A5D9C50-D8C1-5A12-8DA8-5D56C2A5A69A},renderingId={DAC9212F-3249-5BD8-83D9-DC2D0F5F16F0},id={301F77BF-6DC1-5F42-AEFF-DAB07C9C0F71})',null,false)\",\"header\":\"dsHeaderParameter\",\"icon\":\"/temp/iconcache/office/16x16/data.png\",\"disabledIcon\":\"/temp/data_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"dsTooltipParameter\",\"type\":\"datasourcesmenu\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Test the component.\",\"type\":\"sticky\"},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"datasourcesmenu\"},{\"click\":\"chrome:rendering:delete\",\"header\":\"Delete\",\"icon\":\"/temp/iconcache/office/16x16/delete.png\",\"disabledIcon\":\"/temp/delete_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Remove component.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{301F77BF-6DC1-5F42-AEFF-DAB07C9C0F71}?lang=en&ver=1\",\"custom\":{\"renderingID\":\"DAC9212F32495BD883D9DC2D0F5F16F0\",\"editable\":\"true\"},\"displayName\":\"Styleguide-Layout-Reuse\",\"expandedDisplayName\":null}",
                        "attributes": {
                          "type": "text/sitecore",
                          "chrometype": "rendering",
                          "kind": "open",
                          "hintname": "Styleguide-Layout-Reuse",
                          "id": "r_3A5D9C50D8C15A128DA85D56C2A5A69A",
                          "class": "scpm",
                          "data-selectable": "true"
                        }
                      },
                      {
                        "uid": "3a5d9c50-d8c1-5a12-8da8-5d56c2a5a69a",
                        "componentName": "Styleguide-Layout-Reuse",
                        "dataSource": "{301F77BF-6DC1-5F42-AEFF-DAB07C9C0F71}",
                        "params": {},
                        "fields": {
                          "heading": {
                            "value": "Reusing Content",
                            "editable": "<input id='fld_301F77BF6DC15F42AEFFDAB07C9C0F71_2EF2548084095250A76CC3D867340008_en_1_cb2b1a0009b844d982ef2233ce4235cb_504' class='scFieldValue' name='fld_301F77BF6DC15F42AEFFDAB07C9C0F71_2EF2548084095250A76CC3D867340008_en_1_cb2b1a0009b844d982ef2233ce4235cb_504' type='hidden' value=\"Reusing Content\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{301F77BF-6DC1-5F42-AEFF-DAB07C9C0F71}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"heading\",\"expandedDisplayName\":null}</span><span id=\"fld_301F77BF6DC15F42AEFFDAB07C9C0F71_2EF2548084095250A76CC3D867340008_en_1_cb2b1a0009b844d982ef2233ce4235cb_504_edit\" sc_parameters=\"prevent-line-break=true\" contenteditable=\"true\" class=\"scWebEditInput\" scFieldType=\"single-line text\" scDefaultText=\"[No text in field]\">Reusing Content</span>"
                          },
                          "description": {
                            "value": "<p>JSS provides powerful options to reuse content, whether it's sharing a common piece of text across pages or sketching out a site with repeating <em>lorem ipsum</em> content.</p>",
                            "editable": "<input id='fld_301F77BF6DC15F42AEFFDAB07C9C0F71_439F9C97BCD65D8FBE16CFECCC159EFB_en_1_cb2b1a0009b844d982ef2233ce4235cb_505' class='scFieldValue' name='fld_301F77BF6DC15F42AEFFDAB07C9C0F71_439F9C97BCD65D8FBE16CFECCC159EFB_en_1_cb2b1a0009b844d982ef2233ce4235cb_505' type='hidden' value=\"&lt;p&gt;JSS provides powerful options to reuse content, whether it&#39;s sharing a common piece of text across pages or sketching out a site with repeating &lt;em&gt;lorem ipsum&lt;/em&gt; content.&lt;/p&gt;\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:field:editcontrol({command:\\\"webedit:edithtml\\\"})\",\"header\":\"Edit Text\",\"icon\":\"/temp/iconcache/office/16x16/pencil.png\",\"disabledIcon\":\"/temp/pencil_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the text\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"bold\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_bold.png\",\"disabledIcon\":\"/temp/font_style_bold_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Bold\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"Italic\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_italics.png\",\"disabledIcon\":\"/temp/font_style_italics_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Italic\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"Underline\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_underline.png\",\"disabledIcon\":\"/temp/font_style_underline_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Underline\",\"type\":null},{\"click\":\"chrome:field:insertexternallink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/earth_link.png\",\"disabledIcon\":\"/temp/earth_link_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert an external link into the text field.\",\"type\":null},{\"click\":\"chrome:field:insertlink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/link.png\",\"disabledIcon\":\"/temp/link_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert a link into the text field.\",\"type\":null},{\"click\":\"chrome:field:removelink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/link_broken.png\",\"disabledIcon\":\"/temp/link_broken_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Remove link.\",\"type\":null},{\"click\":\"chrome:field:insertimage\",\"header\":\"Insert image\",\"icon\":\"/temp/iconcache/office/16x16/photo_landscape.png\",\"disabledIcon\":\"/temp/photo_landscape_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert an image into the text field.\",\"type\":null},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{301F77BF-6DC1-5F42-AEFF-DAB07C9C0F71}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"description\",\"expandedDisplayName\":null}</span><span scFieldType=\"rich text\" scDefaultText=\"[No text in field]\" contenteditable=\"true\" class=\"scWebEditInput\" id=\"fld_301F77BF6DC15F42AEFFDAB07C9C0F71_439F9C97BCD65D8FBE16CFECCC159EFB_en_1_cb2b1a0009b844d982ef2233ce4235cb_505_edit\"><p>JSS provides powerful options to reuse content, whether it's sharing a common piece of text across pages or sketching out a site with repeating <em>lorem ipsum</em> content.</p></span>"
                          }
                        },
                        "placeholders": {
                          "jss-reuse-example": [
                            {
                              "name": "code",
                              "type": "text/sitecore",
                              "contents": "{\"commands\":[{\"click\":\"chrome:placeholder:addControl\",\"header\":\"Add to here\",\"icon\":\"/temp/iconcache/office/16x16/add.png\",\"disabledIcon\":\"/temp/add_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Add a new rendering to the '{0}' placeholder.\",\"type\":\"\"},{\"click\":\"chrome:placeholder:editSettings\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/window_gear.png\",\"disabledIcon\":\"/temp/window_gear_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the placeholder settings.\",\"type\":\"\"}],\"contextItemUri\":\"sitecore://master/{302C626E-8ADA-516A-AA71-B8D5D4E96B3C}?lang=en&ver=1\",\"custom\":{\"allowedRenderings\":[\"49C9C9892F425AE299F5B7DD1F2A7D20\"],\"editable\":\"true\"},\"displayName\":\"jss-reuse-example\",\"expandedDisplayName\":null}",
                              "attributes": {
                                "type": "text/sitecore",
                                "chrometype": "placeholder",
                                "kind": "open",
                                "id": "_jss_main_jss_styleguide_layout__34A6553C_81DE_5CD3_989E_853F6CB6DF8C__0_jss_styleguide_section__7DE41A1A_24E4_5963_8206_3BB0B7D9DD69__0_jss_reuse_example__3A5D9C50_D8C1_5A12_8DA8_5D56C2A5A69A__0",
                                "key": "/jss-main/jss-styleguide-layout-{34A6553C-81DE-5CD3-989E-853F6CB6DF8C}-0/jss-styleguide-section-{7DE41A1A-24E4-5963-8206-3BB0B7D9DD69}-0/jss-reuse-example-{3A5D9C50-D8C1-5A12-8DA8-5D56C2A5A69A}-0",
                                "class": "scpm",
                                "data-selectable": "true"
                              }
                            },
                            {
                              "name": "code",
                              "type": "text/sitecore",
                              "contents": "{\"commands\":[{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:fieldeditor(command={2F7D34E2-EB93-47ED-8177-B1F429A9FCD7},fields=heading|content,id={6584ED98-B183-555D-A8FC-AC0BA3337797})',null,false)\",\"header\":\"Edit Fields\",\"icon\":\"/temp/iconcache/office/16x16/pencil.png\",\"disabledIcon\":\"/temp/pencil_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"\",\"type\":null},{\"click\":\"chrome:dummy\",\"header\":\"Separator\",\"icon\":\"\",\"disabledIcon\":\"\",\"isDivider\":false,\"tooltip\":null,\"type\":\"separator\"},{\"click\":\"chrome:rendering:sort\",\"header\":\"Change position\",\"icon\":\"/temp/iconcache/office/16x16/document_size.png\",\"disabledIcon\":\"/temp/document_size_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Move component.\",\"type\":\"\"},{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:componentoptions(referenceId={AA328B8A-D6E1-5B37-8143-250D2E93D6B8},renderingId={49C9C989-2F42-5AE2-99F5-B7DD1F2A7D20},id={6584ED98-B183-555D-A8FC-AC0BA3337797})',null,false)\",\"header\":\"Edit Experience Editor Options\",\"icon\":\"/temp/iconcache/office/16x16/clipboard_check_edit.png\",\"disabledIcon\":\"/temp/clipboard_check_edit_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the Experience Editor options for the component.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:properties\",\"header\":\"Edit component properties\",\"icon\":\"/temp/iconcache/office/16x16/elements_branch.png\",\"disabledIcon\":\"/temp/elements_branch_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the properties for the component.\",\"type\":\"common\"},{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:setdatasource(referenceId={AA328B8A-D6E1-5B37-8143-250D2E93D6B8},renderingId={49C9C989-2F42-5AE2-99F5-B7DD1F2A7D20},id={6584ED98-B183-555D-A8FC-AC0BA3337797})',null,false)\",\"header\":\"dsHeaderParameter\",\"icon\":\"/temp/iconcache/office/16x16/data.png\",\"disabledIcon\":\"/temp/data_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"dsTooltipParameter\",\"type\":\"datasourcesmenu\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Test the component.\",\"type\":\"sticky\"},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"datasourcesmenu\"},{\"click\":\"chrome:rendering:delete\",\"header\":\"Delete\",\"icon\":\"/temp/iconcache/office/16x16/delete.png\",\"disabledIcon\":\"/temp/delete_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Remove component.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{6584ED98-B183-555D-A8FC-AC0BA3337797}?lang=en&ver=1\",\"custom\":{\"renderingID\":\"49C9C9892F425AE299F5B7DD1F2A7D20\",\"editable\":\"true\"},\"displayName\":\"Content Block\",\"expandedDisplayName\":null}",
                              "attributes": {
                                "type": "text/sitecore",
                                "chrometype": "rendering",
                                "kind": "open",
                                "hintname": "Content Block",
                                "id": "r_AA328B8AD6E15B378143250D2E93D6B8",
                                "class": "scpm",
                                "data-selectable": "true"
                              }
                            },
                            {
                              "uid": "aa328b8a-d6e1-5b37-8143-250d2e93d6b8",
                              "componentName": "ContentBlock",
                              "dataSource": "{6584ED98-B183-555D-A8FC-AC0BA3337797}",
                              "params": {},
                              "fields": {
                                "heading": {
                                  "value": "",
                                  "editable": "<input id='fld_6584ED98B183555DA8FCAC0BA3337797_3D6144C71C905864AB9732B1F0537ED7_en_1_5fc5e2bc98494ba38af881a11fd11b2a_496' class='scFieldValue' name='fld_6584ED98B183555DA8FCAC0BA3337797_3D6144C71C905864AB9732B1F0537ED7_en_1_5fc5e2bc98494ba38af881a11fd11b2a_496' type='hidden' value=\"\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{6584ED98-B183-555D-A8FC-AC0BA3337797}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"heading\",\"expandedDisplayName\":null}</span><span id=\"fld_6584ED98B183555DA8FCAC0BA3337797_3D6144C71C905864AB9732B1F0537ED7_en_1_5fc5e2bc98494ba38af881a11fd11b2a_496_edit\" sc_parameters=\"prevent-line-break=true\" contenteditable=\"true\" class=\"scWebEditInput\" scFieldType=\"single-line text\" scDefaultText=\"[No text in field]\" scWatermark=\"true\">[No text in field]</span>"
                                },
                                "content": {
                                  "value": "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque felis mauris, pretium id neque vitae, vulputate pellentesque tortor. Mauris hendrerit dolor et ipsum lobortis bibendum non finibus neque. Morbi volutpat aliquam magna id posuere. Duis commodo cursus dui, nec interdum velit congue nec. Aliquam erat volutpat. Aliquam facilisis, sapien quis fringilla tincidunt, magna nulla feugiat neque, a consectetur arcu orci eu augue.</p>",
                                  "editable": "<input id='fld_6584ED98B183555DA8FCAC0BA3337797_764D22AA6E7950EB9D58FF0568203F48_en_1_5fc5e2bc98494ba38af881a11fd11b2a_497' class='scFieldValue' name='fld_6584ED98B183555DA8FCAC0BA3337797_764D22AA6E7950EB9D58FF0568203F48_en_1_5fc5e2bc98494ba38af881a11fd11b2a_497' type='hidden' value=\"&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque felis mauris, pretium id neque vitae, vulputate pellentesque tortor. Mauris hendrerit dolor et ipsum lobortis bibendum non finibus neque. Morbi volutpat aliquam magna id posuere. Duis commodo cursus dui, nec interdum velit congue nec. Aliquam erat volutpat. Aliquam facilisis, sapien quis fringilla tincidunt, magna nulla feugiat neque, a consectetur arcu orci eu augue.&lt;/p&gt;\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:field:editcontrol({command:\\\"webedit:edithtml\\\"})\",\"header\":\"Edit Text\",\"icon\":\"/temp/iconcache/office/16x16/pencil.png\",\"disabledIcon\":\"/temp/pencil_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the text\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"bold\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_bold.png\",\"disabledIcon\":\"/temp/font_style_bold_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Bold\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"Italic\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_italics.png\",\"disabledIcon\":\"/temp/font_style_italics_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Italic\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"Underline\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_underline.png\",\"disabledIcon\":\"/temp/font_style_underline_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Underline\",\"type\":null},{\"click\":\"chrome:field:insertexternallink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/earth_link.png\",\"disabledIcon\":\"/temp/earth_link_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert an external link into the text field.\",\"type\":null},{\"click\":\"chrome:field:insertlink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/link.png\",\"disabledIcon\":\"/temp/link_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert a link into the text field.\",\"type\":null},{\"click\":\"chrome:field:removelink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/link_broken.png\",\"disabledIcon\":\"/temp/link_broken_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Remove link.\",\"type\":null},{\"click\":\"chrome:field:insertimage\",\"header\":\"Insert image\",\"icon\":\"/temp/iconcache/office/16x16/photo_landscape.png\",\"disabledIcon\":\"/temp/photo_landscape_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert an image into the text field.\",\"type\":null},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{6584ED98-B183-555D-A8FC-AC0BA3337797}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"content\",\"expandedDisplayName\":null}</span><span scFieldType=\"rich text\" scDefaultText=\"[No text in field]\" contenteditable=\"true\" class=\"scWebEditInput\" id=\"fld_6584ED98B183555DA8FCAC0BA3337797_764D22AA6E7950EB9D58FF0568203F48_en_1_5fc5e2bc98494ba38af881a11fd11b2a_497_edit\"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque felis mauris, pretium id neque vitae, vulputate pellentesque tortor. Mauris hendrerit dolor et ipsum lobortis bibendum non finibus neque. Morbi volutpat aliquam magna id posuere. Duis commodo cursus dui, nec interdum velit congue nec. Aliquam erat volutpat. Aliquam facilisis, sapien quis fringilla tincidunt, magna nulla feugiat neque, a consectetur arcu orci eu augue.</p></span>"
                                }
                              }
                            },
                            {
                              "name": "code",
                              "type": "text/sitecore",
                              "contents": "",
                              "attributes": {
                                "type": "text/sitecore",
                                "id": "scEnclosingTag_r_",
                                "chrometype": "rendering",
                                "kind": "close",
                                "hintkey": "Content Block",
                                "class": "scpm"
                              }
                            },
                            {
                              "name": "code",
                              "type": "text/sitecore",
                              "contents": "{\"commands\":[{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:fieldeditor(command={2F7D34E2-EB93-47ED-8177-B1F429A9FCD7},fields=heading|content,id={6584ED98-B183-555D-A8FC-AC0BA3337797})',null,false)\",\"header\":\"Edit Fields\",\"icon\":\"/temp/iconcache/office/16x16/pencil.png\",\"disabledIcon\":\"/temp/pencil_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"\",\"type\":null},{\"click\":\"chrome:dummy\",\"header\":\"Separator\",\"icon\":\"\",\"disabledIcon\":\"\",\"isDivider\":false,\"tooltip\":null,\"type\":\"separator\"},{\"click\":\"chrome:rendering:sort\",\"header\":\"Change position\",\"icon\":\"/temp/iconcache/office/16x16/document_size.png\",\"disabledIcon\":\"/temp/document_size_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Move component.\",\"type\":\"\"},{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:componentoptions(referenceId={C4330D34-623C-556C-BF4C-97C93D40FB1E},renderingId={49C9C989-2F42-5AE2-99F5-B7DD1F2A7D20},id={6584ED98-B183-555D-A8FC-AC0BA3337797})',null,false)\",\"header\":\"Edit Experience Editor Options\",\"icon\":\"/temp/iconcache/office/16x16/clipboard_check_edit.png\",\"disabledIcon\":\"/temp/clipboard_check_edit_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the Experience Editor options for the component.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:properties\",\"header\":\"Edit component properties\",\"icon\":\"/temp/iconcache/office/16x16/elements_branch.png\",\"disabledIcon\":\"/temp/elements_branch_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the properties for the component.\",\"type\":\"common\"},{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:setdatasource(referenceId={C4330D34-623C-556C-BF4C-97C93D40FB1E},renderingId={49C9C989-2F42-5AE2-99F5-B7DD1F2A7D20},id={6584ED98-B183-555D-A8FC-AC0BA3337797})',null,false)\",\"header\":\"dsHeaderParameter\",\"icon\":\"/temp/iconcache/office/16x16/data.png\",\"disabledIcon\":\"/temp/data_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"dsTooltipParameter\",\"type\":\"datasourcesmenu\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Test the component.\",\"type\":\"sticky\"},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"datasourcesmenu\"},{\"click\":\"chrome:rendering:delete\",\"header\":\"Delete\",\"icon\":\"/temp/iconcache/office/16x16/delete.png\",\"disabledIcon\":\"/temp/delete_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Remove component.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{6584ED98-B183-555D-A8FC-AC0BA3337797}?lang=en&ver=1\",\"custom\":{\"renderingID\":\"49C9C9892F425AE299F5B7DD1F2A7D20\",\"editable\":\"true\"},\"displayName\":\"Content Block\",\"expandedDisplayName\":null}",
                              "attributes": {
                                "type": "text/sitecore",
                                "chrometype": "rendering",
                                "kind": "open",
                                "hintname": "Content Block",
                                "id": "r_C4330D34623C556CBF4C97C93D40FB1E",
                                "class": "scpm",
                                "data-selectable": "true"
                              }
                            },
                            {
                              "uid": "c4330d34-623c-556c-bf4c-97c93d40fb1e",
                              "componentName": "ContentBlock",
                              "dataSource": "{6584ED98-B183-555D-A8FC-AC0BA3337797}",
                              "params": {},
                              "fields": {
                                "heading": {
                                  "value": "",
                                  "editable": "<input id='fld_6584ED98B183555DA8FCAC0BA3337797_3D6144C71C905864AB9732B1F0537ED7_en_1_5fc5e2bc98494ba38af881a11fd11b2a_498' class='scFieldValue' name='fld_6584ED98B183555DA8FCAC0BA3337797_3D6144C71C905864AB9732B1F0537ED7_en_1_5fc5e2bc98494ba38af881a11fd11b2a_498' type='hidden' value=\"\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{6584ED98-B183-555D-A8FC-AC0BA3337797}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"heading\",\"expandedDisplayName\":null}</span><span id=\"fld_6584ED98B183555DA8FCAC0BA3337797_3D6144C71C905864AB9732B1F0537ED7_en_1_5fc5e2bc98494ba38af881a11fd11b2a_498_edit\" sc_parameters=\"prevent-line-break=true\" contenteditable=\"true\" class=\"scWebEditInput\" scFieldType=\"single-line text\" scDefaultText=\"[No text in field]\" scWatermark=\"true\">[No text in field]</span>"
                                },
                                "content": {
                                  "value": "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque felis mauris, pretium id neque vitae, vulputate pellentesque tortor. Mauris hendrerit dolor et ipsum lobortis bibendum non finibus neque. Morbi volutpat aliquam magna id posuere. Duis commodo cursus dui, nec interdum velit congue nec. Aliquam erat volutpat. Aliquam facilisis, sapien quis fringilla tincidunt, magna nulla feugiat neque, a consectetur arcu orci eu augue.</p>",
                                  "editable": "<input id='fld_6584ED98B183555DA8FCAC0BA3337797_764D22AA6E7950EB9D58FF0568203F48_en_1_5fc5e2bc98494ba38af881a11fd11b2a_499' class='scFieldValue' name='fld_6584ED98B183555DA8FCAC0BA3337797_764D22AA6E7950EB9D58FF0568203F48_en_1_5fc5e2bc98494ba38af881a11fd11b2a_499' type='hidden' value=\"&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque felis mauris, pretium id neque vitae, vulputate pellentesque tortor. Mauris hendrerit dolor et ipsum lobortis bibendum non finibus neque. Morbi volutpat aliquam magna id posuere. Duis commodo cursus dui, nec interdum velit congue nec. Aliquam erat volutpat. Aliquam facilisis, sapien quis fringilla tincidunt, magna nulla feugiat neque, a consectetur arcu orci eu augue.&lt;/p&gt;\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:field:editcontrol({command:\\\"webedit:edithtml\\\"})\",\"header\":\"Edit Text\",\"icon\":\"/temp/iconcache/office/16x16/pencil.png\",\"disabledIcon\":\"/temp/pencil_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the text\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"bold\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_bold.png\",\"disabledIcon\":\"/temp/font_style_bold_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Bold\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"Italic\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_italics.png\",\"disabledIcon\":\"/temp/font_style_italics_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Italic\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"Underline\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_underline.png\",\"disabledIcon\":\"/temp/font_style_underline_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Underline\",\"type\":null},{\"click\":\"chrome:field:insertexternallink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/earth_link.png\",\"disabledIcon\":\"/temp/earth_link_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert an external link into the text field.\",\"type\":null},{\"click\":\"chrome:field:insertlink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/link.png\",\"disabledIcon\":\"/temp/link_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert a link into the text field.\",\"type\":null},{\"click\":\"chrome:field:removelink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/link_broken.png\",\"disabledIcon\":\"/temp/link_broken_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Remove link.\",\"type\":null},{\"click\":\"chrome:field:insertimage\",\"header\":\"Insert image\",\"icon\":\"/temp/iconcache/office/16x16/photo_landscape.png\",\"disabledIcon\":\"/temp/photo_landscape_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert an image into the text field.\",\"type\":null},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{6584ED98-B183-555D-A8FC-AC0BA3337797}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"content\",\"expandedDisplayName\":null}</span><span scFieldType=\"rich text\" scDefaultText=\"[No text in field]\" contenteditable=\"true\" class=\"scWebEditInput\" id=\"fld_6584ED98B183555DA8FCAC0BA3337797_764D22AA6E7950EB9D58FF0568203F48_en_1_5fc5e2bc98494ba38af881a11fd11b2a_499_edit\"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque felis mauris, pretium id neque vitae, vulputate pellentesque tortor. Mauris hendrerit dolor et ipsum lobortis bibendum non finibus neque. Morbi volutpat aliquam magna id posuere. Duis commodo cursus dui, nec interdum velit congue nec. Aliquam erat volutpat. Aliquam facilisis, sapien quis fringilla tincidunt, magna nulla feugiat neque, a consectetur arcu orci eu augue.</p></span>"
                                }
                              }
                            },
                            {
                              "name": "code",
                              "type": "text/sitecore",
                              "contents": "",
                              "attributes": {
                                "type": "text/sitecore",
                                "id": "scEnclosingTag_r_",
                                "chrometype": "rendering",
                                "kind": "close",
                                "hintkey": "Content Block",
                                "class": "scpm"
                              }
                            },
                            {
                              "name": "code",
                              "type": "text/sitecore",
                              "contents": "{\"commands\":[{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:fieldeditor(command={2F7D34E2-EB93-47ED-8177-B1F429A9FCD7},fields=heading|content,id={2BF889B2-F2D9-54AE-9117-F40BA83C8027})',null,false)\",\"header\":\"Edit Fields\",\"icon\":\"/temp/iconcache/office/16x16/pencil.png\",\"disabledIcon\":\"/temp/pencil_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"\",\"type\":null},{\"click\":\"chrome:dummy\",\"header\":\"Separator\",\"icon\":\"\",\"disabledIcon\":\"\",\"isDivider\":false,\"tooltip\":null,\"type\":\"separator\"},{\"click\":\"chrome:rendering:sort\",\"header\":\"Change position\",\"icon\":\"/temp/iconcache/office/16x16/document_size.png\",\"disabledIcon\":\"/temp/document_size_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Move component.\",\"type\":\"\"},{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:componentoptions(referenceId={A42D8B1C-193D-5627-9130-F7F7F87617F1},renderingId={49C9C989-2F42-5AE2-99F5-B7DD1F2A7D20},id={2BF889B2-F2D9-54AE-9117-F40BA83C8027})',null,false)\",\"header\":\"Edit Experience Editor Options\",\"icon\":\"/temp/iconcache/office/16x16/clipboard_check_edit.png\",\"disabledIcon\":\"/temp/clipboard_check_edit_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the Experience Editor options for the component.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:properties\",\"header\":\"Edit component properties\",\"icon\":\"/temp/iconcache/office/16x16/elements_branch.png\",\"disabledIcon\":\"/temp/elements_branch_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the properties for the component.\",\"type\":\"common\"},{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:setdatasource(referenceId={A42D8B1C-193D-5627-9130-F7F7F87617F1},renderingId={49C9C989-2F42-5AE2-99F5-B7DD1F2A7D20},id={2BF889B2-F2D9-54AE-9117-F40BA83C8027})',null,false)\",\"header\":\"dsHeaderParameter\",\"icon\":\"/temp/iconcache/office/16x16/data.png\",\"disabledIcon\":\"/temp/data_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"dsTooltipParameter\",\"type\":\"datasourcesmenu\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Test the component.\",\"type\":\"sticky\"},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"datasourcesmenu\"},{\"click\":\"chrome:rendering:delete\",\"header\":\"Delete\",\"icon\":\"/temp/iconcache/office/16x16/delete.png\",\"disabledIcon\":\"/temp/delete_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Remove component.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{2BF889B2-F2D9-54AE-9117-F40BA83C8027}?lang=en&ver=1\",\"custom\":{\"renderingID\":\"49C9C9892F425AE299F5B7DD1F2A7D20\",\"editable\":\"true\"},\"displayName\":\"Content Block\",\"expandedDisplayName\":null}",
                              "attributes": {
                                "type": "text/sitecore",
                                "chrometype": "rendering",
                                "kind": "open",
                                "hintname": "Content Block",
                                "id": "r_A42D8B1C193D56279130F7F7F87617F1",
                                "class": "scpm",
                                "data-selectable": "true"
                              }
                            },
                            {
                              "uid": "a42d8b1c-193d-5627-9130-f7f7f87617f1",
                              "componentName": "ContentBlock",
                              "dataSource": "{2BF889B2-F2D9-54AE-9117-F40BA83C8027}",
                              "params": {},
                              "fields": {
                                "heading": {
                                  "value": "",
                                  "editable": "<input id='fld_2BF889B2F2D954AE9117F40BA83C8027_3D6144C71C905864AB9732B1F0537ED7_en_1_07d1062f5a8b40dc849fbe64b7bad771_500' class='scFieldValue' name='fld_2BF889B2F2D954AE9117F40BA83C8027_3D6144C71C905864AB9732B1F0537ED7_en_1_07d1062f5a8b40dc849fbe64b7bad771_500' type='hidden' value=\"\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{2BF889B2-F2D9-54AE-9117-F40BA83C8027}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"heading\",\"expandedDisplayName\":null}</span><span id=\"fld_2BF889B2F2D954AE9117F40BA83C8027_3D6144C71C905864AB9732B1F0537ED7_en_1_07d1062f5a8b40dc849fbe64b7bad771_500_edit\" sc_parameters=\"prevent-line-break=true\" contenteditable=\"true\" class=\"scWebEditInput\" scFieldType=\"single-line text\" scDefaultText=\"[No text in field]\" scWatermark=\"true\">[No text in field]</span>"
                                },
                                "content": {
                                  "value": "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque felis mauris, pretium id neque vitae, vulputate pellentesque tortor. Mauris hendrerit dolor et ipsum lobortis bibendum non finibus neque. Morbi volutpat aliquam magna id posuere. Duis commodo cursus dui, nec interdum velit congue nec. Aliquam erat volutpat. Aliquam facilisis, sapien quis fringilla tincidunt, magna nulla feugiat neque, a consectetur arcu orci eu augue.</p>",
                                  "editable": "<input id='fld_2BF889B2F2D954AE9117F40BA83C8027_764D22AA6E7950EB9D58FF0568203F48_en_1_07d1062f5a8b40dc849fbe64b7bad771_501' class='scFieldValue' name='fld_2BF889B2F2D954AE9117F40BA83C8027_764D22AA6E7950EB9D58FF0568203F48_en_1_07d1062f5a8b40dc849fbe64b7bad771_501' type='hidden' value=\"&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque felis mauris, pretium id neque vitae, vulputate pellentesque tortor. Mauris hendrerit dolor et ipsum lobortis bibendum non finibus neque. Morbi volutpat aliquam magna id posuere. Duis commodo cursus dui, nec interdum velit congue nec. Aliquam erat volutpat. Aliquam facilisis, sapien quis fringilla tincidunt, magna nulla feugiat neque, a consectetur arcu orci eu augue.&lt;/p&gt;\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:field:editcontrol({command:\\\"webedit:edithtml\\\"})\",\"header\":\"Edit Text\",\"icon\":\"/temp/iconcache/office/16x16/pencil.png\",\"disabledIcon\":\"/temp/pencil_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the text\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"bold\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_bold.png\",\"disabledIcon\":\"/temp/font_style_bold_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Bold\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"Italic\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_italics.png\",\"disabledIcon\":\"/temp/font_style_italics_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Italic\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"Underline\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_underline.png\",\"disabledIcon\":\"/temp/font_style_underline_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Underline\",\"type\":null},{\"click\":\"chrome:field:insertexternallink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/earth_link.png\",\"disabledIcon\":\"/temp/earth_link_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert an external link into the text field.\",\"type\":null},{\"click\":\"chrome:field:insertlink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/link.png\",\"disabledIcon\":\"/temp/link_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert a link into the text field.\",\"type\":null},{\"click\":\"chrome:field:removelink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/link_broken.png\",\"disabledIcon\":\"/temp/link_broken_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Remove link.\",\"type\":null},{\"click\":\"chrome:field:insertimage\",\"header\":\"Insert image\",\"icon\":\"/temp/iconcache/office/16x16/photo_landscape.png\",\"disabledIcon\":\"/temp/photo_landscape_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert an image into the text field.\",\"type\":null},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{2BF889B2-F2D9-54AE-9117-F40BA83C8027}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"content\",\"expandedDisplayName\":null}</span><span scFieldType=\"rich text\" scDefaultText=\"[No text in field]\" contenteditable=\"true\" class=\"scWebEditInput\" id=\"fld_2BF889B2F2D954AE9117F40BA83C8027_764D22AA6E7950EB9D58FF0568203F48_en_1_07d1062f5a8b40dc849fbe64b7bad771_501_edit\"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque felis mauris, pretium id neque vitae, vulputate pellentesque tortor. Mauris hendrerit dolor et ipsum lobortis bibendum non finibus neque. Morbi volutpat aliquam magna id posuere. Duis commodo cursus dui, nec interdum velit congue nec. Aliquam erat volutpat. Aliquam facilisis, sapien quis fringilla tincidunt, magna nulla feugiat neque, a consectetur arcu orci eu augue.</p></span>"
                                }
                              }
                            },
                            {
                              "name": "code",
                              "type": "text/sitecore",
                              "contents": "",
                              "attributes": {
                                "type": "text/sitecore",
                                "id": "scEnclosingTag_r_",
                                "chrometype": "rendering",
                                "kind": "close",
                                "hintkey": "Content Block",
                                "class": "scpm"
                              }
                            },
                            {
                              "name": "code",
                              "type": "text/sitecore",
                              "contents": "{\"commands\":[{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:fieldeditor(command={2F7D34E2-EB93-47ED-8177-B1F429A9FCD7},fields=heading|content,id={635B5D48-5D0E-553C-94B7-4921A319D960})',null,false)\",\"header\":\"Edit Fields\",\"icon\":\"/temp/iconcache/office/16x16/pencil.png\",\"disabledIcon\":\"/temp/pencil_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"\",\"type\":null},{\"click\":\"chrome:dummy\",\"header\":\"Separator\",\"icon\":\"\",\"disabledIcon\":\"\",\"isDivider\":false,\"tooltip\":null,\"type\":\"separator\"},{\"click\":\"chrome:rendering:sort\",\"header\":\"Change position\",\"icon\":\"/temp/iconcache/office/16x16/document_size.png\",\"disabledIcon\":\"/temp/document_size_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Move component.\",\"type\":\"\"},{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:componentoptions(referenceId={0F4CB47A-979E-5139-B50B-A8E40C73C236},renderingId={49C9C989-2F42-5AE2-99F5-B7DD1F2A7D20},id={635B5D48-5D0E-553C-94B7-4921A319D960})',null,false)\",\"header\":\"Edit Experience Editor Options\",\"icon\":\"/temp/iconcache/office/16x16/clipboard_check_edit.png\",\"disabledIcon\":\"/temp/clipboard_check_edit_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the Experience Editor options for the component.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:properties\",\"header\":\"Edit component properties\",\"icon\":\"/temp/iconcache/office/16x16/elements_branch.png\",\"disabledIcon\":\"/temp/elements_branch_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the properties for the component.\",\"type\":\"common\"},{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:setdatasource(referenceId={0F4CB47A-979E-5139-B50B-A8E40C73C236},renderingId={49C9C989-2F42-5AE2-99F5-B7DD1F2A7D20},id={635B5D48-5D0E-553C-94B7-4921A319D960})',null,false)\",\"header\":\"dsHeaderParameter\",\"icon\":\"/temp/iconcache/office/16x16/data.png\",\"disabledIcon\":\"/temp/data_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"dsTooltipParameter\",\"type\":\"datasourcesmenu\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Test the component.\",\"type\":\"sticky\"},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"datasourcesmenu\"},{\"click\":\"chrome:rendering:delete\",\"header\":\"Delete\",\"icon\":\"/temp/iconcache/office/16x16/delete.png\",\"disabledIcon\":\"/temp/delete_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Remove component.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{635B5D48-5D0E-553C-94B7-4921A319D960}?lang=en&ver=1\",\"custom\":{\"renderingID\":\"49C9C9892F425AE299F5B7DD1F2A7D20\",\"editable\":\"true\"},\"displayName\":\"Content Block\",\"expandedDisplayName\":null}",
                              "attributes": {
                                "type": "text/sitecore",
                                "chrometype": "rendering",
                                "kind": "open",
                                "hintname": "Content Block",
                                "id": "r_0F4CB47A979E5139B50BA8E40C73C236",
                                "class": "scpm",
                                "data-selectable": "true"
                              }
                            },
                            {
                              "uid": "0f4cb47a-979e-5139-b50b-a8e40c73c236",
                              "componentName": "ContentBlock",
                              "dataSource": "{635B5D48-5D0E-553C-94B7-4921A319D960}",
                              "params": {},
                              "fields": {
                                "heading": {
                                  "value": "",
                                  "editable": "<input id='fld_635B5D485D0E553C94B74921A319D960_3D6144C71C905864AB9732B1F0537ED7_en_1_910f31142e2845bbb3585866444b6111_502' class='scFieldValue' name='fld_635B5D485D0E553C94B74921A319D960_3D6144C71C905864AB9732B1F0537ED7_en_1_910f31142e2845bbb3585866444b6111_502' type='hidden' value=\"\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{635B5D48-5D0E-553C-94B7-4921A319D960}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"heading\",\"expandedDisplayName\":null}</span><span id=\"fld_635B5D485D0E553C94B74921A319D960_3D6144C71C905864AB9732B1F0537ED7_en_1_910f31142e2845bbb3585866444b6111_502_edit\" sc_parameters=\"prevent-line-break=true\" contenteditable=\"true\" class=\"scWebEditInput\" scFieldType=\"single-line text\" scDefaultText=\"[No text in field]\" scWatermark=\"true\">[No text in field]</span>"
                                },
                                "content": {
                                  "value": "<p>Mix and match reused and local content. Check out <code>/data/routes/styleguide/en.yml</code> to see how.</p>",
                                  "editable": "<input id='fld_635B5D485D0E553C94B74921A319D960_764D22AA6E7950EB9D58FF0568203F48_en_1_910f31142e2845bbb3585866444b6111_503' class='scFieldValue' name='fld_635B5D485D0E553C94B74921A319D960_764D22AA6E7950EB9D58FF0568203F48_en_1_910f31142e2845bbb3585866444b6111_503' type='hidden' value=\"&lt;p&gt;Mix and match reused and local content. Check out &lt;code&gt;/data/routes/styleguide/en.yml&lt;/code&gt; to see how.&lt;/p&gt;\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:field:editcontrol({command:\\\"webedit:edithtml\\\"})\",\"header\":\"Edit Text\",\"icon\":\"/temp/iconcache/office/16x16/pencil.png\",\"disabledIcon\":\"/temp/pencil_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the text\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"bold\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_bold.png\",\"disabledIcon\":\"/temp/font_style_bold_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Bold\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"Italic\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_italics.png\",\"disabledIcon\":\"/temp/font_style_italics_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Italic\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"Underline\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_underline.png\",\"disabledIcon\":\"/temp/font_style_underline_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Underline\",\"type\":null},{\"click\":\"chrome:field:insertexternallink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/earth_link.png\",\"disabledIcon\":\"/temp/earth_link_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert an external link into the text field.\",\"type\":null},{\"click\":\"chrome:field:insertlink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/link.png\",\"disabledIcon\":\"/temp/link_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert a link into the text field.\",\"type\":null},{\"click\":\"chrome:field:removelink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/link_broken.png\",\"disabledIcon\":\"/temp/link_broken_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Remove link.\",\"type\":null},{\"click\":\"chrome:field:insertimage\",\"header\":\"Insert image\",\"icon\":\"/temp/iconcache/office/16x16/photo_landscape.png\",\"disabledIcon\":\"/temp/photo_landscape_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert an image into the text field.\",\"type\":null},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{635B5D48-5D0E-553C-94B7-4921A319D960}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"content\",\"expandedDisplayName\":null}</span><span scFieldType=\"rich text\" scDefaultText=\"[No text in field]\" contenteditable=\"true\" class=\"scWebEditInput\" id=\"fld_635B5D485D0E553C94B74921A319D960_764D22AA6E7950EB9D58FF0568203F48_en_1_910f31142e2845bbb3585866444b6111_503_edit\"><p>Mix and match reused and local content. Check out <code>/data/routes/styleguide/en.yml</code> to see how.</p></span>"
                                }
                              }
                            },
                            {
                              "name": "code",
                              "type": "text/sitecore",
                              "contents": "",
                              "attributes": {
                                "type": "text/sitecore",
                                "id": "scEnclosingTag_r_",
                                "chrometype": "rendering",
                                "kind": "close",
                                "hintkey": "Content Block",
                                "class": "scpm"
                              }
                            },
                            {
                              "name": "code",
                              "type": "text/sitecore",
                              "contents": "",
                              "attributes": {
                                "type": "text/sitecore",
                                "id": "scEnclosingTag_",
                                "chrometype": "placeholder",
                                "kind": "close",
                                "hintname": "jss-reuse-example",
                                "class": "scpm"
                              }
                            }
                          ]
                        }
                      },
                      {
                        "name": "code",
                        "type": "text/sitecore",
                        "contents": "",
                        "attributes": {
                          "type": "text/sitecore",
                          "id": "scEnclosingTag_r_",
                          "chrometype": "rendering",
                          "kind": "close",
                          "hintkey": "Styleguide-Layout-Reuse",
                          "class": "scpm"
                        }
                      },
                      {
                        "name": "code",
                        "type": "text/sitecore",
                        "contents": "{\"commands\":[{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:fieldeditor(command={2F7D34E2-EB93-47ED-8177-B1F429A9FCD7},fields=heading|description,id={AEF33723-6280-5674-AF2A-4B11E780179A})',null,false)\",\"header\":\"Edit Fields\",\"icon\":\"/temp/iconcache/office/16x16/pencil.png\",\"disabledIcon\":\"/temp/pencil_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"\",\"type\":null},{\"click\":\"chrome:dummy\",\"header\":\"Separator\",\"icon\":\"\",\"disabledIcon\":\"\",\"isDivider\":false,\"tooltip\":null,\"type\":\"separator\"},{\"click\":\"chrome:rendering:sort\",\"header\":\"Change position\",\"icon\":\"/temp/iconcache/office/16x16/document_size.png\",\"disabledIcon\":\"/temp/document_size_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Move component.\",\"type\":\"\"},{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:componentoptions(referenceId={538E4831-F157-50BB-AC74-277FCAC9FDDB},renderingId={FEACAB92-BAD0-551D-914D-19DFD91B1565},id={AEF33723-6280-5674-AF2A-4B11E780179A})',null,false)\",\"header\":\"Edit Experience Editor Options\",\"icon\":\"/temp/iconcache/office/16x16/clipboard_check_edit.png\",\"disabledIcon\":\"/temp/clipboard_check_edit_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the Experience Editor options for the component.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:properties\",\"header\":\"Edit component properties\",\"icon\":\"/temp/iconcache/office/16x16/elements_branch.png\",\"disabledIcon\":\"/temp/elements_branch_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the properties for the component.\",\"type\":\"common\"},{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:setdatasource(referenceId={538E4831-F157-50BB-AC74-277FCAC9FDDB},renderingId={FEACAB92-BAD0-551D-914D-19DFD91B1565},id={AEF33723-6280-5674-AF2A-4B11E780179A})',null,false)\",\"header\":\"dsHeaderParameter\",\"icon\":\"/temp/iconcache/office/16x16/data.png\",\"disabledIcon\":\"/temp/data_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"dsTooltipParameter\",\"type\":\"datasourcesmenu\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Test the component.\",\"type\":\"sticky\"},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"datasourcesmenu\"},{\"click\":\"chrome:rendering:delete\",\"header\":\"Delete\",\"icon\":\"/temp/iconcache/office/16x16/delete.png\",\"disabledIcon\":\"/temp/delete_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Remove component.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{AEF33723-6280-5674-AF2A-4B11E780179A}?lang=en&ver=1\",\"custom\":{\"renderingID\":\"FEACAB92BAD0551D914D19DFD91B1565\",\"editable\":\"true\"},\"displayName\":\"Styleguide-Layout-Tabs\",\"expandedDisplayName\":null}",
                        "attributes": {
                          "type": "text/sitecore",
                          "chrometype": "rendering",
                          "kind": "open",
                          "hintname": "Styleguide-Layout-Tabs",
                          "id": "r_538E4831F15750BBAC74277FCAC9FDDB",
                          "class": "scpm",
                          "data-selectable": "true"
                        }
                      },
                      {
                        "uid": "538e4831-f157-50bb-ac74-277fcac9fddb",
                        "componentName": "Styleguide-Layout-Tabs",
                        "dataSource": "{AEF33723-6280-5674-AF2A-4B11E780179A}",
                        "params": {},
                        "fields": {
                          "heading": {
                            "value": "Tabs",
                            "editable": "<input id='fld_AEF3372362805674AF2A4B11E780179A_2EF2548084095250A76CC3D867340008_en_1_9d3444a7becd413784b0b1947769edd7_512' class='scFieldValue' name='fld_AEF3372362805674AF2A4B11E780179A_2EF2548084095250A76CC3D867340008_en_1_9d3444a7becd413784b0b1947769edd7_512' type='hidden' value=\"Tabs\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{AEF33723-6280-5674-AF2A-4B11E780179A}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"heading\",\"expandedDisplayName\":null}</span><span id=\"fld_AEF3372362805674AF2A4B11E780179A_2EF2548084095250A76CC3D867340008_en_1_9d3444a7becd413784b0b1947769edd7_512_edit\" sc_parameters=\"prevent-line-break=true\" contenteditable=\"true\" class=\"scWebEditInput\" scFieldType=\"single-line text\" scDefaultText=\"[No text in field]\">Tabs</span>"
                          },
                          "description": {
                            "value": "<p>Creating hierarchical components like tabs is made simpler in JSS because it's easy to introspect the layout structure.</p>",
                            "editable": "<input id='fld_AEF3372362805674AF2A4B11E780179A_439F9C97BCD65D8FBE16CFECCC159EFB_en_1_9d3444a7becd413784b0b1947769edd7_513' class='scFieldValue' name='fld_AEF3372362805674AF2A4B11E780179A_439F9C97BCD65D8FBE16CFECCC159EFB_en_1_9d3444a7becd413784b0b1947769edd7_513' type='hidden' value=\"&lt;p&gt;Creating hierarchical components like tabs is made simpler in JSS because it&#39;s easy to introspect the layout structure.&lt;/p&gt;\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:field:editcontrol({command:\\\"webedit:edithtml\\\"})\",\"header\":\"Edit Text\",\"icon\":\"/temp/iconcache/office/16x16/pencil.png\",\"disabledIcon\":\"/temp/pencil_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the text\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"bold\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_bold.png\",\"disabledIcon\":\"/temp/font_style_bold_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Bold\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"Italic\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_italics.png\",\"disabledIcon\":\"/temp/font_style_italics_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Italic\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"Underline\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_underline.png\",\"disabledIcon\":\"/temp/font_style_underline_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Underline\",\"type\":null},{\"click\":\"chrome:field:insertexternallink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/earth_link.png\",\"disabledIcon\":\"/temp/earth_link_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert an external link into the text field.\",\"type\":null},{\"click\":\"chrome:field:insertlink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/link.png\",\"disabledIcon\":\"/temp/link_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert a link into the text field.\",\"type\":null},{\"click\":\"chrome:field:removelink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/link_broken.png\",\"disabledIcon\":\"/temp/link_broken_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Remove link.\",\"type\":null},{\"click\":\"chrome:field:insertimage\",\"header\":\"Insert image\",\"icon\":\"/temp/iconcache/office/16x16/photo_landscape.png\",\"disabledIcon\":\"/temp/photo_landscape_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert an image into the text field.\",\"type\":null},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{AEF33723-6280-5674-AF2A-4B11E780179A}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"description\",\"expandedDisplayName\":null}</span><span scFieldType=\"rich text\" scDefaultText=\"[No text in field]\" contenteditable=\"true\" class=\"scWebEditInput\" id=\"fld_AEF3372362805674AF2A4B11E780179A_439F9C97BCD65D8FBE16CFECCC159EFB_en_1_9d3444a7becd413784b0b1947769edd7_513_edit\"><p>Creating hierarchical components like tabs is made simpler in JSS because it's easy to introspect the layout structure.</p></span>"
                          }
                        },
                        "placeholders": {
                          "jss-tabs": [
                            {
                              "name": "code",
                              "type": "text/sitecore",
                              "contents": "{\"commands\":[{\"click\":\"chrome:placeholder:addControl\",\"header\":\"Add to here\",\"icon\":\"/temp/iconcache/office/16x16/add.png\",\"disabledIcon\":\"/temp/add_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Add a new rendering to the '{0}' placeholder.\",\"type\":\"\"},{\"click\":\"chrome:placeholder:editSettings\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/window_gear.png\",\"disabledIcon\":\"/temp/window_gear_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the placeholder settings.\",\"type\":\"\"}],\"contextItemUri\":\"sitecore://master/{302C626E-8ADA-516A-AA71-B8D5D4E96B3C}?lang=en&ver=1\",\"custom\":{\"allowedRenderings\":[\"E441005852735348B88CBCF8B9B94896\"],\"editable\":\"true\"},\"displayName\":\"Tabs\",\"expandedDisplayName\":null}",
                              "attributes": {
                                "type": "text/sitecore",
                                "chrometype": "placeholder",
                                "kind": "open",
                                "id": "_jss_main_jss_styleguide_layout__34A6553C_81DE_5CD3_989E_853F6CB6DF8C__0_jss_styleguide_section__7DE41A1A_24E4_5963_8206_3BB0B7D9DD69__0_jss_tabs__538E4831_F157_50BB_AC74_277FCAC9FDDB__0",
                                "key": "/jss-main/jss-styleguide-layout-{34A6553C-81DE-5CD3-989E-853F6CB6DF8C}-0/jss-styleguide-section-{7DE41A1A-24E4-5963-8206-3BB0B7D9DD69}-0/jss-tabs-{538E4831-F157-50BB-AC74-277FCAC9FDDB}-0",
                                "class": "scpm",
                                "data-selectable": "true"
                              }
                            },
                            {
                              "name": "code",
                              "type": "text/sitecore",
                              "contents": "{\"commands\":[{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:fieldeditor(command={2F7D34E2-EB93-47ED-8177-B1F429A9FCD7},fields=title|content,id={3251B01F-9616-5811-B98E-EA3D76AF8FEA})',null,false)\",\"header\":\"Edit Fields\",\"icon\":\"/temp/iconcache/office/16x16/pencil.png\",\"disabledIcon\":\"/temp/pencil_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"\",\"type\":null},{\"click\":\"chrome:dummy\",\"header\":\"Separator\",\"icon\":\"\",\"disabledIcon\":\"\",\"isDivider\":false,\"tooltip\":null,\"type\":\"separator\"},{\"click\":\"chrome:rendering:sort\",\"header\":\"Change position\",\"icon\":\"/temp/iconcache/office/16x16/document_size.png\",\"disabledIcon\":\"/temp/document_size_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Move component.\",\"type\":\"\"},{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:componentoptions(referenceId={7ECB2ED2-AC9B-58D1-8365-10CA74824AF7},renderingId={E4410058-5273-5348-B88C-BCF8B9B94896},id={3251B01F-9616-5811-B98E-EA3D76AF8FEA})',null,false)\",\"header\":\"Edit Experience Editor Options\",\"icon\":\"/temp/iconcache/office/16x16/clipboard_check_edit.png\",\"disabledIcon\":\"/temp/clipboard_check_edit_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the Experience Editor options for the component.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:properties\",\"header\":\"Edit component properties\",\"icon\":\"/temp/iconcache/office/16x16/elements_branch.png\",\"disabledIcon\":\"/temp/elements_branch_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the properties for the component.\",\"type\":\"common\"},{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:setdatasource(referenceId={7ECB2ED2-AC9B-58D1-8365-10CA74824AF7},renderingId={E4410058-5273-5348-B88C-BCF8B9B94896},id={3251B01F-9616-5811-B98E-EA3D76AF8FEA})',null,false)\",\"header\":\"dsHeaderParameter\",\"icon\":\"/temp/iconcache/office/16x16/data.png\",\"disabledIcon\":\"/temp/data_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"dsTooltipParameter\",\"type\":\"datasourcesmenu\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Test the component.\",\"type\":\"sticky\"},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"datasourcesmenu\"},{\"click\":\"chrome:rendering:delete\",\"header\":\"Delete\",\"icon\":\"/temp/iconcache/office/16x16/delete.png\",\"disabledIcon\":\"/temp/delete_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Remove component.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{3251B01F-9616-5811-B98E-EA3D76AF8FEA}?lang=en&ver=1\",\"custom\":{\"renderingID\":\"E441005852735348B88CBCF8B9B94896\",\"editable\":\"true\"},\"displayName\":\"Styleguide-Layout-Tabs-Tab\",\"expandedDisplayName\":null}",
                              "attributes": {
                                "type": "text/sitecore",
                                "chrometype": "rendering",
                                "kind": "open",
                                "hintname": "Styleguide-Layout-Tabs-Tab",
                                "id": "r_7ECB2ED2AC9B58D1836510CA74824AF7",
                                "class": "scpm",
                                "data-selectable": "true"
                              }
                            },
                            {
                              "uid": "7ecb2ed2-ac9b-58d1-8365-10ca74824af7",
                              "componentName": "Styleguide-Layout-Tabs-Tab",
                              "dataSource": "{3251B01F-9616-5811-B98E-EA3D76AF8FEA}",
                              "params": {},
                              "fields": {
                                "title": {
                                  "value": "Tab 1",
                                  "editable": "<input id='fld_3251B01F96165811B98EEA3D76AF8FEA_961C56CE6E8D562BBFB75B1665D9D521_en_1_291766b5dc1342deb4fcaacc7c4a1d5b_506' class='scFieldValue' name='fld_3251B01F96165811B98EEA3D76AF8FEA_961C56CE6E8D562BBFB75B1665D9D521_en_1_291766b5dc1342deb4fcaacc7c4a1d5b_506' type='hidden' value=\"Tab 1\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{3251B01F-9616-5811-B98E-EA3D76AF8FEA}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"title\",\"expandedDisplayName\":null}</span><span id=\"fld_3251B01F96165811B98EEA3D76AF8FEA_961C56CE6E8D562BBFB75B1665D9D521_en_1_291766b5dc1342deb4fcaacc7c4a1d5b_506_edit\" sc_parameters=\"prevent-line-break=true\" contenteditable=\"true\" class=\"scWebEditInput\" scFieldType=\"single-line text\" scDefaultText=\"[No text in field]\">Tab 1</span>"
                                },
                                "content": {
                                  "value": "<p>Tab 1 contents!</p>",
                                  "editable": "<input id='fld_3251B01F96165811B98EEA3D76AF8FEA_E2F28301CF57544CA2ED72668FF5C569_en_1_291766b5dc1342deb4fcaacc7c4a1d5b_507' class='scFieldValue' name='fld_3251B01F96165811B98EEA3D76AF8FEA_E2F28301CF57544CA2ED72668FF5C569_en_1_291766b5dc1342deb4fcaacc7c4a1d5b_507' type='hidden' value=\"&lt;p&gt;Tab 1 contents!&lt;/p&gt;\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:field:editcontrol({command:\\\"webedit:edithtml\\\"})\",\"header\":\"Edit Text\",\"icon\":\"/temp/iconcache/office/16x16/pencil.png\",\"disabledIcon\":\"/temp/pencil_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the text\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"bold\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_bold.png\",\"disabledIcon\":\"/temp/font_style_bold_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Bold\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"Italic\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_italics.png\",\"disabledIcon\":\"/temp/font_style_italics_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Italic\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"Underline\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_underline.png\",\"disabledIcon\":\"/temp/font_style_underline_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Underline\",\"type\":null},{\"click\":\"chrome:field:insertexternallink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/earth_link.png\",\"disabledIcon\":\"/temp/earth_link_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert an external link into the text field.\",\"type\":null},{\"click\":\"chrome:field:insertlink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/link.png\",\"disabledIcon\":\"/temp/link_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert a link into the text field.\",\"type\":null},{\"click\":\"chrome:field:removelink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/link_broken.png\",\"disabledIcon\":\"/temp/link_broken_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Remove link.\",\"type\":null},{\"click\":\"chrome:field:insertimage\",\"header\":\"Insert image\",\"icon\":\"/temp/iconcache/office/16x16/photo_landscape.png\",\"disabledIcon\":\"/temp/photo_landscape_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert an image into the text field.\",\"type\":null},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{3251B01F-9616-5811-B98E-EA3D76AF8FEA}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"content\",\"expandedDisplayName\":null}</span><span scFieldType=\"rich text\" scDefaultText=\"[No text in field]\" contenteditable=\"true\" class=\"scWebEditInput\" id=\"fld_3251B01F96165811B98EEA3D76AF8FEA_E2F28301CF57544CA2ED72668FF5C569_en_1_291766b5dc1342deb4fcaacc7c4a1d5b_507_edit\"><p>Tab 1 contents!</p></span>"
                                }
                              }
                            },
                            {
                              "name": "code",
                              "type": "text/sitecore",
                              "contents": "",
                              "attributes": {
                                "type": "text/sitecore",
                                "id": "scEnclosingTag_r_",
                                "chrometype": "rendering",
                                "kind": "close",
                                "hintkey": "Styleguide-Layout-Tabs-Tab",
                                "class": "scpm"
                              }
                            },
                            {
                              "name": "code",
                              "type": "text/sitecore",
                              "contents": "{\"commands\":[{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:fieldeditor(command={2F7D34E2-EB93-47ED-8177-B1F429A9FCD7},fields=title|content,id={C9B6E96C-0EE7-5CF5-A075-D1A976F224B9})',null,false)\",\"header\":\"Edit Fields\",\"icon\":\"/temp/iconcache/office/16x16/pencil.png\",\"disabledIcon\":\"/temp/pencil_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"\",\"type\":null},{\"click\":\"chrome:dummy\",\"header\":\"Separator\",\"icon\":\"\",\"disabledIcon\":\"\",\"isDivider\":false,\"tooltip\":null,\"type\":\"separator\"},{\"click\":\"chrome:rendering:sort\",\"header\":\"Change position\",\"icon\":\"/temp/iconcache/office/16x16/document_size.png\",\"disabledIcon\":\"/temp/document_size_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Move component.\",\"type\":\"\"},{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:componentoptions(referenceId={AFD64900-0A61-50EB-A674-A7A884E0D496},renderingId={E4410058-5273-5348-B88C-BCF8B9B94896},id={C9B6E96C-0EE7-5CF5-A075-D1A976F224B9})',null,false)\",\"header\":\"Edit Experience Editor Options\",\"icon\":\"/temp/iconcache/office/16x16/clipboard_check_edit.png\",\"disabledIcon\":\"/temp/clipboard_check_edit_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the Experience Editor options for the component.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:properties\",\"header\":\"Edit component properties\",\"icon\":\"/temp/iconcache/office/16x16/elements_branch.png\",\"disabledIcon\":\"/temp/elements_branch_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the properties for the component.\",\"type\":\"common\"},{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:setdatasource(referenceId={AFD64900-0A61-50EB-A674-A7A884E0D496},renderingId={E4410058-5273-5348-B88C-BCF8B9B94896},id={C9B6E96C-0EE7-5CF5-A075-D1A976F224B9})',null,false)\",\"header\":\"dsHeaderParameter\",\"icon\":\"/temp/iconcache/office/16x16/data.png\",\"disabledIcon\":\"/temp/data_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"dsTooltipParameter\",\"type\":\"datasourcesmenu\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Test the component.\",\"type\":\"sticky\"},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"datasourcesmenu\"},{\"click\":\"chrome:rendering:delete\",\"header\":\"Delete\",\"icon\":\"/temp/iconcache/office/16x16/delete.png\",\"disabledIcon\":\"/temp/delete_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Remove component.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{C9B6E96C-0EE7-5CF5-A075-D1A976F224B9}?lang=en&ver=1\",\"custom\":{\"renderingID\":\"E441005852735348B88CBCF8B9B94896\",\"editable\":\"true\"},\"displayName\":\"Styleguide-Layout-Tabs-Tab\",\"expandedDisplayName\":null}",
                              "attributes": {
                                "type": "text/sitecore",
                                "chrometype": "rendering",
                                "kind": "open",
                                "hintname": "Styleguide-Layout-Tabs-Tab",
                                "id": "r_AFD649000A6150EBA674A7A884E0D496",
                                "class": "scpm",
                                "data-selectable": "true"
                              }
                            },
                            {
                              "uid": "afd64900-0a61-50eb-a674-a7a884e0d496",
                              "componentName": "Styleguide-Layout-Tabs-Tab",
                              "dataSource": "{C9B6E96C-0EE7-5CF5-A075-D1A976F224B9}",
                              "params": {},
                              "fields": {
                                "title": {
                                  "value": "Tab 2",
                                  "editable": "<input id='fld_C9B6E96C0EE75CF5A075D1A976F224B9_961C56CE6E8D562BBFB75B1665D9D521_en_1_d68f7bc6bd1a46ad9f53df8b7123bc50_508' class='scFieldValue' name='fld_C9B6E96C0EE75CF5A075D1A976F224B9_961C56CE6E8D562BBFB75B1665D9D521_en_1_d68f7bc6bd1a46ad9f53df8b7123bc50_508' type='hidden' value=\"Tab 2\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{C9B6E96C-0EE7-5CF5-A075-D1A976F224B9}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"title\",\"expandedDisplayName\":null}</span><span id=\"fld_C9B6E96C0EE75CF5A075D1A976F224B9_961C56CE6E8D562BBFB75B1665D9D521_en_1_d68f7bc6bd1a46ad9f53df8b7123bc50_508_edit\" sc_parameters=\"prevent-line-break=true\" contenteditable=\"true\" class=\"scWebEditInput\" scFieldType=\"single-line text\" scDefaultText=\"[No text in field]\">Tab 2</span>"
                                },
                                "content": {
                                  "value": "<p>Tab 2 contents!</p>",
                                  "editable": "<input id='fld_C9B6E96C0EE75CF5A075D1A976F224B9_E2F28301CF57544CA2ED72668FF5C569_en_1_d68f7bc6bd1a46ad9f53df8b7123bc50_509' class='scFieldValue' name='fld_C9B6E96C0EE75CF5A075D1A976F224B9_E2F28301CF57544CA2ED72668FF5C569_en_1_d68f7bc6bd1a46ad9f53df8b7123bc50_509' type='hidden' value=\"&lt;p&gt;Tab 2 contents!&lt;/p&gt;\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:field:editcontrol({command:\\\"webedit:edithtml\\\"})\",\"header\":\"Edit Text\",\"icon\":\"/temp/iconcache/office/16x16/pencil.png\",\"disabledIcon\":\"/temp/pencil_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the text\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"bold\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_bold.png\",\"disabledIcon\":\"/temp/font_style_bold_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Bold\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"Italic\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_italics.png\",\"disabledIcon\":\"/temp/font_style_italics_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Italic\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"Underline\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_underline.png\",\"disabledIcon\":\"/temp/font_style_underline_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Underline\",\"type\":null},{\"click\":\"chrome:field:insertexternallink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/earth_link.png\",\"disabledIcon\":\"/temp/earth_link_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert an external link into the text field.\",\"type\":null},{\"click\":\"chrome:field:insertlink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/link.png\",\"disabledIcon\":\"/temp/link_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert a link into the text field.\",\"type\":null},{\"click\":\"chrome:field:removelink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/link_broken.png\",\"disabledIcon\":\"/temp/link_broken_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Remove link.\",\"type\":null},{\"click\":\"chrome:field:insertimage\",\"header\":\"Insert image\",\"icon\":\"/temp/iconcache/office/16x16/photo_landscape.png\",\"disabledIcon\":\"/temp/photo_landscape_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert an image into the text field.\",\"type\":null},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{C9B6E96C-0EE7-5CF5-A075-D1A976F224B9}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"content\",\"expandedDisplayName\":null}</span><span scFieldType=\"rich text\" scDefaultText=\"[No text in field]\" contenteditable=\"true\" class=\"scWebEditInput\" id=\"fld_C9B6E96C0EE75CF5A075D1A976F224B9_E2F28301CF57544CA2ED72668FF5C569_en_1_d68f7bc6bd1a46ad9f53df8b7123bc50_509_edit\"><p>Tab 2 contents!</p></span>"
                                }
                              }
                            },
                            {
                              "name": "code",
                              "type": "text/sitecore",
                              "contents": "",
                              "attributes": {
                                "type": "text/sitecore",
                                "id": "scEnclosingTag_r_",
                                "chrometype": "rendering",
                                "kind": "close",
                                "hintkey": "Styleguide-Layout-Tabs-Tab",
                                "class": "scpm"
                              }
                            },
                            {
                              "name": "code",
                              "type": "text/sitecore",
                              "contents": "{\"commands\":[{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:fieldeditor(command={2F7D34E2-EB93-47ED-8177-B1F429A9FCD7},fields=title|content,id={388F96A8-88EF-58AC-9C71-32DD442BBA10})',null,false)\",\"header\":\"Edit Fields\",\"icon\":\"/temp/iconcache/office/16x16/pencil.png\",\"disabledIcon\":\"/temp/pencil_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"\",\"type\":null},{\"click\":\"chrome:dummy\",\"header\":\"Separator\",\"icon\":\"\",\"disabledIcon\":\"\",\"isDivider\":false,\"tooltip\":null,\"type\":\"separator\"},{\"click\":\"chrome:rendering:sort\",\"header\":\"Change position\",\"icon\":\"/temp/iconcache/office/16x16/document_size.png\",\"disabledIcon\":\"/temp/document_size_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Move component.\",\"type\":\"\"},{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:componentoptions(referenceId={44C12983-3A84-5462-84C0-6CA1430050C8},renderingId={E4410058-5273-5348-B88C-BCF8B9B94896},id={388F96A8-88EF-58AC-9C71-32DD442BBA10})',null,false)\",\"header\":\"Edit Experience Editor Options\",\"icon\":\"/temp/iconcache/office/16x16/clipboard_check_edit.png\",\"disabledIcon\":\"/temp/clipboard_check_edit_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the Experience Editor options for the component.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:properties\",\"header\":\"Edit component properties\",\"icon\":\"/temp/iconcache/office/16x16/elements_branch.png\",\"disabledIcon\":\"/temp/elements_branch_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the properties for the component.\",\"type\":\"common\"},{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:setdatasource(referenceId={44C12983-3A84-5462-84C0-6CA1430050C8},renderingId={E4410058-5273-5348-B88C-BCF8B9B94896},id={388F96A8-88EF-58AC-9C71-32DD442BBA10})',null,false)\",\"header\":\"dsHeaderParameter\",\"icon\":\"/temp/iconcache/office/16x16/data.png\",\"disabledIcon\":\"/temp/data_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"dsTooltipParameter\",\"type\":\"datasourcesmenu\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Test the component.\",\"type\":\"sticky\"},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"datasourcesmenu\"},{\"click\":\"chrome:rendering:delete\",\"header\":\"Delete\",\"icon\":\"/temp/iconcache/office/16x16/delete.png\",\"disabledIcon\":\"/temp/delete_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Remove component.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{388F96A8-88EF-58AC-9C71-32DD442BBA10}?lang=en&ver=1\",\"custom\":{\"renderingID\":\"E441005852735348B88CBCF8B9B94896\",\"editable\":\"true\"},\"displayName\":\"Styleguide-Layout-Tabs-Tab\",\"expandedDisplayName\":null}",
                              "attributes": {
                                "type": "text/sitecore",
                                "chrometype": "rendering",
                                "kind": "open",
                                "hintname": "Styleguide-Layout-Tabs-Tab",
                                "id": "r_44C129833A84546284C06CA1430050C8",
                                "class": "scpm",
                                "data-selectable": "true"
                              }
                            },
                            {
                              "uid": "44c12983-3a84-5462-84c0-6ca1430050c8",
                              "componentName": "Styleguide-Layout-Tabs-Tab",
                              "dataSource": "{388F96A8-88EF-58AC-9C71-32DD442BBA10}",
                              "params": {},
                              "fields": {
                                "title": {
                                  "value": "Tab 3",
                                  "editable": "<input id='fld_388F96A888EF58AC9C7132DD442BBA10_961C56CE6E8D562BBFB75B1665D9D521_en_1_084701196201433281beca70395c45e4_510' class='scFieldValue' name='fld_388F96A888EF58AC9C7132DD442BBA10_961C56CE6E8D562BBFB75B1665D9D521_en_1_084701196201433281beca70395c45e4_510' type='hidden' value=\"Tab 3\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{388F96A8-88EF-58AC-9C71-32DD442BBA10}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"title\",\"expandedDisplayName\":null}</span><span id=\"fld_388F96A888EF58AC9C7132DD442BBA10_961C56CE6E8D562BBFB75B1665D9D521_en_1_084701196201433281beca70395c45e4_510_edit\" sc_parameters=\"prevent-line-break=true\" contenteditable=\"true\" class=\"scWebEditInput\" scFieldType=\"single-line text\" scDefaultText=\"[No text in field]\">Tab 3</span>"
                                },
                                "content": {
                                  "value": "<p>Tab 3 contents!</p>",
                                  "editable": "<input id='fld_388F96A888EF58AC9C7132DD442BBA10_E2F28301CF57544CA2ED72668FF5C569_en_1_084701196201433281beca70395c45e4_511' class='scFieldValue' name='fld_388F96A888EF58AC9C7132DD442BBA10_E2F28301CF57544CA2ED72668FF5C569_en_1_084701196201433281beca70395c45e4_511' type='hidden' value=\"&lt;p&gt;Tab 3 contents!&lt;/p&gt;\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:field:editcontrol({command:\\\"webedit:edithtml\\\"})\",\"header\":\"Edit Text\",\"icon\":\"/temp/iconcache/office/16x16/pencil.png\",\"disabledIcon\":\"/temp/pencil_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the text\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"bold\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_bold.png\",\"disabledIcon\":\"/temp/font_style_bold_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Bold\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"Italic\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_italics.png\",\"disabledIcon\":\"/temp/font_style_italics_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Italic\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"Underline\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_underline.png\",\"disabledIcon\":\"/temp/font_style_underline_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Underline\",\"type\":null},{\"click\":\"chrome:field:insertexternallink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/earth_link.png\",\"disabledIcon\":\"/temp/earth_link_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert an external link into the text field.\",\"type\":null},{\"click\":\"chrome:field:insertlink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/link.png\",\"disabledIcon\":\"/temp/link_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert a link into the text field.\",\"type\":null},{\"click\":\"chrome:field:removelink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/link_broken.png\",\"disabledIcon\":\"/temp/link_broken_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Remove link.\",\"type\":null},{\"click\":\"chrome:field:insertimage\",\"header\":\"Insert image\",\"icon\":\"/temp/iconcache/office/16x16/photo_landscape.png\",\"disabledIcon\":\"/temp/photo_landscape_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert an image into the text field.\",\"type\":null},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{388F96A8-88EF-58AC-9C71-32DD442BBA10}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"content\",\"expandedDisplayName\":null}</span><span scFieldType=\"rich text\" scDefaultText=\"[No text in field]\" contenteditable=\"true\" class=\"scWebEditInput\" id=\"fld_388F96A888EF58AC9C7132DD442BBA10_E2F28301CF57544CA2ED72668FF5C569_en_1_084701196201433281beca70395c45e4_511_edit\"><p>Tab 3 contents!</p></span>"
                                }
                              }
                            },
                            {
                              "name": "code",
                              "type": "text/sitecore",
                              "contents": "",
                              "attributes": {
                                "type": "text/sitecore",
                                "id": "scEnclosingTag_r_",
                                "chrometype": "rendering",
                                "kind": "close",
                                "hintkey": "Styleguide-Layout-Tabs-Tab",
                                "class": "scpm"
                              }
                            },
                            {
                              "name": "code",
                              "type": "text/sitecore",
                              "contents": "",
                              "attributes": {
                                "type": "text/sitecore",
                                "id": "scEnclosingTag_",
                                "chrometype": "placeholder",
                                "kind": "close",
                                "hintname": "Tabs",
                                "class": "scpm"
                              }
                            }
                          ]
                        }
                      },
                      {
                        "name": "code",
                        "type": "text/sitecore",
                        "contents": "",
                        "attributes": {
                          "type": "text/sitecore",
                          "id": "scEnclosingTag_r_",
                          "chrometype": "rendering",
                          "kind": "close",
                          "hintkey": "Styleguide-Layout-Tabs",
                          "class": "scpm"
                        }
                      },
                      {
                        "name": "code",
                        "type": "text/sitecore",
                        "contents": "",
                        "attributes": {
                          "type": "text/sitecore",
                          "id": "scEnclosingTag_",
                          "chrometype": "placeholder",
                          "kind": "close",
                          "hintname": "jss-styleguide-section",
                          "class": "scpm"
                        }
                      }
                    ]
                  }
                },
                {
                  "name": "code",
                  "type": "text/sitecore",
                  "contents": "",
                  "attributes": {
                    "type": "text/sitecore",
                    "id": "scEnclosingTag_r_",
                    "chrometype": "rendering",
                    "kind": "close",
                    "hintkey": "Styleguide-Section",
                    "class": "scpm"
                  }
                },
                {
                  "name": "code",
                  "type": "text/sitecore",
                  "contents": "{\"commands\":[{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:fieldeditor(command={2F7D34E2-EB93-47ED-8177-B1F429A9FCD7},fields=heading,id={75E98485-67E9-5186-B644-091A09624205})',null,false)\",\"header\":\"Edit Fields\",\"icon\":\"/temp/iconcache/office/16x16/pencil.png\",\"disabledIcon\":\"/temp/pencil_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"\",\"type\":null},{\"click\":\"chrome:dummy\",\"header\":\"Separator\",\"icon\":\"\",\"disabledIcon\":\"\",\"isDivider\":false,\"tooltip\":null,\"type\":\"separator\"},{\"click\":\"chrome:rendering:sort\",\"header\":\"Change position\",\"icon\":\"/temp/iconcache/office/16x16/document_size.png\",\"disabledIcon\":\"/temp/document_size_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Move component.\",\"type\":\"\"},{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:componentoptions(referenceId={2D806C25-DD46-51E3-93DE-63CF9035122C},renderingId={DADE61AE-8C27-5EC1-A5B1-B02CC436C4E1},id={75E98485-67E9-5186-B644-091A09624205})',null,false)\",\"header\":\"Edit Experience Editor Options\",\"icon\":\"/temp/iconcache/office/16x16/clipboard_check_edit.png\",\"disabledIcon\":\"/temp/clipboard_check_edit_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the Experience Editor options for the component.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:properties\",\"header\":\"Edit component properties\",\"icon\":\"/temp/iconcache/office/16x16/elements_branch.png\",\"disabledIcon\":\"/temp/elements_branch_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the properties for the component.\",\"type\":\"common\"},{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:setdatasource(referenceId={2D806C25-DD46-51E3-93DE-63CF9035122C},renderingId={DADE61AE-8C27-5EC1-A5B1-B02CC436C4E1},id={75E98485-67E9-5186-B644-091A09624205})',null,false)\",\"header\":\"dsHeaderParameter\",\"icon\":\"/temp/iconcache/office/16x16/data.png\",\"disabledIcon\":\"/temp/data_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"dsTooltipParameter\",\"type\":\"datasourcesmenu\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Test the component.\",\"type\":\"sticky\"},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"datasourcesmenu\"},{\"click\":\"chrome:rendering:delete\",\"header\":\"Delete\",\"icon\":\"/temp/iconcache/office/16x16/delete.png\",\"disabledIcon\":\"/temp/delete_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Remove component.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{75E98485-67E9-5186-B644-091A09624205}?lang=en&ver=1\",\"custom\":{\"renderingID\":\"DADE61AE8C275EC1A5B1B02CC436C4E1\",\"editable\":\"true\"},\"displayName\":\"Styleguide-Section\",\"expandedDisplayName\":null}",
                  "attributes": {
                    "type": "text/sitecore",
                    "chrometype": "rendering",
                    "kind": "open",
                    "hintname": "Styleguide-Section",
                    "id": "r_2D806C25DD4651E393DE63CF9035122C",
                    "class": "scpm",
                    "data-selectable": "true"
                  }
                },
                {
                  "uid": "2d806c25-dd46-51e3-93de-63cf9035122c",
                  "componentName": "Styleguide-Section",
                  "dataSource": "{75E98485-67E9-5186-B644-091A09624205}",
                  "params": {},
                  "fields": {
                    "heading": {
                      "value": "Sitecore Patterns",
                      "editable": "<input id='fld_75E9848567E95186B644091A09624205_51D3C9FDEB395D6096E6EF8FB686BC75_en_1_b58aee0feaf042baacf8429c632ab5ab_523' class='scFieldValue' name='fld_75E9848567E95186B644091A09624205_51D3C9FDEB395D6096E6EF8FB686BC75_en_1_b58aee0feaf042baacf8429c632ab5ab_523' type='hidden' value=\"Sitecore Patterns\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{75E98485-67E9-5186-B644-091A09624205}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"heading\",\"expandedDisplayName\":null}</span><span id=\"fld_75E9848567E95186B644091A09624205_51D3C9FDEB395D6096E6EF8FB686BC75_en_1_b58aee0feaf042baacf8429c632ab5ab_523_edit\" sc_parameters=\"prevent-line-break=true\" contenteditable=\"true\" class=\"scWebEditInput\" scFieldType=\"single-line text\" scDefaultText=\"[No text in field]\">Sitecore Patterns</span>"
                    }
                  },
                  "placeholders": {
                    "jss-styleguide-section": [
                      {
                        "name": "code",
                        "type": "text/sitecore",
                        "contents": "{\"commands\":[{\"click\":\"chrome:placeholder:addControl\",\"header\":\"Add to here\",\"icon\":\"/temp/iconcache/office/16x16/add.png\",\"disabledIcon\":\"/temp/add_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Add a new rendering to the '{0}' placeholder.\",\"type\":\"\"},{\"click\":\"chrome:placeholder:editSettings\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/window_gear.png\",\"disabledIcon\":\"/temp/window_gear_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the placeholder settings.\",\"type\":\"\"}],\"contextItemUri\":\"sitecore://master/{302C626E-8ADA-516A-AA71-B8D5D4E96B3C}?lang=en&ver=1\",\"custom\":{\"allowedRenderings\":[\"0B82E18FF08856D3BC52C4D054C4B218\",\"DAB81669A0825E998D52717D4EF215F8\",\"862F358DBB635D50ABDBE01FBD771B7F\",\"7CC402CBA3945DAAB45F32D5FCDFFF6B\",\"7C0EF2D278095B2E84221C677744445F\",\"3127DF7E6ED652DFBC2DFA4CDC65C9A6\",\"200369E789C4531BB076C53AB1C50154\",\"2D83A15B7EEF52C1BBA2EAB2D742D952\",\"0B711DC99DD2568AAECBD75DBA2F8DCD\",\"5E747641F9A457EEB62573D976639071\",\"5EB0D6136DAF5CAA8F565175867EC5BC\",\"DAC9212F32495BD883D9DC2D0F5F16F0\",\"FEACAB92BAD0551D914D19DFD91B1565\",\"6879CB46822C5F9E987B1041537629D1\",\"7F7E9A4D8FA952C18F87860195CB2C31\",\"690D327E36385C259E7F959A4D64E339\",\"2ABC73F365E85301A06165CF10397070\",\"4D4F0795D9B45A03BB91138B1BAE8A5F\"],\"editable\":\"true\"},\"displayName\":\"jss-styleguide-section\",\"expandedDisplayName\":null}",
                        "attributes": {
                          "type": "text/sitecore",
                          "chrometype": "placeholder",
                          "kind": "open",
                          "id": "_jss_main_jss_styleguide_layout__34A6553C_81DE_5CD3_989E_853F6CB6DF8C__0_jss_styleguide_section__2D806C25_DD46_51E3_93DE_63CF9035122C__0",
                          "key": "/jss-main/jss-styleguide-layout-{34A6553C-81DE-5CD3-989E-853F6CB6DF8C}-0/jss-styleguide-section-{2D806C25-DD46-51E3-93DE-63CF9035122C}-0",
                          "class": "scpm",
                          "data-selectable": "true"
                        }
                      },
                      {
                        "name": "code",
                        "type": "text/sitecore",
                        "contents": "{\"commands\":[{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:fieldeditor(command={2F7D34E2-EB93-47ED-8177-B1F429A9FCD7},fields=heading|description,id={522C0231-A64D-5FC6-AEF6-BB2FB3A2CD5B})',null,false)\",\"header\":\"Edit Fields\",\"icon\":\"/temp/iconcache/office/16x16/pencil.png\",\"disabledIcon\":\"/temp/pencil_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"\",\"type\":null},{\"click\":\"chrome:dummy\",\"header\":\"Separator\",\"icon\":\"\",\"disabledIcon\":\"\",\"isDivider\":false,\"tooltip\":null,\"type\":\"separator\"},{\"click\":\"chrome:rendering:sort\",\"header\":\"Change position\",\"icon\":\"/temp/iconcache/office/16x16/document_size.png\",\"disabledIcon\":\"/temp/document_size_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Move component.\",\"type\":\"\"},{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:componentoptions(referenceId={471FA16A-BB82-5C42-9C95-E7EAB1E3BD30},renderingId={6879CB46-822C-5F9E-987B-1041537629D1},id={522C0231-A64D-5FC6-AEF6-BB2FB3A2CD5B})',null,false)\",\"header\":\"Edit Experience Editor Options\",\"icon\":\"/temp/iconcache/office/16x16/clipboard_check_edit.png\",\"disabledIcon\":\"/temp/clipboard_check_edit_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the Experience Editor options for the component.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:properties\",\"header\":\"Edit component properties\",\"icon\":\"/temp/iconcache/office/16x16/elements_branch.png\",\"disabledIcon\":\"/temp/elements_branch_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the properties for the component.\",\"type\":\"common\"},{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:setdatasource(referenceId={471FA16A-BB82-5C42-9C95-E7EAB1E3BD30},renderingId={6879CB46-822C-5F9E-987B-1041537629D1},id={522C0231-A64D-5FC6-AEF6-BB2FB3A2CD5B})',null,false)\",\"header\":\"dsHeaderParameter\",\"icon\":\"/temp/iconcache/office/16x16/data.png\",\"disabledIcon\":\"/temp/data_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"dsTooltipParameter\",\"type\":\"datasourcesmenu\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Test the component.\",\"type\":\"sticky\"},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"datasourcesmenu\"},{\"click\":\"chrome:rendering:delete\",\"header\":\"Delete\",\"icon\":\"/temp/iconcache/office/16x16/delete.png\",\"disabledIcon\":\"/temp/delete_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Remove component.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{522C0231-A64D-5FC6-AEF6-BB2FB3A2CD5B}?lang=en&ver=1\",\"custom\":{\"renderingID\":\"6879CB46822C5F9E987B1041537629D1\",\"editable\":\"true\"},\"displayName\":\"Styleguide-SitecoreContext\",\"expandedDisplayName\":null}",
                        "attributes": {
                          "type": "text/sitecore",
                          "chrometype": "rendering",
                          "kind": "open",
                          "hintname": "Styleguide-SitecoreContext",
                          "id": "r_471FA16ABB825C429C95E7EAB1E3BD30",
                          "class": "scpm",
                          "data-selectable": "true"
                        }
                      },
                      {
                        "uid": "471fa16a-bb82-5c42-9c95-e7eab1e3bd30",
                        "componentName": "Styleguide-SitecoreContext",
                        "dataSource": "{522C0231-A64D-5FC6-AEF6-BB2FB3A2CD5B}",
                        "params": {},
                        "fields": {
                          "heading": {
                            "value": "Sitecore Context",
                            "editable": "<input id='fld_522C0231A64D5FC6AEF6BB2FB3A2CD5B_2EF2548084095250A76CC3D867340008_en_1_5e04e6ecc3df4b6cb40375700e7c7a72_515' class='scFieldValue' name='fld_522C0231A64D5FC6AEF6BB2FB3A2CD5B_2EF2548084095250A76CC3D867340008_en_1_5e04e6ecc3df4b6cb40375700e7c7a72_515' type='hidden' value=\"Sitecore Context\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{522C0231-A64D-5FC6-AEF6-BB2FB3A2CD5B}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"heading\",\"expandedDisplayName\":null}</span><span id=\"fld_522C0231A64D5FC6AEF6BB2FB3A2CD5B_2EF2548084095250A76CC3D867340008_en_1_5e04e6ecc3df4b6cb40375700e7c7a72_515_edit\" sc_parameters=\"prevent-line-break=true\" contenteditable=\"true\" class=\"scWebEditInput\" scFieldType=\"single-line text\" scDefaultText=\"[No text in field]\">Sitecore Context</span>"
                          },
                          "description": {
                            "value": "<p><small>The Sitecore Context contains route-level data about the current context - for example, <code>pageState</code> enables conditionally executing code based on whether Sitecore is in Experience Editor or not.</small></p>",
                            "editable": "<input id='fld_522C0231A64D5FC6AEF6BB2FB3A2CD5B_439F9C97BCD65D8FBE16CFECCC159EFB_en_1_5e04e6ecc3df4b6cb40375700e7c7a72_516' class='scFieldValue' name='fld_522C0231A64D5FC6AEF6BB2FB3A2CD5B_439F9C97BCD65D8FBE16CFECCC159EFB_en_1_5e04e6ecc3df4b6cb40375700e7c7a72_516' type='hidden' value=\"&lt;p&gt;&lt;small&gt;The Sitecore Context contains route-level data about the current context - for example, &lt;code&gt;pageState&lt;/code&gt; enables conditionally executing code based on whether Sitecore is in Experience Editor or not.&lt;/small&gt;&lt;/p&gt;\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:field:editcontrol({command:\\\"webedit:edithtml\\\"})\",\"header\":\"Edit Text\",\"icon\":\"/temp/iconcache/office/16x16/pencil.png\",\"disabledIcon\":\"/temp/pencil_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the text\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"bold\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_bold.png\",\"disabledIcon\":\"/temp/font_style_bold_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Bold\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"Italic\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_italics.png\",\"disabledIcon\":\"/temp/font_style_italics_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Italic\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"Underline\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_underline.png\",\"disabledIcon\":\"/temp/font_style_underline_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Underline\",\"type\":null},{\"click\":\"chrome:field:insertexternallink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/earth_link.png\",\"disabledIcon\":\"/temp/earth_link_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert an external link into the text field.\",\"type\":null},{\"click\":\"chrome:field:insertlink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/link.png\",\"disabledIcon\":\"/temp/link_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert a link into the text field.\",\"type\":null},{\"click\":\"chrome:field:removelink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/link_broken.png\",\"disabledIcon\":\"/temp/link_broken_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Remove link.\",\"type\":null},{\"click\":\"chrome:field:insertimage\",\"header\":\"Insert image\",\"icon\":\"/temp/iconcache/office/16x16/photo_landscape.png\",\"disabledIcon\":\"/temp/photo_landscape_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert an image into the text field.\",\"type\":null},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{522C0231-A64D-5FC6-AEF6-BB2FB3A2CD5B}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"description\",\"expandedDisplayName\":null}</span><span scFieldType=\"rich text\" scDefaultText=\"[No text in field]\" contenteditable=\"true\" class=\"scWebEditInput\" id=\"fld_522C0231A64D5FC6AEF6BB2FB3A2CD5B_439F9C97BCD65D8FBE16CFECCC159EFB_en_1_5e04e6ecc3df4b6cb40375700e7c7a72_516_edit\"><p><small>The Sitecore Context contains route-level data about the current context - for example, <code>pageState</code> enables conditionally executing code based on whether Sitecore is in Experience Editor or not.</small></p></span>"
                          }
                        }
                      },
                      {
                        "name": "code",
                        "type": "text/sitecore",
                        "contents": "",
                        "attributes": {
                          "type": "text/sitecore",
                          "id": "scEnclosingTag_r_",
                          "chrometype": "rendering",
                          "kind": "close",
                          "hintkey": "Styleguide-SitecoreContext",
                          "class": "scpm"
                        }
                      },
                      {
                        "name": "code",
                        "type": "text/sitecore",
                        "contents": "{\"commands\":[{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:fieldeditor(command={2F7D34E2-EB93-47ED-8177-B1F429A9FCD7},fields=heading|description,id={32F3F92B-AD4F-5EE4-93B6-1FD144B28192})',null,false)\",\"header\":\"Edit Fields\",\"icon\":\"/temp/iconcache/office/16x16/pencil.png\",\"disabledIcon\":\"/temp/pencil_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"\",\"type\":null},{\"click\":\"chrome:dummy\",\"header\":\"Separator\",\"icon\":\"\",\"disabledIcon\":\"\",\"isDivider\":false,\"tooltip\":null,\"type\":\"separator\"},{\"click\":\"chrome:rendering:sort\",\"header\":\"Change position\",\"icon\":\"/temp/iconcache/office/16x16/document_size.png\",\"disabledIcon\":\"/temp/document_size_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Move component.\",\"type\":\"\"},{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:componentoptions(referenceId={21F21053-8F8A-5436-BC79-E674E246A2FC},renderingId={7F7E9A4D-8FA9-52C1-8F87-860195CB2C31},id={32F3F92B-AD4F-5EE4-93B6-1FD144B28192})',null,false)\",\"header\":\"Edit Experience Editor Options\",\"icon\":\"/temp/iconcache/office/16x16/clipboard_check_edit.png\",\"disabledIcon\":\"/temp/clipboard_check_edit_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the Experience Editor options for the component.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:properties\",\"header\":\"Edit component properties\",\"icon\":\"/temp/iconcache/office/16x16/elements_branch.png\",\"disabledIcon\":\"/temp/elements_branch_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the properties for the component.\",\"type\":\"common\"},{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:setdatasource(referenceId={21F21053-8F8A-5436-BC79-E674E246A2FC},renderingId={7F7E9A4D-8FA9-52C1-8F87-860195CB2C31},id={32F3F92B-AD4F-5EE4-93B6-1FD144B28192})',null,false)\",\"header\":\"dsHeaderParameter\",\"icon\":\"/temp/iconcache/office/16x16/data.png\",\"disabledIcon\":\"/temp/data_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"dsTooltipParameter\",\"type\":\"datasourcesmenu\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Test the component.\",\"type\":\"sticky\"},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"datasourcesmenu\"},{\"click\":\"chrome:rendering:delete\",\"header\":\"Delete\",\"icon\":\"/temp/iconcache/office/16x16/delete.png\",\"disabledIcon\":\"/temp/delete_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Remove component.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{32F3F92B-AD4F-5EE4-93B6-1FD144B28192}?lang=en&ver=1\",\"custom\":{\"renderingID\":\"7F7E9A4D8FA952C18F87860195CB2C31\",\"editable\":\"true\"},\"displayName\":\"Styleguide-RouteFields\",\"expandedDisplayName\":null}",
                        "attributes": {
                          "type": "text/sitecore",
                          "chrometype": "rendering",
                          "kind": "open",
                          "hintname": "Styleguide-RouteFields",
                          "id": "r_21F210538F8A5436BC79E674E246A2FC",
                          "class": "scpm",
                          "data-selectable": "true"
                        }
                      },
                      {
                        "uid": "21f21053-8f8a-5436-bc79-e674e246a2fc",
                        "componentName": "Styleguide-RouteFields",
                        "dataSource": "{32F3F92B-AD4F-5EE4-93B6-1FD144B28192}",
                        "params": {},
                        "fields": {
                          "heading": {
                            "value": "Route-level Fields",
                            "editable": "<input id='fld_32F3F92BAD4F5EE493B61FD144B28192_2EF2548084095250A76CC3D867340008_en_1_26515f49b15d44c0b867628bed95b610_517' class='scFieldValue' name='fld_32F3F92BAD4F5EE493B61FD144B28192_2EF2548084095250A76CC3D867340008_en_1_26515f49b15d44c0b867628bed95b610_517' type='hidden' value=\"Route-level Fields\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{32F3F92B-AD4F-5EE4-93B6-1FD144B28192}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"heading\",\"expandedDisplayName\":null}</span><span id=\"fld_32F3F92BAD4F5EE493B61FD144B28192_2EF2548084095250A76CC3D867340008_en_1_26515f49b15d44c0b867628bed95b610_517_edit\" sc_parameters=\"prevent-line-break=true\" contenteditable=\"true\" class=\"scWebEditInput\" scFieldType=\"single-line text\" scDefaultText=\"[No text in field]\">Route-level Fields</span>"
                          },
                          "description": {
                            "value": "<p><small>Route-level content fields are defined on the <em>route</em> instead of on a <em>component</em>. This allows multiple components to share the field data on the same route - and querying is much easier on route level fields, making <em>custom route types</em> ideal for filterable/queryable data such as articles.</small></p>",
                            "editable": "<input id='fld_32F3F92BAD4F5EE493B61FD144B28192_439F9C97BCD65D8FBE16CFECCC159EFB_en_1_26515f49b15d44c0b867628bed95b610_518' class='scFieldValue' name='fld_32F3F92BAD4F5EE493B61FD144B28192_439F9C97BCD65D8FBE16CFECCC159EFB_en_1_26515f49b15d44c0b867628bed95b610_518' type='hidden' value=\"&lt;p&gt;&lt;small&gt;Route-level content fields are defined on the &lt;em&gt;route&lt;/em&gt; instead of on a &lt;em&gt;component&lt;/em&gt;. This allows multiple components to share the field data on the same route - and querying is much easier on route level fields, making &lt;em&gt;custom route types&lt;/em&gt; ideal for filterable/queryable data such as articles.&lt;/small&gt;&lt;/p&gt;\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:field:editcontrol({command:\\\"webedit:edithtml\\\"})\",\"header\":\"Edit Text\",\"icon\":\"/temp/iconcache/office/16x16/pencil.png\",\"disabledIcon\":\"/temp/pencil_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the text\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"bold\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_bold.png\",\"disabledIcon\":\"/temp/font_style_bold_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Bold\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"Italic\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_italics.png\",\"disabledIcon\":\"/temp/font_style_italics_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Italic\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"Underline\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_underline.png\",\"disabledIcon\":\"/temp/font_style_underline_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Underline\",\"type\":null},{\"click\":\"chrome:field:insertexternallink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/earth_link.png\",\"disabledIcon\":\"/temp/earth_link_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert an external link into the text field.\",\"type\":null},{\"click\":\"chrome:field:insertlink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/link.png\",\"disabledIcon\":\"/temp/link_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert a link into the text field.\",\"type\":null},{\"click\":\"chrome:field:removelink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/link_broken.png\",\"disabledIcon\":\"/temp/link_broken_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Remove link.\",\"type\":null},{\"click\":\"chrome:field:insertimage\",\"header\":\"Insert image\",\"icon\":\"/temp/iconcache/office/16x16/photo_landscape.png\",\"disabledIcon\":\"/temp/photo_landscape_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert an image into the text field.\",\"type\":null},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{32F3F92B-AD4F-5EE4-93B6-1FD144B28192}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"description\",\"expandedDisplayName\":null}</span><span scFieldType=\"rich text\" scDefaultText=\"[No text in field]\" contenteditable=\"true\" class=\"scWebEditInput\" id=\"fld_32F3F92BAD4F5EE493B61FD144B28192_439F9C97BCD65D8FBE16CFECCC159EFB_en_1_26515f49b15d44c0b867628bed95b610_518_edit\"><p><small>Route-level content fields are defined on the <em>route</em> instead of on a <em>component</em>. This allows multiple components to share the field data on the same route - and querying is much easier on route level fields, making <em>custom route types</em> ideal for filterable/queryable data such as articles.</small></p></span>"
                          }
                        }
                      },
                      {
                        "name": "code",
                        "type": "text/sitecore",
                        "contents": "",
                        "attributes": {
                          "type": "text/sitecore",
                          "id": "scEnclosingTag_r_",
                          "chrometype": "rendering",
                          "kind": "close",
                          "hintkey": "Styleguide-RouteFields",
                          "class": "scpm"
                        }
                      },
                      {
                        "name": "code",
                        "type": "text/sitecore",
                        "contents": "{\"commands\":[{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:fieldeditor(command={2F7D34E2-EB93-47ED-8177-B1F429A9FCD7},fields=heading|description,id={B4A85F78-7D62-5944-BDC0-26F3F5C81422})',null,false)\",\"header\":\"Edit Fields\",\"icon\":\"/temp/iconcache/office/16x16/pencil.png\",\"disabledIcon\":\"/temp/pencil_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"\",\"type\":null},{\"click\":\"chrome:dummy\",\"header\":\"Separator\",\"icon\":\"\",\"disabledIcon\":\"\",\"isDivider\":false,\"tooltip\":null,\"type\":\"separator\"},{\"click\":\"chrome:rendering:sort\",\"header\":\"Change position\",\"icon\":\"/temp/iconcache/office/16x16/document_size.png\",\"disabledIcon\":\"/temp/document_size_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Move component.\",\"type\":\"\"},{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:componentoptions(referenceId={A0A66136-C21F-52E8-A2EA-F04DCFA6A027},renderingId={690D327E-3638-5C25-9E7F-959A4D64E339},id={B4A85F78-7D62-5944-BDC0-26F3F5C81422})',null,false)\",\"header\":\"Edit Experience Editor Options\",\"icon\":\"/temp/iconcache/office/16x16/clipboard_check_edit.png\",\"disabledIcon\":\"/temp/clipboard_check_edit_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the Experience Editor options for the component.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:properties\",\"header\":\"Edit component properties\",\"icon\":\"/temp/iconcache/office/16x16/elements_branch.png\",\"disabledIcon\":\"/temp/elements_branch_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the properties for the component.\",\"type\":\"common\"},{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:setdatasource(referenceId={A0A66136-C21F-52E8-A2EA-F04DCFA6A027},renderingId={690D327E-3638-5C25-9E7F-959A4D64E339},id={B4A85F78-7D62-5944-BDC0-26F3F5C81422})',null,false)\",\"header\":\"dsHeaderParameter\",\"icon\":\"/temp/iconcache/office/16x16/data.png\",\"disabledIcon\":\"/temp/data_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"dsTooltipParameter\",\"type\":\"datasourcesmenu\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Test the component.\",\"type\":\"sticky\"},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"datasourcesmenu\"},{\"click\":\"chrome:rendering:delete\",\"header\":\"Delete\",\"icon\":\"/temp/iconcache/office/16x16/delete.png\",\"disabledIcon\":\"/temp/delete_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Remove component.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{B4A85F78-7D62-5944-BDC0-26F3F5C81422}?lang=en&ver=1\",\"custom\":{\"renderingID\":\"690D327E36385C259E7F959A4D64E339\",\"editable\":\"true\"},\"displayName\":\"Styleguide-ComponentParams\",\"expandedDisplayName\":null}",
                        "attributes": {
                          "type": "text/sitecore",
                          "chrometype": "rendering",
                          "kind": "open",
                          "hintname": "Styleguide-ComponentParams",
                          "id": "r_A0A66136C21F52E8A2EAF04DCFA6A027",
                          "class": "scpm",
                          "data-selectable": "true"
                        }
                      },
                      {
                        "uid": "a0a66136-c21f-52e8-a2ea-f04dcfa6a027",
                        "componentName": "Styleguide-ComponentParams",
                        "dataSource": "{B4A85F78-7D62-5944-BDC0-26F3F5C81422}",
                        "params": {
                          "cssClass": "alert alert-success",
                          "columns": "5",
                          "useCallToAction": "true"
                        },
                        "fields": {
                          "heading": {
                            "value": "Component Params",
                            "editable": "<input id='fld_B4A85F787D625944BDC026F3F5C81422_2EF2548084095250A76CC3D867340008_en_1_3f8748d3b9b04c18b05c9172543b2c78_519' class='scFieldValue' name='fld_B4A85F787D625944BDC026F3F5C81422_2EF2548084095250A76CC3D867340008_en_1_3f8748d3b9b04c18b05c9172543b2c78_519' type='hidden' value=\"Component Params\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{B4A85F78-7D62-5944-BDC0-26F3F5C81422}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"heading\",\"expandedDisplayName\":null}</span><span id=\"fld_B4A85F787D625944BDC026F3F5C81422_2EF2548084095250A76CC3D867340008_en_1_3f8748d3b9b04c18b05c9172543b2c78_519_edit\" sc_parameters=\"prevent-line-break=true\" contenteditable=\"true\" class=\"scWebEditInput\" scFieldType=\"single-line text\" scDefaultText=\"[No text in field]\">Component Params</span>"
                          },
                          "description": {
                            "value": "<p><small>Component params (also called Rendering Parameters) allow storing non-content parameters for a component. These params should be used for more technical options such as CSS class names or structural settings.</small></p>",
                            "editable": "<input id='fld_B4A85F787D625944BDC026F3F5C81422_439F9C97BCD65D8FBE16CFECCC159EFB_en_1_3f8748d3b9b04c18b05c9172543b2c78_520' class='scFieldValue' name='fld_B4A85F787D625944BDC026F3F5C81422_439F9C97BCD65D8FBE16CFECCC159EFB_en_1_3f8748d3b9b04c18b05c9172543b2c78_520' type='hidden' value=\"&lt;p&gt;&lt;small&gt;Component params (also called Rendering Parameters) allow storing non-content parameters for a component. These params should be used for more technical options such as CSS class names or structural settings.&lt;/small&gt;&lt;/p&gt;\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:field:editcontrol({command:\\\"webedit:edithtml\\\"})\",\"header\":\"Edit Text\",\"icon\":\"/temp/iconcache/office/16x16/pencil.png\",\"disabledIcon\":\"/temp/pencil_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the text\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"bold\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_bold.png\",\"disabledIcon\":\"/temp/font_style_bold_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Bold\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"Italic\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_italics.png\",\"disabledIcon\":\"/temp/font_style_italics_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Italic\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"Underline\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_underline.png\",\"disabledIcon\":\"/temp/font_style_underline_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Underline\",\"type\":null},{\"click\":\"chrome:field:insertexternallink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/earth_link.png\",\"disabledIcon\":\"/temp/earth_link_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert an external link into the text field.\",\"type\":null},{\"click\":\"chrome:field:insertlink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/link.png\",\"disabledIcon\":\"/temp/link_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert a link into the text field.\",\"type\":null},{\"click\":\"chrome:field:removelink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/link_broken.png\",\"disabledIcon\":\"/temp/link_broken_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Remove link.\",\"type\":null},{\"click\":\"chrome:field:insertimage\",\"header\":\"Insert image\",\"icon\":\"/temp/iconcache/office/16x16/photo_landscape.png\",\"disabledIcon\":\"/temp/photo_landscape_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert an image into the text field.\",\"type\":null},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{B4A85F78-7D62-5944-BDC0-26F3F5C81422}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"description\",\"expandedDisplayName\":null}</span><span scFieldType=\"rich text\" scDefaultText=\"[No text in field]\" contenteditable=\"true\" class=\"scWebEditInput\" id=\"fld_B4A85F787D625944BDC026F3F5C81422_439F9C97BCD65D8FBE16CFECCC159EFB_en_1_3f8748d3b9b04c18b05c9172543b2c78_520_edit\"><p><small>Component params (also called Rendering Parameters) allow storing non-content parameters for a component. These params should be used for more technical options such as CSS class names or structural settings.</small></p></span>"
                          }
                        }
                      },
                      {
                        "name": "code",
                        "type": "text/sitecore",
                        "contents": "",
                        "attributes": {
                          "type": "text/sitecore",
                          "id": "scEnclosingTag_r_",
                          "chrometype": "rendering",
                          "kind": "close",
                          "hintkey": "Styleguide-ComponentParams",
                          "class": "scpm"
                        }
                      },
                      {
                        "name": "code",
                        "type": "text/sitecore",
                        "contents": "{\"commands\":[{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:fieldeditor(command={2F7D34E2-EB93-47ED-8177-B1F429A9FCD7},fields=heading|description,id={E38E7405-E2F4-512E-AD4C-3AFEB8CDF582})',null,false)\",\"header\":\"Edit Fields\",\"icon\":\"/temp/iconcache/office/16x16/pencil.png\",\"disabledIcon\":\"/temp/pencil_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"\",\"type\":null},{\"click\":\"chrome:dummy\",\"header\":\"Separator\",\"icon\":\"\",\"disabledIcon\":\"\",\"isDivider\":false,\"tooltip\":null,\"type\":\"separator\"},{\"click\":\"chrome:rendering:sort\",\"header\":\"Change position\",\"icon\":\"/temp/iconcache/office/16x16/document_size.png\",\"disabledIcon\":\"/temp/document_size_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Move component.\",\"type\":\"\"},{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:componentoptions(referenceId={7F765FCB-3B10-58FD-8AA7-B346EF38C9BB},renderingId={2ABC73F3-65E8-5301-A061-65CF10397070},id={E38E7405-E2F4-512E-AD4C-3AFEB8CDF582})',null,false)\",\"header\":\"Edit Experience Editor Options\",\"icon\":\"/temp/iconcache/office/16x16/clipboard_check_edit.png\",\"disabledIcon\":\"/temp/clipboard_check_edit_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the Experience Editor options for the component.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:properties\",\"header\":\"Edit component properties\",\"icon\":\"/temp/iconcache/office/16x16/elements_branch.png\",\"disabledIcon\":\"/temp/elements_branch_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the properties for the component.\",\"type\":\"common\"},{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:setdatasource(referenceId={7F765FCB-3B10-58FD-8AA7-B346EF38C9BB},renderingId={2ABC73F3-65E8-5301-A061-65CF10397070},id={E38E7405-E2F4-512E-AD4C-3AFEB8CDF582})',null,false)\",\"header\":\"dsHeaderParameter\",\"icon\":\"/temp/iconcache/office/16x16/data.png\",\"disabledIcon\":\"/temp/data_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"dsTooltipParameter\",\"type\":\"datasourcesmenu\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Test the component.\",\"type\":\"sticky\"},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"datasourcesmenu\"},{\"click\":\"chrome:rendering:delete\",\"header\":\"Delete\",\"icon\":\"/temp/iconcache/office/16x16/delete.png\",\"disabledIcon\":\"/temp/delete_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Remove component.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{E38E7405-E2F4-512E-AD4C-3AFEB8CDF582}?lang=en&ver=1\",\"custom\":{\"renderingID\":\"2ABC73F365E85301A06165CF10397070\",\"editable\":\"true\"},\"displayName\":\"Styleguide-Tracking\",\"expandedDisplayName\":null}",
                        "attributes": {
                          "type": "text/sitecore",
                          "chrometype": "rendering",
                          "kind": "open",
                          "hintname": "Styleguide-Tracking",
                          "id": "r_7F765FCB3B1058FD8AA7B346EF38C9BB",
                          "class": "scpm",
                          "data-selectable": "true"
                        }
                      },
                      {
                        "uid": "7f765fcb-3b10-58fd-8aa7-b346ef38c9bb",
                        "componentName": "Styleguide-Tracking",
                        "dataSource": "{E38E7405-E2F4-512E-AD4C-3AFEB8CDF582}",
                        "params": {},
                        "fields": {
                          "heading": {
                            "value": "Tracking",
                            "editable": "<input id='fld_E38E7405E2F4512EAD4C3AFEB8CDF582_2EF2548084095250A76CC3D867340008_en_1_bb56c0cac88347efa7ef64d08188c671_521' class='scFieldValue' name='fld_E38E7405E2F4512EAD4C3AFEB8CDF582_2EF2548084095250A76CC3D867340008_en_1_bb56c0cac88347efa7ef64d08188c671_521' type='hidden' value=\"Tracking\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{E38E7405-E2F4-512E-AD4C-3AFEB8CDF582}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"heading\",\"expandedDisplayName\":null}</span><span id=\"fld_E38E7405E2F4512EAD4C3AFEB8CDF582_2EF2548084095250A76CC3D867340008_en_1_bb56c0cac88347efa7ef64d08188c671_521_edit\" sc_parameters=\"prevent-line-break=true\" contenteditable=\"true\" class=\"scWebEditInput\" scFieldType=\"single-line text\" scDefaultText=\"[No text in field]\">Tracking</span>"
                          },
                          "description": {
                            "value": "<p><small>JSS supports tracking Sitecore analytics events from within apps. Give it a try with this handy interactive demo.</small></p>",
                            "editable": "<input id='fld_E38E7405E2F4512EAD4C3AFEB8CDF582_439F9C97BCD65D8FBE16CFECCC159EFB_en_1_bb56c0cac88347efa7ef64d08188c671_522' class='scFieldValue' name='fld_E38E7405E2F4512EAD4C3AFEB8CDF582_439F9C97BCD65D8FBE16CFECCC159EFB_en_1_bb56c0cac88347efa7ef64d08188c671_522' type='hidden' value=\"&lt;p&gt;&lt;small&gt;JSS supports tracking Sitecore analytics events from within apps. Give it a try with this handy interactive demo.&lt;/small&gt;&lt;/p&gt;\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:field:editcontrol({command:\\\"webedit:edithtml\\\"})\",\"header\":\"Edit Text\",\"icon\":\"/temp/iconcache/office/16x16/pencil.png\",\"disabledIcon\":\"/temp/pencil_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the text\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"bold\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_bold.png\",\"disabledIcon\":\"/temp/font_style_bold_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Bold\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"Italic\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_italics.png\",\"disabledIcon\":\"/temp/font_style_italics_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Italic\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"Underline\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_underline.png\",\"disabledIcon\":\"/temp/font_style_underline_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Underline\",\"type\":null},{\"click\":\"chrome:field:insertexternallink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/earth_link.png\",\"disabledIcon\":\"/temp/earth_link_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert an external link into the text field.\",\"type\":null},{\"click\":\"chrome:field:insertlink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/link.png\",\"disabledIcon\":\"/temp/link_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert a link into the text field.\",\"type\":null},{\"click\":\"chrome:field:removelink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/link_broken.png\",\"disabledIcon\":\"/temp/link_broken_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Remove link.\",\"type\":null},{\"click\":\"chrome:field:insertimage\",\"header\":\"Insert image\",\"icon\":\"/temp/iconcache/office/16x16/photo_landscape.png\",\"disabledIcon\":\"/temp/photo_landscape_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert an image into the text field.\",\"type\":null},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{E38E7405-E2F4-512E-AD4C-3AFEB8CDF582}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"description\",\"expandedDisplayName\":null}</span><span scFieldType=\"rich text\" scDefaultText=\"[No text in field]\" contenteditable=\"true\" class=\"scWebEditInput\" id=\"fld_E38E7405E2F4512EAD4C3AFEB8CDF582_439F9C97BCD65D8FBE16CFECCC159EFB_en_1_bb56c0cac88347efa7ef64d08188c671_522_edit\"><p><small>JSS supports tracking Sitecore analytics events from within apps. Give it a try with this handy interactive demo.</small></p></span>"
                          }
                        }
                      },
                      {
                        "name": "code",
                        "type": "text/sitecore",
                        "contents": "",
                        "attributes": {
                          "type": "text/sitecore",
                          "id": "scEnclosingTag_r_",
                          "chrometype": "rendering",
                          "kind": "close",
                          "hintkey": "Styleguide-Tracking",
                          "class": "scpm"
                        }
                      },
                      {
                        "name": "code",
                        "type": "text/sitecore",
                        "contents": "",
                        "attributes": {
                          "type": "text/sitecore",
                          "id": "scEnclosingTag_",
                          "chrometype": "placeholder",
                          "kind": "close",
                          "hintname": "jss-styleguide-section",
                          "class": "scpm"
                        }
                      }
                    ]
                  }
                },
                {
                  "name": "code",
                  "type": "text/sitecore",
                  "contents": "",
                  "attributes": {
                    "type": "text/sitecore",
                    "id": "scEnclosingTag_r_",
                    "chrometype": "rendering",
                    "kind": "close",
                    "hintkey": "Styleguide-Section",
                    "class": "scpm"
                  }
                },
                {
                  "name": "code",
                  "type": "text/sitecore",
                  "contents": "{\"commands\":[{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:fieldeditor(command={2F7D34E2-EB93-47ED-8177-B1F429A9FCD7},fields=heading,id={C89229C7-F3B5-54E2-9EBC-6ABCA2726271})',null,false)\",\"header\":\"Edit Fields\",\"icon\":\"/temp/iconcache/office/16x16/pencil.png\",\"disabledIcon\":\"/temp/pencil_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"\",\"type\":null},{\"click\":\"chrome:dummy\",\"header\":\"Separator\",\"icon\":\"\",\"disabledIcon\":\"\",\"isDivider\":false,\"tooltip\":null,\"type\":\"separator\"},{\"click\":\"chrome:rendering:sort\",\"header\":\"Change position\",\"icon\":\"/temp/iconcache/office/16x16/document_size.png\",\"disabledIcon\":\"/temp/document_size_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Move component.\",\"type\":\"\"},{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:componentoptions(referenceId={66AF8F03-0B52-5425-A6AF-6FB54F2D64D9},renderingId={DADE61AE-8C27-5EC1-A5B1-B02CC436C4E1},id={C89229C7-F3B5-54E2-9EBC-6ABCA2726271})',null,false)\",\"header\":\"Edit Experience Editor Options\",\"icon\":\"/temp/iconcache/office/16x16/clipboard_check_edit.png\",\"disabledIcon\":\"/temp/clipboard_check_edit_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the Experience Editor options for the component.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:properties\",\"header\":\"Edit component properties\",\"icon\":\"/temp/iconcache/office/16x16/elements_branch.png\",\"disabledIcon\":\"/temp/elements_branch_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the properties for the component.\",\"type\":\"common\"},{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:setdatasource(referenceId={66AF8F03-0B52-5425-A6AF-6FB54F2D64D9},renderingId={DADE61AE-8C27-5EC1-A5B1-B02CC436C4E1},id={C89229C7-F3B5-54E2-9EBC-6ABCA2726271})',null,false)\",\"header\":\"dsHeaderParameter\",\"icon\":\"/temp/iconcache/office/16x16/data.png\",\"disabledIcon\":\"/temp/data_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"dsTooltipParameter\",\"type\":\"datasourcesmenu\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Test the component.\",\"type\":\"sticky\"},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"datasourcesmenu\"},{\"click\":\"chrome:rendering:delete\",\"header\":\"Delete\",\"icon\":\"/temp/iconcache/office/16x16/delete.png\",\"disabledIcon\":\"/temp/delete_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Remove component.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{C89229C7-F3B5-54E2-9EBC-6ABCA2726271}?lang=en&ver=1\",\"custom\":{\"renderingID\":\"DADE61AE8C275EC1A5B1B02CC436C4E1\",\"editable\":\"true\"},\"displayName\":\"Styleguide-Section\",\"expandedDisplayName\":null}",
                  "attributes": {
                    "type": "text/sitecore",
                    "chrometype": "rendering",
                    "kind": "open",
                    "hintname": "Styleguide-Section",
                    "id": "r_66AF8F030B525425A6AF6FB54F2D64D9",
                    "class": "scpm",
                    "data-selectable": "true"
                  }
                },
                {
                  "uid": "66af8f03-0b52-5425-a6af-6fb54f2d64d9",
                  "componentName": "Styleguide-Section",
                  "dataSource": "{C89229C7-F3B5-54E2-9EBC-6ABCA2726271}",
                  "params": {},
                  "fields": {
                    "heading": {
                      "value": "Multilingual Patterns",
                      "editable": "<input id='fld_C89229C7F3B554E29EBC6ABCA2726271_51D3C9FDEB395D6096E6EF8FB686BC75_en_1_82084f836fd245669d04cb356ae2134f_527' class='scFieldValue' name='fld_C89229C7F3B554E29EBC6ABCA2726271_51D3C9FDEB395D6096E6EF8FB686BC75_en_1_82084f836fd245669d04cb356ae2134f_527' type='hidden' value=\"Multilingual Patterns\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{C89229C7-F3B5-54E2-9EBC-6ABCA2726271}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"heading\",\"expandedDisplayName\":null}</span><span id=\"fld_C89229C7F3B554E29EBC6ABCA2726271_51D3C9FDEB395D6096E6EF8FB686BC75_en_1_82084f836fd245669d04cb356ae2134f_527_edit\" sc_parameters=\"prevent-line-break=true\" contenteditable=\"true\" class=\"scWebEditInput\" scFieldType=\"single-line text\" scDefaultText=\"[No text in field]\">Multilingual Patterns</span>"
                    }
                  },
                  "placeholders": {
                    "jss-styleguide-section": [
                      {
                        "name": "code",
                        "type": "text/sitecore",
                        "contents": "{\"commands\":[{\"click\":\"chrome:placeholder:addControl\",\"header\":\"Add to here\",\"icon\":\"/temp/iconcache/office/16x16/add.png\",\"disabledIcon\":\"/temp/add_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Add a new rendering to the '{0}' placeholder.\",\"type\":\"\"},{\"click\":\"chrome:placeholder:editSettings\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/window_gear.png\",\"disabledIcon\":\"/temp/window_gear_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the placeholder settings.\",\"type\":\"\"}],\"contextItemUri\":\"sitecore://master/{302C626E-8ADA-516A-AA71-B8D5D4E96B3C}?lang=en&ver=1\",\"custom\":{\"allowedRenderings\":[\"0B82E18FF08856D3BC52C4D054C4B218\",\"DAB81669A0825E998D52717D4EF215F8\",\"862F358DBB635D50ABDBE01FBD771B7F\",\"7CC402CBA3945DAAB45F32D5FCDFFF6B\",\"7C0EF2D278095B2E84221C677744445F\",\"3127DF7E6ED652DFBC2DFA4CDC65C9A6\",\"200369E789C4531BB076C53AB1C50154\",\"2D83A15B7EEF52C1BBA2EAB2D742D952\",\"0B711DC99DD2568AAECBD75DBA2F8DCD\",\"5E747641F9A457EEB62573D976639071\",\"5EB0D6136DAF5CAA8F565175867EC5BC\",\"DAC9212F32495BD883D9DC2D0F5F16F0\",\"FEACAB92BAD0551D914D19DFD91B1565\",\"6879CB46822C5F9E987B1041537629D1\",\"7F7E9A4D8FA952C18F87860195CB2C31\",\"690D327E36385C259E7F959A4D64E339\",\"2ABC73F365E85301A06165CF10397070\",\"4D4F0795D9B45A03BB91138B1BAE8A5F\"],\"editable\":\"true\"},\"displayName\":\"jss-styleguide-section\",\"expandedDisplayName\":null}",
                        "attributes": {
                          "type": "text/sitecore",
                          "chrometype": "placeholder",
                          "kind": "open",
                          "id": "_jss_main_jss_styleguide_layout__34A6553C_81DE_5CD3_989E_853F6CB6DF8C__0_jss_styleguide_section__66AF8F03_0B52_5425_A6AF_6FB54F2D64D9__0",
                          "key": "/jss-main/jss-styleguide-layout-{34A6553C-81DE-5CD3-989E-853F6CB6DF8C}-0/jss-styleguide-section-{66AF8F03-0B52-5425-A6AF-6FB54F2D64D9}-0",
                          "class": "scpm",
                          "data-selectable": "true"
                        }
                      },
                      {
                        "name": "code",
                        "type": "text/sitecore",
                        "contents": "{\"commands\":[{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:fieldeditor(command={2F7D34E2-EB93-47ED-8177-B1F429A9FCD7},fields=sample|heading|description,id={2A2B3CFF-B9B0-5D6A-9BB4-3214C2E10D19})',null,false)\",\"header\":\"Edit Fields\",\"icon\":\"/temp/iconcache/office/16x16/pencil.png\",\"disabledIcon\":\"/temp/pencil_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"\",\"type\":null},{\"click\":\"chrome:dummy\",\"header\":\"Separator\",\"icon\":\"\",\"disabledIcon\":\"\",\"isDivider\":false,\"tooltip\":null,\"type\":\"separator\"},{\"click\":\"chrome:rendering:sort\",\"header\":\"Change position\",\"icon\":\"/temp/iconcache/office/16x16/document_size.png\",\"disabledIcon\":\"/temp/document_size_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Move component.\",\"type\":\"\"},{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:componentoptions(referenceId={CF1B5D2B-C949-56E7-9594-66AFACEACA9D},renderingId={4D4F0795-D9B4-5A03-BB91-138B1BAE8A5F},id={2A2B3CFF-B9B0-5D6A-9BB4-3214C2E10D19})',null,false)\",\"header\":\"Edit Experience Editor Options\",\"icon\":\"/temp/iconcache/office/16x16/clipboard_check_edit.png\",\"disabledIcon\":\"/temp/clipboard_check_edit_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the Experience Editor options for the component.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:properties\",\"header\":\"Edit component properties\",\"icon\":\"/temp/iconcache/office/16x16/elements_branch.png\",\"disabledIcon\":\"/temp/elements_branch_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the properties for the component.\",\"type\":\"common\"},{\"click\":\"javascript:Sitecore.PageModes.PageEditor.postRequest('webedit:setdatasource(referenceId={CF1B5D2B-C949-56E7-9594-66AFACEACA9D},renderingId={4D4F0795-D9B4-5A03-BB91-138B1BAE8A5F},id={2A2B3CFF-B9B0-5D6A-9BB4-3214C2E10D19})',null,false)\",\"header\":\"dsHeaderParameter\",\"icon\":\"/temp/iconcache/office/16x16/data.png\",\"disabledIcon\":\"/temp/data_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"dsTooltipParameter\",\"type\":\"datasourcesmenu\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Test the component.\",\"type\":\"sticky\"},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"datasourcesmenu\"},{\"click\":\"chrome:rendering:delete\",\"header\":\"Delete\",\"icon\":\"/temp/iconcache/office/16x16/delete.png\",\"disabledIcon\":\"/temp/delete_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Remove component.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{2A2B3CFF-B9B0-5D6A-9BB4-3214C2E10D19}?lang=en&ver=1\",\"custom\":{\"renderingID\":\"4D4F0795D9B45A03BB91138B1BAE8A5F\",\"editable\":\"true\"},\"displayName\":\"Styleguide-Multilingual\",\"expandedDisplayName\":null}",
                        "attributes": {
                          "type": "text/sitecore",
                          "chrometype": "rendering",
                          "kind": "open",
                          "hintname": "Styleguide-Multilingual",
                          "id": "r_CF1B5D2BC94956E7959466AFACEACA9D",
                          "class": "scpm",
                          "data-selectable": "true"
                        }
                      },
                      {
                        "uid": "cf1b5d2b-c949-56e7-9594-66afaceaca9d",
                        "componentName": "Styleguide-Multilingual",
                        "dataSource": "{2A2B3CFF-B9B0-5D6A-9BB4-3214C2E10D19}",
                        "params": {},
                        "fields": {
                          "sample": {
                            "value": "This text can be translated in en.yml",
                            "editable": "<input id='fld_2A2B3CFFB9B05D6A9BB43214C2E10D19_63CD8FD82EB65F008930F25B0352337B_en_1_dcfb24c0f3ee4591870dc0f0021803a8_524' class='scFieldValue' name='fld_2A2B3CFFB9B05D6A9BB43214C2E10D19_63CD8FD82EB65F008930F25B0352337B_en_1_dcfb24c0f3ee4591870dc0f0021803a8_524' type='hidden' value=\"This text can be translated in en.yml\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{2A2B3CFF-B9B0-5D6A-9BB4-3214C2E10D19}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"This field has a translated value\",\"expandedDisplayName\":null}</span><span id=\"fld_2A2B3CFFB9B05D6A9BB43214C2E10D19_63CD8FD82EB65F008930F25B0352337B_en_1_dcfb24c0f3ee4591870dc0f0021803a8_524_edit\" sc_parameters=\"prevent-line-break=true\" contenteditable=\"true\" class=\"scWebEditInput\" scFieldType=\"single-line text\" scDefaultText=\"[No text in field]\">This text can be translated in en.yml</span>"
                          },
                          "heading": {
                            "value": "Translation Patterns",
                            "editable": "<input id='fld_2A2B3CFFB9B05D6A9BB43214C2E10D19_2EF2548084095250A76CC3D867340008_en_1_dcfb24c0f3ee4591870dc0f0021803a8_525' class='scFieldValue' name='fld_2A2B3CFFB9B05D6A9BB43214C2E10D19_2EF2548084095250A76CC3D867340008_en_1_dcfb24c0f3ee4591870dc0f0021803a8_525' type='hidden' value=\"Translation Patterns\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{2A2B3CFF-B9B0-5D6A-9BB4-3214C2E10D19}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"heading\",\"expandedDisplayName\":null}</span><span id=\"fld_2A2B3CFFB9B05D6A9BB43214C2E10D19_2EF2548084095250A76CC3D867340008_en_1_dcfb24c0f3ee4591870dc0f0021803a8_525_edit\" sc_parameters=\"prevent-line-break=true\" contenteditable=\"true\" class=\"scWebEditInput\" scFieldType=\"single-line text\" scDefaultText=\"[No text in field]\">Translation Patterns</span>"
                          },
                          "description": {
                            "value": "",
                            "editable": "<input id='fld_2A2B3CFFB9B05D6A9BB43214C2E10D19_439F9C97BCD65D8FBE16CFECCC159EFB_en_1_dcfb24c0f3ee4591870dc0f0021803a8_526' class='scFieldValue' name='fld_2A2B3CFFB9B05D6A9BB43214C2E10D19_439F9C97BCD65D8FBE16CFECCC159EFB_en_1_dcfb24c0f3ee4591870dc0f0021803a8_526' type='hidden' value=\"\" /><span class=\"scChromeData\">{\"commands\":[{\"click\":\"chrome:field:editcontrol({command:\\\"webedit:edithtml\\\"})\",\"header\":\"Edit Text\",\"icon\":\"/temp/iconcache/office/16x16/pencil.png\",\"disabledIcon\":\"/temp/pencil_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the text\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"bold\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_bold.png\",\"disabledIcon\":\"/temp/font_style_bold_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Bold\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"Italic\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_italics.png\",\"disabledIcon\":\"/temp/font_style_italics_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Italic\",\"type\":null},{\"click\":\"chrome:field:execute({command:\\\"Underline\\\", userInterface:true, value:true})\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/font_style_underline.png\",\"disabledIcon\":\"/temp/font_style_underline_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Underline\",\"type\":null},{\"click\":\"chrome:field:insertexternallink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/earth_link.png\",\"disabledIcon\":\"/temp/earth_link_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert an external link into the text field.\",\"type\":null},{\"click\":\"chrome:field:insertlink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/link.png\",\"disabledIcon\":\"/temp/link_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert a link into the text field.\",\"type\":null},{\"click\":\"chrome:field:removelink\",\"header\":\"\",\"icon\":\"/temp/iconcache/office/16x16/link_broken.png\",\"disabledIcon\":\"/temp/link_broken_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Remove link.\",\"type\":null},{\"click\":\"chrome:field:insertimage\",\"header\":\"Insert image\",\"icon\":\"/temp/iconcache/office/16x16/photo_landscape.png\",\"disabledIcon\":\"/temp/photo_landscape_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Insert an image into the text field.\",\"type\":null},{\"click\":\"chrome:common:edititem({command:\\\"webedit:open\\\"})\",\"header\":\"Edit the related item\",\"icon\":\"/temp/iconcache/office/16x16/cubes.png\",\"disabledIcon\":\"/temp/cubes_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the related item in the Content Editor.\",\"type\":\"common\"},{\"click\":\"chrome:rendering:personalize({command:\\\"webedit:personalize\\\"})\",\"header\":\"Personalize\",\"icon\":\"/temp/iconcache/office/16x16/users_family.png\",\"disabledIcon\":\"/temp/users_family_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Create or edit personalization for this component.\",\"type\":\"sticky\"},{\"click\":\"chrome:rendering:editvariations({command:\\\"webedit:editvariations\\\"})\",\"header\":\"Edit variations\",\"icon\":\"/temp/iconcache/office/16x16/windows.png\",\"disabledIcon\":\"/temp/windows_disabled16x16.png\",\"isDivider\":false,\"tooltip\":\"Edit the variations.\",\"type\":\"sticky\"}],\"contextItemUri\":\"sitecore://master/{2A2B3CFF-B9B0-5D6A-9BB4-3214C2E10D19}?lang=en&ver=1\",\"custom\":{},\"displayName\":\"description\",\"expandedDisplayName\":null}</span><span id=\"fld_2A2B3CFFB9B05D6A9BB43214C2E10D19_439F9C97BCD65D8FBE16CFECCC159EFB_en_1_dcfb24c0f3ee4591870dc0f0021803a8_526_edit\" contenteditable=\"true\" class=\"scWebEditInput\" scFieldType=\"rich text\" scDefaultText=\"[No text in field]\" scWatermark=\"true\">[No text in field]</span>"
                          }
                        }
                      },
                      {
                        "name": "code",
                        "type": "text/sitecore",
                        "contents": "",
                        "attributes": {
                          "type": "text/sitecore",
                          "id": "scEnclosingTag_r_",
                          "chrometype": "rendering",
                          "kind": "close",
                          "hintkey": "Styleguide-Multilingual",
                          "class": "scpm"
                        }
                      },
                      {
                        "name": "code",
                        "type": "text/sitecore",
                        "contents": "",
                        "attributes": {
                          "type": "text/sitecore",
                          "id": "scEnclosingTag_",
                          "chrometype": "placeholder",
                          "kind": "close",
                          "hintname": "jss-styleguide-section",
                          "class": "scpm"
                        }
                      }
                    ]
                  }
                },
                {
                  "name": "code",
                  "type": "text/sitecore",
                  "contents": "",
                  "attributes": {
                    "type": "text/sitecore",
                    "id": "scEnclosingTag_r_",
                    "chrometype": "rendering",
                    "kind": "close",
                    "hintkey": "Styleguide-Section",
                    "class": "scpm"
                  }
                },
                {
                  "name": "code",
                  "type": "text/sitecore",
                  "contents": "",
                  "attributes": {
                    "type": "text/sitecore",
                    "id": "scEnclosingTag_",
                    "chrometype": "placeholder",
                    "kind": "close",
                    "hintname": "jss-styleguide-layout",
                    "class": "scpm"
                  }
                }
              ]
            }
          },
          {
            "name": "code",
            "type": "text/sitecore",
            "contents": "",
            "attributes": {
              "type": "text/sitecore",
              "id": "scEnclosingTag_r_",
              "chrometype": "rendering",
              "kind": "close",
              "hintkey": "Styleguide-Layout",
              "class": "scpm"
            }
          },
          {
            "name": "code",
            "type": "text/sitecore",
            "contents": "",
            "attributes": {
              "type": "text/sitecore",
              "id": "scEnclosingTag_",
              "chrometype": "placeholder",
              "kind": "close",
              "hintname": "Main",
              "class": "scpm"
            }
          }
        ]
      }
    }
  }
}

module.exports = {
  styleguide,
  home,
  editor
}