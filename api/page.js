const express = require('express')
const axios = require('axios')
const { styleguide, home, editor } = require('./mockData')
const { mergeEditable } = require('./formatResponse')

const router = express.Router()

router.get('/*', (req, res) => {
  const base = process.env.ENDPOINT_URL
  const apiKey = process.env.ENDPOINT_API_KEY
  const path = encodeURIComponent(req.path)

  if (!base) {
    console.log('Missing ENDPOINT_URL')
  }

  if (!apiKey) {
    console.log('Missing ENDPOINT_API_KEY')
  }

  const isMissingEnv = !base || !apiKey

  if (req.path.toLowerCase() === '/editor' ||
    req.path.toLowerCase() === '/editor/') {
      const copy = JSON.parse(JSON.stringify(editor))
      const formatted = mergeEditable(copy)
      res.json(req.query.dev
        ? { formatted, original: editor }
        : formatted
      )

    return
  }


  if (isMissingEnv) {
    if (req.path.toLowerCase() === '/styleguide' ||
      req.path.toLowerCase() === '/styleguide/') {
        const copy = JSON.parse(JSON.stringify(styleguide))
        const formatted = mergeEditable(copy)
        res.json(req.query.dev
          ? { formatted, original: styleguide }
          : formatted
        )
    } else if (req.path === '/') {
      res.json(mergeEditable(home))
    } else {
      res.json('Missing ENDPOINT_URL or ENDPOINT_API_KEY')
    }

    return
  }

  const url = `${base}?item=${path}&$lang=en&sc_apikey=${apiKey}`

  axios.get(url)
    .then(response => {
      const data = response.data
      const copy = JSON.parse(JSON.stringify(data))
      const formatted = mergeEditable(copy)
      res.json(req.query.dev
        ? { formatted, original: data }
        : formatted
      )
    })
    .catch(error => res.status(404))
})

module.exports = router
