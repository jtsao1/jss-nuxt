const express = require('express')
const { Nuxt, Builder } = require('nuxt')
const config = require('./nuxt.config.js')
const pageRouter = require('./api/page')

const app = express()
app.use('/api/pages', pageRouter)

const nuxt = new Nuxt(config)
nuxt.ready()
  .then(_ => {
    if (nuxt.options.dev) {
      new Builder(nuxt).build()
    }
    
    app.use(nuxt.render)
    const port = process.env.PORT || 3030
    app.listen(port, () => console.log('Server running on port', port))
  })
