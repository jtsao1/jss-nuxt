const { Nuxt } = require('nuxt')
const config = require('./nuxt.config.js')
config.dev = false

const nuxt = new Nuxt(config)

const isObject = obj => typeof obj === 'object' && obj !== null

const each = (obj, callback) => {
  if (!isObject(obj)) {
    return
  }

  Object.keys(obj).forEach(key => callback(obj[key], key, obj))
}

const isEditableField = obj => {
  if (!isObject(obj)) {
    return false
  }

  const keys = Object.keys(obj)

  if (keys.length === 1) {
    return obj.hasOwnProperty('value')
  } else if (keys.length === 2) {
    return obj.hasOwnProperty('value') &&
    obj.hasOwnProperty('editable')
  } else if (keys.length === 4) {
    return obj.hasOwnProperty('value') &&
    obj.hasOwnProperty('editable') &&
    obj.hasOwnProperty('editableFirstPart') &&
    obj.hasOwnProperty('editableLastPart')
  } else {
    return false
  }    
}

const mergeEditable = obj => {
  if (!isObject(obj)) {
    return obj
  }

  each(obj, (fieldValue, key, collection) => {
    if (isEditableField(fieldValue)) {
      collection[key] = fieldValue.editable || fieldValue.value
    } else if (isObject(fieldValue)) {
      mergeEditable(fieldValue)
    }
  })

  return obj
}

const renderView = async (callback, path, data, viewbag) => {
  await nuxt.ready()
  const context = {
    req: {
      ssrData: typeof data === 'string'
        ? mergeEditable(JSON.parse(data))
        : data
    }
  }

  nuxt.renderRoute(path, context)
    .then(({html}) => callback(null, { html }))
    .catch(error => callback(error))
}

module.exports = {
  renderView
}