export const state = () => ({
  context: undefined,
  route: undefined
})

export const mutations = {
  setContext (state, payload) {
    state.context = payload
  },
  setRoute (state, payload) {
    state.route = payload
  }
}

export const getters = {
  isEditing (state) {
    return state.context && state.context.pageEditing
  }
}
